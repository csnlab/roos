import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.metrics import roc_auc_score
from warp import warp_trials
from utils import select_data_experiment, get_experiments_combo
from utils import rolling_window

binsize = 50
align_to = 'stimulus'

"""
small script to print the performance per animal
"""

trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   add_reaction_speed_quantiles=False,
                                                   enforce_min_max_reaction_times=True)
trial_df_all = trial_df

animal_ids = trial_df['animal_id'].unique()

for animal_id in animal_ids:
    adf = trial_df[trial_df['animal_id'] == animal_id]

    animal_sessions = adf['session_id'].unique()

    for sid in animal_sessions:

        sdf = adf[adf['session_id'] == sid]
        sdf = sdf[sdf['hit_miss'] == 'hit']

        perf = 100 * sdf['response'].sum() / sdf.shape[0]

        print('Animal {} - session {:02d} - performance = {:.1f}'.format(animal_id, sid, perf))



for animal_id in animal_ids:
    adf = trial_df[trial_df['animal_id'] == animal_id]

    animal_sessions = adf['session_id'].unique()


    sdf = adf[adf['hit_miss'] == 'hit']

    perf = 100 * sdf['response'].sum() / sdf.shape[0]

    print('Animal {} - session {:02d} - performance = {:.1f}'.format(animal_id, sid, perf))


