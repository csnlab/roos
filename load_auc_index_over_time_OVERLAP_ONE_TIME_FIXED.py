import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *
from utils import get_experiments_combo

"""
Computes the overlap in the sets of cells with significant AUC
between different contrasts. We first find the set of cells
which have significant AUC for contrast 1 in at least X time bins 
within a specified range and then compute the overlap with cells 
significant for contrast 2 across all time points. This is done
per modality.
"""


plot_format = 'png'
alpha_level = 0.01

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc_overlap')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


if alpha_level == 0.001:
    col_name = 'is_sign_001'
elif alpha_level == 0.01:
    col_name = 'is_sign_01'
elif alpha_level == 0.05:
    col_name = 'is_sign_05'
else:
    raise ValueError

global_setting = 'jan24'
global_setting = 'jan29'

comparisons = ['contrast_vs_outcome',
               'contrast_vs_hitmiss',
               'hitmiss_vs_outcome']

#comparisons = ['hitmiss_vs_outcome']

for comparison in comparisons:
    if comparison == 'contrast_vs_outcome':

        ref_settings_name = global_setting
        ref_combo = 'target_vs_distractor'
        ref_times = [5,   45,   85,  125,  165,  205, 245]

        tar_settings_name = global_setting
        tar_combo = 'correct_vs_incorrect'


    elif comparison == 'contrast_vs_hitmiss':

        ref_settings_name = global_setting
        ref_combo = 'target_vs_distractor'
        ref_times = [5, 45, 85, 125, 165, 205]

        tar_settings_name = 'jan23'
        tar_combo = 'hit_miss'



    elif comparison == 'hitmiss_vs_outcome':

        ref_settings_name = global_setting
        ref_combo = 'hit_miss'
        ref_times = [285,  325,  365, 405,  445,  485,
                     525, 565, 605, 645]
        ref_times = [485,  525,  565,  605,  645,  685,  725,]

        tar_settings_name = global_setting
        tar_combo = 'correct_vs_incorrect'



    ref_experiments = get_experiments_combo(ref_combo)
    tar_experiments = get_experiments_combo(tar_combo)

    dn = pd.DataFrame(columns=['time', 'experiment_1', 'experiment_2', 'modality', 'overlap'])

    for ref_experiment, tar_experiment in zip(ref_experiments, tar_experiments):

        modality = ref_experiment[-1]
        assert ref_experiment[-1] == tar_experiment[-1]
        # TODO: assert that both experiments are warped or not warped

        # LOAD REFERENCE EXPERIMENT:
        output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(ref_combo, ref_settings_name)
        output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', ref_settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)
        out = pickle.load(open(output_full_path, 'rb'))
        ref_df = out['auc_scores']
        ref_df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(ref_df['session_id'], ref_df['unit_ind'])]
        try:
            warped = out['pars']['warp']
        except:
            if len(out['pars']['median_reaction_times']) > 0:
                warped = True
            else:
                warped = False


        # LOAD TARGET EXPERIMENT:
        output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(tar_combo, tar_settings_name)
        output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', tar_settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)
        out_tar = pickle.load(open(output_full_path, 'rb'))
        tar_df = out_tar['auc_scores']
        tar_df['unit_id'] = ['{}_{}'.format(a, b) for a, b in zip(tar_df['session_id'], tar_df['unit_ind'])]

        times = tar_df['time'].unique()

        for tix, time in enumerate(times):

            # get significant cells in the reference experiment
            ref_df_time = ref_df[np.isin(ref_df['time'], ref_times) & (ref_df['experiment'] == ref_experiment)]
            ref_sign = ref_df_time[col_name]
            reference_cohort = ref_df_time['unit_id'][ref_sign]

            # if tix>=2 and tix<=(times.shape[0]-3):
            #
            #     expanded_times = [times[tix-2], times[tix-1], times[tix],
            #                       times[tix+1], times[tix+2]]


            tar_df_time = tar_df[np.isin(tar_df['time'], time) & (tar_df['experiment'] == tar_experiment)]
            tar_sign = tar_df_time[col_name]
            target_cohort = tar_df_time['unit_id'][tar_sign]

            n_shared_cells = len(set(reference_cohort).intersection(set(target_cohort)))
            n_total_cells = len(set(reference_cohort).union(set(target_cohort)))

            overlap = 100 * n_shared_cells / (n_total_cells+0.00001)

            #new_cells = 100 * (~np.isin(target_cohort, reference_cohort)).sum() / target_cohort.shape[0]

            # if bootstrap:
            #     for boot in range(n_boot):
            #         sign_boot = np.random.choice(sign, size=sign.shape[0], replace=True)
            #         perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
            #         dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]
            # dn.loc[dn.shape[0], :] = [time, ref_experiment, tar_experiment, overlap]

            dn.loc[dn.shape[0], :] = [time, ref_experiment, tar_experiment, modality, overlap]


    dn['area'] = None
    dn['area'] = 'V1'


    if warped:
        f, ax = plt.subplots(1, 1, figsize=[5, 3])
    else:
        f, ax = plt.subplots(1, 1, figsize=[3, 3])

    sns.lineplot(data=dn, y='overlap', x='time', ax=ax, hue='modality',
                 palette=modality_palette, markers=True, style='area',
                 ci='sd')
    #ax.axvline(x=ref_time, c=sns.xkcd_rgb['grey'], ls='--')

    ax.axvspan(ref_times[0], ref_times[-1], color=sns.xkcd_rgb['light grey'], zorder=-10)
    #ax.set_title('overlap {} vs. {}'.format(ref_experiment, tar_experiment))
    ax.axvline(0,
               c=sns.xkcd_rgb['grey'],
               ls='--')
    if warped:
        median_reaction_times = out['pars']['median_reaction_times']
        for modality in modalities:
            ax.axvline(median_reaction_times[modality],
                       c=modality_palette[modality],
                       ls='--')
    ax.set_ylabel('% overlap {}'.format(comparison))
    ax.set_xlabel('Time [ms]')
    ax.set_ylim([0, 30])
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()


    plot_name = 'auc_overlap_{}_{}.{}'.format(comparison, alpha_level, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)






# df_time = ref_df[(ref_df['time'] == ref_time) & (ref_df['experiment'] == ref_experiment)]
# sign = df_time['is_sign']
# reference_cohort = df_time['unit_id'][sign]

# n_sig = len(reference_cohort)
# n_tot = len(df_time)
# perc_sig = 100*n_sig/n_tot
# print('Reference cohort:\n')
# print('Units discriminating --> {}: {} of {} ({:.1f}%)'.format(ref_experiment,n_sig, n_tot, perc_sig))



