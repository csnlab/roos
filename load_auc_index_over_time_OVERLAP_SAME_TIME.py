import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *
import itertools
from utils import get_experiments_combo

"""
Look at the overlap between sets of unit significant for a given 
contrast across different modalities over time. 

only_shared_sessions makes sure we select only sessions in which
a given AUC contrast is computed in all three modalities (to avoid confounds).
"""


plot_format = 'png'
alpha_level = 0.01
only_shared_sessions = True

settings_name = 'jan20warp'
combo = 'target_vs_distractor'

settings_name = 'jan16'
combo = 'correct_vs_incorrect'
#
settings_name = 'jan20warp'
combo = 'hit_miss'


settings_name = 'jan23'
settings_name = 'jan24'

settings_name = 'jan29'


combos = ['target_vs_distractor',
          'correct_vs_incorrect',
          'hit_miss']

for combo in combos:

    experiments = get_experiments_combo(combo)

    experiments = list(itertools.combinations(experiments, r=2))


    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc_overlap')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    if alpha_level == 0.001:
        col_name = 'is_sign_001'
    elif alpha_level == 0.01:
        col_name = 'is_sign_01'
    elif alpha_level == 0.05:
        col_name = 'is_sign_05'
    else:
        raise ValueError


    dn = pd.DataFrame(columns=['time', 'experiment_1', 'experiment_2', 'combination_name', 'overlap'])


    for ref_experiment, tar_experiment in experiments:

        # TODO: assert that both experiments are warped or not warped

        # LOAD REFERENCE EXPERIMENT:
        output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
        output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)
        out = pickle.load(open(output_full_path, 'rb'))
        ref_df = out['auc_scores']
        ref_df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(ref_df['session_id'], ref_df['unit_ind'])]
        ref_df = ref_df[ref_df['experiment'] == ref_experiment]

        # LOAD TARGET EXPERIMENT:
        output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
        output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)
        out = pickle.load(open(output_full_path, 'rb'))
        tar_df = out['auc_scores']
        tar_df['unit_id'] = ['{}_{}'.format(a, b) for a, b in zip(tar_df['session_id'], tar_df['unit_ind'])]
        tar_df = tar_df[tar_df['experiment'] == tar_experiment]

        if only_shared_sessions:
            ref_sess = ref_df['session_id'].unique()
            tar_sess = tar_df['session_id'].unique()
            shared_sess = list(set(ref_sess).intersection(set(tar_sess)))
            ref_df = ref_df[np.isin(ref_df['session_id'], shared_sess)]
            tar_df = tar_df[np.isin(tar_df['session_id'], shared_sess)]
            ref_sess = ref_df['session_id'].unique()
            tar_sess = tar_df['session_id'].unique()
            np.testing.assert_array_equal(ref_sess,  tar_sess)

        try:
            warped = out['pars']['warp']
        except:
            if len(out['pars']['median_reaction_times']) > 0:
                warped = True
            else:
                warped = False


        times = tar_df['time'].unique()

        for tix, time in enumerate(times):

            # we consider cells that are significant in a small range of times
            # to contain precise timing effects

            if tix>=2 and tix<=(times.shape[0]-3):

                expanded_times = [times[tix-2], times[tix-1], times[tix],
                                  times[tix+1], times[tix+2]]

            #expanded_times = [time]

                # get significant cells in the reference experiment
                ref_df_time = ref_df[np.isin(ref_df['time'], expanded_times)]
                ref_sign = ref_df_time[col_name]
                reference_cohort = ref_df_time['unit_id'][ref_sign]

                # get significant cells in the target experiment
                tar_df_time = tar_df[np.isin(tar_df['time'], expanded_times)]
                tar_sign = tar_df_time[col_name]
                target_cohort = tar_df_time['unit_id'][tar_sign]

                n_shared_cells = len(set(reference_cohort).intersection(set(target_cohort)))
                n_total_cells = len(set(reference_cohort).union(set(target_cohort)))

                overlap = 100 * n_shared_cells / (n_total_cells+0.00001)

                #new_cells = 100 * (~np.isin(target_cohort, reference_cohort)).sum() / target_cohort.shape[0]

                combination_name = '{}_{}'.format(ref_experiment, tar_experiment)
                dn.loc[dn.shape[0], :] = [time, ref_experiment, tar_experiment, combination_name, overlap]

                # for boot in range(n_boot):
                #     sign_boot = np.random.choice(sign, size=sign.shape[0], replace=True)
                #     perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
                #     dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]



    f, ax = plt.subplots(1, 1, figsize=[5, 3])

    sns.lineplot(data=dn, y='overlap', x='time', ax=ax,
                 color=sns.xkcd_rgb['black'], markers=True, style='combination_name',
                 ci='sd', )
    #ax.axvline(x=ref_time, c=sns.xkcd_rgb['grey'], ls='--')

    #ax.set_title('overlap {} vs. {}'.format(ref_experiment, tar_experiment))
    ax.axvline(0,
               c=sns.xkcd_rgb['grey'],
               ls='--')
    if warped:
        median_reaction_times = out['pars']['median_reaction_times']
        for modality in modalities:
            ax.axvline(median_reaction_times[modality],
                       c=modality_palette[modality],
                       ls='--')
    ax.set_ylabel('% overlap\n{}\nvs. {}'.format(ref_experiment, tar_experiment))
    ax.set_xlabel('Time [ms]')
    ax.set_ylim([0, 55])
    ax.legend(prop={'size':8})
    #ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()


    plot_name = 'auc_overlap_{}_VS_{}_{}.{}'.format(ref_experiment, tar_experiment, settings_name, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)






# df_time = ref_df[(ref_df['time'] == ref_time) & (ref_df['experiment'] == ref_experiment)]
# sign = df_time['is_sign']
# reference_cohort = df_time['unit_id'][sign]

# n_sig = len(reference_cohort)
# n_tot = len(df_time)
# perc_sig = 100*n_sig/n_tot
# print('Reference cohort:\n')
# print('Units discriminating --> {}: {} of {} ({:.1f}%)'.format(ref_experiment,n_sig, n_tot, perc_sig))



