import os
import numpy as np
import pandas as pd
from constants import *
from loadmat import loadmat


def load_data(binsize, align_to, add_reaction_speed_quantiles=False,
              speed_quantile=0.25, enforce_min_max_reaction_times=True):

    if align_to == 'stimulus':
        filename = 'PSTHstruct{}_incMisses.mat'.format(binsize)
    elif align_to == 'response':
        filename = 'PSTHstruct{}_RespLocked.mat'.format(binsize)

    data = loadmat(os.path.join(DATA_FOLDER, filename))

    try:
        data = data[filename.split('.')[0]]
    except KeyError:
        data = data['{}_incMisses'.format(filename.split('.')[0])]

    n_sessions = data.shape[0]

    trial_df = pd.DataFrame(columns=['animal_id',
                                     'session_id',
                                     'trial_id',
                                     'modality',
                                     'difficulty',
                                     'response',
                                     'response_side',
                                     'reaction_time',
                                     'hit_miss',
                                     'stimulus_onset_time',
                                     'target',
                                     'n_neurons'])

    trial_data = {}
    motion_data = {}
    time_points = []

    for sess_ind in range(n_sessions) :

        session_id = sess_ind
        n_trials = data[sess_ind].shape[0]

        for trial_ind in range(n_trials) :
            animal_id = data[sess_ind][trial_ind].animal

            if '_' in animal_id:
                animal_id = '-'.join(animal_id.split('_'))

            trial_id = '{}_{}'.format(sess_ind, trial_ind)
            modality = data[sess_ind][trial_ind].mod
            difficulty = data[sess_ind][trial_ind].diff
            response = data[sess_ind][trial_ind].resp
            response_side = data[sess_ind][trial_ind].respSide
            reaction_time = data[sess_ind][trial_ind].RT
            hit_miss = 'hit'

            if np.isnan(response):
                assert reaction_time == 0.0

            if reaction_time == 0.0 and np.isnan(response):
                response = np.nan
                response_side = np.nan
                reaction_time = np.nan
                hit_miss = 'miss'

            if reaction_time >= MAX_REACTION_TIME_IN_SECONDS:
                response = np.nan
                response_side = np.nan
                reaction_time = np.nan
                hit_miss = 'late'

            if reaction_time < MIN_REACTION_TIME_IN_SECONDS:
                response = np.nan
                response_side = np.nan
                reaction_time = np.nan
                hit_miss = 'early'

            try:
                stimulus_onset_time = data[sess_ind][trial_ind].stimOn
            except AttributeError:
                stimulus_onset_time = None

            target = data[sess_ind][trial_ind].RF
            n_time_points = data[sess_ind][trial_ind].spikesPerTimePoint.shape[0]
            n_neurons = data[sess_ind][trial_ind].spikesPerTimePoint[0].spikesPerNeuron.shape[0]

            trial_df.loc[trial_df.shape[0], :] = [animal_id,
                                                  session_id,
                                                  trial_id,
                                                  modality,
                                                  difficulty,
                                                  response,
                                                  response_side,
                                                  reaction_time,
                                                  hit_miss,
                                                  stimulus_onset_time,
                                                  target,
                                                  n_neurons]

            all_spikes = []
            for time_ind in range(n_time_points) :
                spikes_time_point = \
                data[sess_ind][trial_ind].spikesPerTimePoint[
                    time_ind].spikesPerNeuron
                all_spikes.append(spikes_time_point)
                if trial_ind == 0 and sess_ind == 0 :
                    time_points.append(data[sess_ind][trial_ind].spikesPerTimePoint[
                            time_ind].timePointCenter)

            all_spikes = np.vstack(all_spikes)

            assert all_spikes.shape[0] == n_time_points
            assert all_spikes.shape[1] == n_neurons

            trial_data[trial_id] = all_spikes

            motion = []
            for time_ind in range(n_time_points) :
                motion_time_point = data[sess_ind][trial_ind].spikesPerTimePoint[time_ind].motion
                motion.append(motion_time_point)
            motion = np.array(motion)
            motion_data[trial_id] = motion

    for col in ['reaction_time', 'stimulus_onset_time', 'n_neurons'] :
        trial_df[col] = pd.to_numeric(trial_df[col])

    if align_to == 'response':
        trial_df['reaction_time'] = np.abs(trial_df['stimulus_onset_time'])
        trial_df = trial_df.drop('stimulus_onset_time', axis=1)

    if add_reaction_speed_quantiles:
        raise NotImplementedError

    # if add_reaction_speed_quantiles:
    #     dfx = []
    #     for modality in modalities:
    #         trial_dfx = trial_df[trial_df['modality'] == modality]
    #         rt = trial_dfx['reaction_time']
    #         q1 = rt.quantile(speed_quantile)
    #         trial_dfx['reaction_speed'] = 'medium'
    #         trial_dfx.loc[rt < q1, 'reaction_speed'] = 'fast'
    #         q2 = rt.quantile(1 - speed_quantile)
    #         trial_dfx.loc[rt > q2, 'reaction_speed'] = 'slow'
    #         dfx.append(trial_dfx)
    #     trial_df = pd.concat(dfx)

    if enforce_min_max_reaction_times:
        mask1 = trial_df['reaction_time'] >= MIN_REACTION_TIME_IN_SECONDS
        mask2 = trial_df['reaction_time'] <= MAX_REACTION_TIME_IN_SECONDS
        mask3 = trial_df['reaction_time'].isna()

        mask = np.logical_or(np.logical_and(mask1, mask2), mask3)

        trial_df = trial_df[mask]

    # make sure the session id is a string, so it cannot be confused for an index
    trial_df['session_id'] = trial_df['session_id'].astype(str)

    return trial_df, trial_data, motion_data, time_points





if False:
    binsize = 50

    # --- def prep_data

    filename = 'PSTHstruct{}.mat'.format(binsize)

    data = loadmat(os.path.join(DATA_FOLDER, filename))['PSTHstruct{}'.format(binsize)]

    n_sessions = data.shape[0]

    trial_df = pd.DataFrame(columns=['session_id',
                                     'trial_id',
                                     'modality',
                                     'difficulty',
                                     'response',
                                     'response_side',
                                     'reaction_time',
                                     'target',
                                     'n_neurons'])

    session_df = pd.DataFrame

    trial_data = {}
    time_points = []

    for sess_ind in range(n_sessions):

        session_id = sess_ind
        n_trials = data[sess_ind].shape[0]

        for trial_ind in range(n_trials):

            trial_id = '{}_{}'.format(sess_ind, trial_ind)
            modality = data[sess_ind][trial_ind].mod
            difficulty = data[sess_ind][trial_ind].diff
            response = data[sess_ind][trial_ind].resp
            response_side = data[sess_ind][trial_ind].respSide
            reaction_time = data[sess_ind][trial_ind].RT
            target = data[sess_ind][trial_ind].RF
            n_time_points = data[sess_ind][trial_ind].spikesPerTimePoint.shape[0]
            n_neurons = data[sess_ind][trial_ind].spikesPerTimePoint[0].spikesPerNeuron.shape[0]

            trial_df.loc[trial_df.shape[0], :] = [session_id,
                                                  trial_id,
                                                  modality,
                                                  difficulty,
                                                  response,
                                                  response_side,
                                                  reaction_time,
                                                  target,
                                                  n_neurons]


            all_spikes = []
            for time_ind in range(n_time_points):
                spikes_time_point = data[sess_ind][trial_ind].spikesPerTimePoint[time_ind].spikesPerNeuron
                all_spikes.append(spikes_time_point)
                if trial_ind == 0 and sess_ind == 0:
                    time_points.append(data[sess_ind][trial_ind].spikesPerTimePoint[time_ind].timePointCenter)

            all_spikes = np.vstack(all_spikes)

            assert all_spikes.shape[0] == n_time_points
            assert all_spikes.shape[1] == n_neurons
            n_time_points = all_spikes.shape[1]

            trial_data[trial_id] = all_spikes

    for col in ['reaction_time', 'n_neurons']:
        trial_df[col] = pd.to_numeric(trial_df[col])


