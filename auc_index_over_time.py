import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.metrics import roc_auc_score
from warp import warp_trials
from utils import select_data_experiment, get_experiments_combo
from utils import rolling_window
from utils import fix_animal_id

"""
Main script used to compute AUC at each point in time for all cells. 

If we do not balance the data, I introduced a debiased score where we 
subtract the mean of the surrogates and add back 0.5 (so it's centered around
0.5 as normal AUC). This  may be really superfluous because AUC is not very
sensitive to imbalances in trial numbers, so the difference between 
observed and debiased scores is typically super small. 

"""

settings_name = 'jan29'
align_to = 'stimulus'
binsize = 50
min_units = 1
min_trials_per_class = 8 #20
start_time_in_ms = -500
end_time_in_ms = 1500 #1500

warp = True

# define median reaction time used for warping for each modality
# or just take the median across modalities
warp_per_modality = False

# combine bins into 200 ms (if using 50 ms) with rolling window
larger_bins = False

# as AUC is not sensitive to class imbalance, we can use all trials
# instead of subsampling
subsample_majority_class = False

n_bootstraps = 250
seed = 92

# combo = 'main_experiments'
# combo = 'difficulty_VM'
combos = ['target_vs_distractor_correct',
          'target_vs_distractor',
          'correct_vs_incorrect_target',
          'correct_vs_incorrect_distractor',
          'correct_vs_incorrect',
          'hit_miss',
          'difficulty_VM']

combos = ['difficulty_V',
          'difficulty_M']

combos = ['difficulty_VM']


# combos = ['target_vs_distractor_correct',
#           'target_vs_distractor',
#           'hit_miss',
#           'difficulty_VM']

#combos = ['target_vs_distractor']

# combos = ['correct_vs_incorrect_V',
#           'correct_vs_incorrect_M',
#           'correct_vs_incorrect_A']
#
# combos = ['difficulty_VM']
#
# combos = ['correct_vs_incorrect']


for combo in combos:
    # --- OUTPUT FILES -------------------------------------------------------------
    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)



    # --- LOAD DATA ----------------------------------------------------------------
    trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                       align_to=align_to,
                                                       add_reaction_speed_quantiles=False,
                                                       enforce_min_max_reaction_times=True)
    trial_df_all = trial_df



    if warp:
        trial_data, trial_df, median_reaction_times = warp_trials(trial_data,
                                                                  trial_df,
                                                                  time_bin_centers,
                                                                  warp_per_modality=warp_per_modality,
                                                                  keep_misses=True)
    else:
        median_reaction_times = {}

    if larger_bins:
        trial_data, time_bin_centers = rolling_window(trial_data, time_bin_centers)

    experiments = get_experiments_combo(combo)


    df = pd.DataFrame(
        columns=['animal_id', 'session_id', 'experiment', 'binsize', 'unit_ind',
                 'time_bin', 'time',
                 'n_trials_minority_class', 'score_observed', 'score_debiased',
                 'p_val_1', 'p_val_2', 'is_sign_001', 'is_sign_01', 'is_sign_05'])

    for experiment in experiments:

        # the dp is generated every time from trial_df, which does not change in select_data_experiment
        # but it does get changed by warp_trials
        session_df, sdf, dp = select_data_experiment(experiment, trial_df)

        n_time_bins_per_trial = trial_data[dp['trial_numbers'].iloc[0]].shape[0]

        selsess_df = session_df[(session_df['n1'] >= min_trials_per_class) &
                                    (session_df['n2'] >= min_trials_per_class) &
                                    (session_df['n_units'] >= min_units)]
        selected_session_ids = selsess_df['session_id'].__array__()
        tot_n_units = selsess_df['n_units'].sum()

        print('\n\nEXPERIMENT: {}'.format(experiment))
        print('\n # units: {}'.format(tot_n_units))
        print(selsess_df)

        for sessn, session_id in enumerate(selsess_df['session_id']):

            print('Session {:02d} of {:02d}'.format(sessn, len(selsess_df)))

            animal_id = trial_df[trial_df['session_id'] == session_id]['animal_id'].unique()
            assert len(animal_id) == 1
            animal_id = animal_id[0]

            #trial_df[trial_df['session_id'] == session_id]

            d0 = dp[(dp['session_id'] == session_id) & (dp['target'] == 0)]
            d1 = dp[(dp['session_id'] == session_id) & (dp['target'] == 1)]
            # print(d0.shape)
            # print(d1.shape)
            n_trials_minority_class = min(d0.shape[0], d1.shape[0])

            if subsample_majority_class:
                t0 = np.random.choice(d0['trial_numbers'],
                                      size=n_trials_minority_class,
                                      replace=False)
                t1 = np.random.choice(d1['trial_numbers'],
                                      size=n_trials_minority_class,
                                      replace=False)

                resampled_trials = np.hstack([t0, t1])

                y = np.hstack([np.zeros(n_trials_minority_class),
                               np.ones(n_trials_minority_class)]).astype(int)

            else:
                resampled_trials = np.hstack([d0['trial_numbers'], d1['trial_numbers']])

                y = np.hstack([np.zeros(d0['trial_numbers'].shape[0]),
                               np.ones(d1['trial_numbers'].shape[0])]).astype(int)


            n_neurons = trial_df[trial_df['session_id'] == session_id]['n_neurons'].iloc[0]


            for unit_ind in np.arange(n_neurons):

                for time_bin in range(n_time_bins_per_trial):

                    time = time_bin_centers[time_bin]
                    # time0, time1 = time_bin_edges[time_bin]

                    if time >= start_time_in_ms and time <= end_time_in_ms:

                        X = np.vstack([trial_data[t][time_bin, unit_ind] for t in resampled_trials])
                        score_observed = roc_auc_score(y, X)

                        scores_null = []
                        for n in range(n_bootstraps):
                            y_perm = np.random.permutation(y)
                            score_null_overtime = []

                            score = roc_auc_score(y_perm, X)
                            scores_null.append(score)

                        p_val_1 = (np.array(scores_null) > score_observed).sum() / len(scores_null)
                        p_val_2 = (np.array(scores_null) <= score_observed).sum() / len(scores_null)

                        alpha = 0.001
                        q1 = np.quantile(scores_null, alpha / 2)
                        q2 = np.quantile(scores_null, 1 - alpha / 2)
                        is_sign_001 = score_observed < q1 or score_observed > q2

                        alpha = 0.01
                        q1 = np.quantile(scores_null, alpha / 2)
                        q2 = np.quantile(scores_null, 1 - alpha / 2)
                        is_sign_01 = score_observed < q1 or score_observed > q2

                        alpha = 0.05
                        q1_05 = np.quantile(scores_null, alpha / 2)
                        q2_05 = np.quantile(scores_null, 1 - alpha / 2)
                        is_sign_05 = score_observed < q1_05 or score_observed > q2_05

                        score_debiased = score_observed - np.mean(scores_null) + 0.5

                        row = [animal_id, session_id, experiment, binsize, unit_ind, time_bin, time,
                               n_trials_minority_class, score_observed, score_debiased, p_val_1, p_val_2,
                               is_sign_001, is_sign_01, is_sign_05]

                        df.loc[df.shape[0], :] = row

    numeric_cols = ['score_observed', 'p_val_1', 'p_val_2', 'time_bin', 'time']
    for col in numeric_cols :
        df[col] = pd.to_numeric(df[col])
    df['is_sign_01'] = df['is_sign_01'].astype(bool)
    df['is_sign_05'] = df['is_sign_05'].astype(bool)

    df['animal_id'] = [fix_animal_id(aid) for aid in df['animal_id']]

    df['unit_id'] = ['{}_{}_{}'.format(a, s, i) for a, s, i in
                     zip(df['animal_id'], df['session_id'], df['unit_ind'])]

    pars = {'settings_name' : settings_name,
            'combo' : combo,
            'experiments' : experiments,
            'align_to' : align_to,
            'binsize' : binsize,
            'min_trials_per_class' : min_trials_per_class,
            'start_time_in_ms' : start_time_in_ms,
            'end_time_in_ms' : end_time_in_ms,
            'n_bootstraps' : n_bootstraps,
            'seed' : seed,
            'warp' : warp,
            'median_reaction_times' : median_reaction_times,
            'warp_per_modality' : warp_per_modality,
            'larger_bins' : larger_bins,
            'time_bin_centers' : time_bin_centers,
            'subsample_majority_class' : subsample_majority_class}

    out = {'pars' : pars,
           'auc_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))

