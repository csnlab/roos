import pandas as pd
import numpy as np

hit_miss_experiments = ['hit_miss_V',
                        'hit_miss_M',
                        'hit_miss_A']

correct_vs_incorrect_experiments = ['correct_vs_incorrect_V',
                                    'correct_vs_incorrect_M',
                                    'correct_vs_incorrect_A']

target_vs_distractor_experiments = ['target_vs_distractor_V',
                                    'target_vs_distractor_M',
                                    'target_vs_distractor_A',
                                    'target_vs_distractor_with_misses_V',
                                    'target_vs_distractor_with_misses_M',
                                    'target_vs_distractor_with_misses_A']


response_side_experiments = ['response_side',
                             'response_side_V',
                             'response_side_M',
                             'response_side_A']


def poisson_EV(y, yhat, ynull) :
    eps = np.spacing(1)
    D_model = np.sum(y * np.log(eps + y / (yhat+eps)) - (y - yhat))
    D_total = np.sum(y * np.log(eps + y / (ynull+eps)) - (y - ynull))
    EV = 1 - D_model / D_total
    return EV


def fix_animal_id(animal_id):
    if '_' in animal_id :
        animal_id = '-'.join(animal_id.split('_'))
    return animal_id

def rolling_window(trial_data, motion_data, time_bin_centers):
    for key in trial_data.keys():
        new_trial = []
        new_motion = []
        new_times = []
        for indx, time in enumerate(time_bin_centers):
            if indx >= 2 and indx < len(time_bin_centers) - 2:
                rebinned_trial = trial_data[key][indx - 2 :indx + 2, :].sum(axis=0) / 4
                rebinned_motion = motion_data[key][indx - 2 :indx + 2].sum(axis=0) / 4
                new_trial.append(rebinned_trial)
                new_motion.append(rebinned_motion)
                new_times.append(time)
        new_trial = np.vstack(new_trial)
        new_motion = np.array(new_motion)
        trial_data[key] = new_trial
        motion_data[key] = new_motion

    return trial_data, motion_data, new_times


def rolling_window_sparse(trial_data, motion_data, time_bin_centers):
    N_BINS = 3
    SUBSAMPLE = 3
    for key in trial_data.keys():
        new_trial = []
        new_motion = []
        new_times = []
        for indx, time in enumerate(time_bin_centers):
            if indx >= N_BINS and indx < len(time_bin_centers) - N_BINS:
                rebinned_trial = trial_data[key][indx - N_BINS :indx + N_BINS, :].sum(axis=0) / N_BINS
                rebinned_motion = motion_data[key][indx - N_BINS :indx + N_BINS].sum(axis=0) / N_BINS
                new_trial.append(rebinned_trial)
                new_motion.append(rebinned_motion)
                new_times.append(time)
        new_trial = np.vstack(new_trial)[::SUBSAMPLE]
        new_motion = np.array(new_motion)[::SUBSAMPLE]
        new_times = np.array(new_times)[::SUBSAMPLE]
        trial_data[key] = new_trial
        motion_data[key] = new_motion

    return trial_data, motion_data, new_times


def rebin(trial_data, motion_data, time_bin_centers):
    time_bin_centers = np.array(time_bin_centers)
    for key in trial_data.keys():
        new_trial = []
        new_motion = []
        new_times = []
        for i, time in enumerate(time_bin_centers[2:-2:2]):
            indx = np.where(time_bin_centers==time)[0][0]
            rebinned_trial = trial_data[key][indx - 2 :indx + 2, :].sum(axis=0) / 4
            rebinned_motion = motion_data[key][indx - 2 :indx + 2].sum(axis=0) / 4

            new_trial.append(rebinned_trial)
            new_motion.append(rebinned_motion)
            new_times.append(time)
        new_trial = np.vstack(new_trial)
        new_motion = np.array(new_motion)
        trial_data[key] = new_trial
        motion_data[key] = new_motion
    return trial_data, motion_data, new_times


def log2_likelihood(true, predictions) :
    # https://stackoverflow.com/questions/48185090/how-to-get-the-log-likelihood-for-a-logistic-regression-model-in-sklearn
    y_true = true
    y_pred = predictions[:, 1]
    eps = np.finfo(y_pred.dtype).eps
    y_pred = np.clip(y_pred, eps, 1 - eps)
    terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
    ll = np.sum(terms) / len(y_true)
    return ll



def select_data_experiment(experiment, trial_df):

    session_ids = trial_df['session_id'].unique()

    if experiment == 'target_vs_distractor_V':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            trial_ids = sdf['trial_id']
            target = [1 if t== 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'target_vs_distractor_with_misses_V':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'V']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'V']
            trial_ids = sdf['trial_id']
            target = [1 if t== 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_with_misses_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'M']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'M']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_with_misses_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'A']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'A']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'target_vs_distractor_correct_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['response'] == 1]
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['response'] == 1]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_correct_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['response'] == 1]
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['response'] == 1]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_correct_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['response'] == 1]
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['response'] == 1]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'target_vs_distractor_incorrect_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['response'] == 0]
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['response'] == 0]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_incorrect_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['response'] == 0]
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['response'] == 0]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'target_vs_distractor_slow_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['reaction_speed'] == 'slow']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['reaction_speed'] == 'slow']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_slow_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['reaction_speed'] == 'slow']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['reaction_speed'] == 'slow']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_slow_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['reaction_speed'] == 'slow']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['reaction_speed'] == 'slow']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'target_vs_distractor_fast_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['reaction_speed'] == 'fast']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['reaction_speed'] == 'fast']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_fast_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['reaction_speed'] == 'fast']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['reaction_speed'] == 'fast']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'target_vs_distractor_fast_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['reaction_speed'] == 'fast']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['reaction_speed'] == 'fast']
            trial_ids = sdf['trial_id']
            target = [1 if t == 'target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'correct_vs_incorrect_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)


        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'correct_vs_incorrect_target' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            # sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            # sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_target_VM' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_target_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_target_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_target_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'correct_vs_incorrect_distractor' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_distractor_VM' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_distractor_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_distractor_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'correct_vs_incorrect_distractor_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[sdf['response'] == 1].shape[0]
            n2 = sdf[sdf['response'] == 0].shape[0]
            try :
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError :
                n_neu = 0
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'A']
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if r == 1 else 0 for r in sdf['response']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'difficulty_distractor_VM' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [2, 3])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if t == 1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_distractor_1vs3_VM':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'distractor']
            sdf = sdf[np.isin(sdf['difficulty'], [1, 3])]
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [3])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'distractor']
            sdf = sdf[np.isin(sdf['difficulty'], [1, 3])]
            trial_ids = sdf['trial_id']
            target = [1 if t==1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_distractor_V':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [2, 3])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if t==1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_distractor_M':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'distractor']
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [2, 3])].shape[0]
            try:
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError:
                continue
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'distractor']
            trial_ids = sdf['trial_id']
            target = [1 if t==1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_target_VM' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [2, 3])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if t == 1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_target_1vs3_VM':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'target']
            sdf = sdf[np.isin(sdf['difficulty'], [1, 3])]
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [3])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            sdf = sdf[sdf['target'] == 'target']
            sdf = sdf[np.isin(sdf['difficulty'], [1, 3])]
            trial_ids = sdf['trial_id']
            target = [1 if t==1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_target_V':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [2, 3])].shape[0]
            try:
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError:
                continue
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'V']
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if t==1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'difficulty_target_M':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'target']
            n1 = sdf[np.isin(sdf['difficulty'], [1])].shape[0]
            n2 = sdf[np.isin(sdf['difficulty'], [2, 3])].shape[0]
            try:
                n_neu = sdf['n_neurons'].iloc[0]
            except IndexError:
                continue
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[sdf['modality'] == 'M']
            sdf = sdf[sdf['target'] == 'target']
            trial_ids = sdf['trial_id']
            target = [1 if t==1 else 0 for t in sdf['difficulty']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'response_side' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            # sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            n1 = sdf[np.isin(sdf['response_side'], ['L'])].shape[0]
            n2 = sdf[np.isin(sdf['response_side'], ['R'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            # sdf = sdf[np.isin(sdf['modality'], ['V', 'M'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'R' else 0 for t in sdf['response_side']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'response_side_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V'])]
            n1 = sdf[np.isin(sdf['response_side'], ['L'])].shape[0]
            n2 = sdf[np.isin(sdf['response_side'], ['R'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['V'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'R' else 0 for t in sdf['response_side']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'response_side_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['M'])]
            n1 = sdf[np.isin(sdf['response_side'], ['L'])].shape[0]
            n2 = sdf[np.isin(sdf['response_side'], ['R'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['M'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'R' else 0 for t in sdf['response_side']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'response_side_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['A'])]
            n1 = sdf[np.isin(sdf['response_side'], ['L'])].shape[0]
            n2 = sdf[np.isin(sdf['response_side'], ['R'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['hit_miss'] == 'hit']
            sdf = sdf[np.isin(sdf['modality'], ['A'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'R' else 0 for t in sdf['response_side']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'hit_miss_A' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[np.isin(sdf['modality'], ['A'])]
            n1 = sdf[np.isin(sdf['hit_miss'], ['hit'])].shape[0]
            n2 = sdf[np.isin(sdf['hit_miss'], ['miss'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[np.isin(sdf['modality'], ['A'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'hit' else 0 for t in sdf['hit_miss']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'hit_miss_V' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[np.isin(sdf['modality'], ['V'])]
            n1 = sdf[np.isin(sdf['hit_miss'], ['hit'])].shape[0]
            n2 = sdf[np.isin(sdf['hit_miss'], ['miss'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[np.isin(sdf['modality'], ['V'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'hit' else 0 for t in sdf['hit_miss']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    elif experiment == 'hit_miss_M' :
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        sdfs = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[np.isin(sdf['modality'], ['M'])]
            n1 = sdf[np.isin(sdf['hit_miss'], ['hit'])].shape[0]
            n2 = sdf[np.isin(sdf['hit_miss'], ['miss'])].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]
            sdfs.append(sdf)
        sdf_return = pd.concat(sdfs)

        dp = []
        for session_id in session_ids :
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[np.isin(sdf['modality'], ['M'])]
            trial_ids = sdf['trial_id']
            target = [1 if t == 'hit' else 0 for t in sdf['hit_miss']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    else:
        raise ValueError

    if not np.isin(experiment, ['target_vs_distractor_V',
                                'target_vs_distractor_M',
                                'target_vs_distractor_A',
                                'target_vs_distractor_with_misses_V',
                                'target_vs_distractor_with_misses_M',
                                'target_vs_distractor_with_misses_A',
                                'correct_vs_incorrect_V',
                                'correct_vs_incorrect_M',
                                'correct_vs_incorrect_A',
                                'hit_miss_V',
                                'hit_miss_M',
                                'hit_miss_A',
                                'response_side',
                                'response_side_V',
                                'response_side_M',
                                'response_side_A']):
        sdf_return = None

    return session_df, sdf_return, dp



def get_experiments_combo(combo):

    if combo == 'target_vs_distractor':
        experiments = ['target_vs_distractor_V', 'target_vs_distractor_M', 'target_vs_distractor_A']
    elif combo == 'target_vs_distractor_easy':
        experiments = ['target_vs_distractor_easy_V', 'target_vs_distractor_easy_M', 'target_vs_distractor_easy_A']
    elif combo == 'target_vs_distractor_correct':
        experiments = ['target_vs_distractor_correct_V', 'target_vs_distractor_correct_M', 'target_vs_distractor_correct_A']
    elif combo == 'target_vs_distractor_incorrect':
        experiments = ['target_vs_distractor_incorrect_V', 'target_vs_distractor_incorrect_M']
    elif combo == 'target_vs_distractor_slow':
       experiments = ['target_vs_distractor_slow_V', 'target_vs_distractor_slow_M', 'target_vs_distractor_slow_A']
    elif combo == 'target_vs_distractor_fast':
       experiments = ['target_vs_distractor_fast_V', 'target_vs_distractor_fast_M', 'target_vs_distractor_fast_A']
    elif combo == 'correct_vs_incorrect':
        experiments = ['correct_vs_incorrect_V', 'correct_vs_incorrect_M', 'correct_vs_incorrect_A']
    elif combo == 'correct_vs_incorrect_target':
        experiments = ['correct_vs_incorrect_target_V', 'correct_vs_incorrect_target_M', 'correct_vs_incorrect_target_A']
    elif combo == 'correct_vs_incorrect_distractor':
        experiments = ['correct_vs_incorrect_distractor_V', 'correct_vs_incorrect_distractor_M', 'correct_vs_incorrect_distractor_A']
    elif combo == 'difficulty_V':
        experiments = ['difficulty_distractor_V', 'difficulty_target_V']
    elif combo == 'difficulty_M':
        experiments = ['difficulty_distractor_M', 'difficulty_target_M']
    elif combo == 'difficulty_VM':
        experiments = ['difficulty_distractor_VM', 'difficulty_target_VM']
    elif combo == 'response_side':
        experiments = ['response_side', 'response_side_V', 'response_side_M', 'response_side_A']
    elif combo == 'response_side_correct':
        experiments = ['response_side_correct', 'response_side_correct_V', 'response_side_correct_M', 'response_side_correct_A']
    elif combo == 'correct_vs_incorrect_V':
        experiments = ['correct_vs_incorrect_target_V', 'correct_vs_incorrect_distractor_V']
    elif combo == 'correct_vs_incorrect_M':
        experiments = ['correct_vs_incorrect_target_M', 'correct_vs_incorrect_distractor_M']
    elif combo == 'correct_vs_incorrect_A':
        experiments = ['correct_vs_incorrect_target_A', 'correct_vs_incorrect_distractor_A']
    elif combo == 'hit_miss':
        experiments = ['hit_miss_V', 'hit_miss_M', 'hit_miss_A']
    elif combo == 'main_experiments':
        experiments = ['target_vs_distractor_V',
                       'target_vs_distractor_M',
                       'target_vs_distractor_A',
                       'correct_vs_incorrect_V',
                       'correct_vs_incorrect_M',
                       'correct_vs_incorrect_A',
                       'hit_miss_V',
                       'hit_miss_M',
                       'hit_miss_A']

    return experiments