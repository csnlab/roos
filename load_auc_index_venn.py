import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from plotting_style import *
import matplotlib.gridspec as gridspec
from utils import rolling_window
from utils import get_experiments_combo
from matplotlib_venn import venn3

"""
Make venn diagrams of AUC results.

Make sure the required number of bins and the time ranges specified
for each contrast are consistent with other scripts. 
"""


auc_alpha_level = 0.01
align_to = 'stimulus'
binsize = 50

combos = [['jan24', 'target_vs_distractor'],
          ['jan24', 'correct_vs_incorrect'],
          ['jan24', 'hit_miss']]

combos = [['jan25_min8', 'target_vs_distractor'],
          ['jan25_min8', 'correct_vs_incorrect'],
          ['jan25_min8', 'hit_miss']]

combos = [['jan29', 'target_vs_distractor'],
          ['jan29', 'correct_vs_incorrect'],
          ['jan29', 'hit_miss']]

only_shared_sessions = True

#combos = [['jan24', 'target_vs_distractor']]


plot_format = 'png'

seed = 92

# --- PLOTS FOLDER -------------------------------------------------------------

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc_global')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

# --- ALPHA LEVEL -------------------------------------------------------------

if auc_alpha_level == 0.001:
    col_name = 'is_sign_001'
elif auc_alpha_level == 0.01:
    col_name = 'is_sign_01'
elif auc_alpha_level == 0.05:
    col_name = 'is_sign_05'
else:
    raise ValueError


for settings_name, combo in combos:

    if combo == 'target_vs_distractor':
        auc_ref_times = [5,   45,   85,  125,  165,  205, 245,  285]
        min_sign_times_points = 1

    elif combo == 'hit_miss':
        auc_ref_times = [485,  525,  565,  605,  645,  685,  725,  765]
        min_sign_times_points = 2

    elif combo == 'correct_vs_incorrect':
        auc_ref_times = [805, 845, 885, 925, 965, 1005, 1045, 1085, 1125, 1165,
                         1205, 1245, 1285, 1325, 1365, 1405, 1445, 1485]
        min_sign_times_points = 3

    # --- GET IDs of SIGNIFICANT UNITS ---------------------------------------------
    #session_df, sdf, dp = select_data_experiment(experiment, trial_df)

    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo,
                                                                    settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    df = out['auc_scores']

    def fix_animal_id(animal_id):
        if '_' in animal_id :
            animal_id = '-'.join(animal_id.split('_'))
        return animal_id

    df['animal_id'] = [fix_animal_id(aid) for aid in df['animal_id']]


    df['unit_id'] = ['{}_{}_{}'.format(a, s, i) for a, s, i in zip(df['animal_id'], df['session_id'], df['unit_ind'])]

    times = df['time'].unique()
    experiments =  df['experiment'].unique()
    pars = out['pars']

    try:
        larger_bins = pars['larger_bins']
    except KeyError:
        if settings_name == 'jan24':
            larger_bins = True
        else:
            larger_bins = False

    try:
        subsample_majority_class = pars['subsample_majority_class']
    except KeyError:
        subsample_majority_class = True

    if subsample_majority_class:
        score_col = 'score_observed'
    else:
        score_col = 'score_debiased'

    if only_shared_sessions:
        expsess = []
        for experiment in df['experiment'].unique():
            sessions_experiment = df[df['experiment'] == experiment]['session_id'].unique()
            expsess.append(set(sessions_experiment))
        shared_sessions = set.intersection(*expsess)
        df = df[np.isin(df['session_id'], list(shared_sessions))]




    experiments = df['experiment'].unique()

    significant_neurons = {}
    total_n_neurons = {}

    for experiment in experiments:
        signdf = df[np.isin(df['time'], auc_ref_times) & (df['experiment'] == experiment)]

        selected_units = []
        for uid in signdf['unit_id'].unique():
            sign_time_points = signdf[signdf['unit_id'] == uid][col_name].sum()
            if sign_time_points >= min_sign_times_points:
                selected_units.append(uid)
        significant_neurons[experiment] = selected_units
        total_n_neurons[experiment] = signdf['unit_id'].unique().shape[0]


    sets = [set(significant_neurons[e]) for e in experiments]

    f, ax = plt.subplots(1, 1, figsize=[4, 4])
    v = venn3([*sets], ['', '', ''], ax=ax)

    for patch, experiment in zip(['100', '010', '001'], experiments):
        try:
            v.get_patch_by_id(patch).set_color(experiment_palette[experiment])
        except AttributeError:
            pass

    for patch in ['110', '011', '101']:
        try:
            v.get_patch_by_id(patch).set_color(sns.xkcd_rgb['light grey'])
        except AttributeError:
            pass
    ax.set_title(combo_labels[combo])

    plot_name = 'auc_global_venn_{}_{}.{}'.format(settings_name, combo, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()
