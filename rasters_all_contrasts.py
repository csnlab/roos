import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from plotting_style import *
import matplotlib.gridspec as gridspec
from utils import rolling_window
from utils import fix_animal_id

"""
plot rasters for all contrasts
"""

settings_name = 'simple'

auc_alpha_level = 0.01
align_to = 'stimulus'
binsize = 50


larger_bins = True
warp = True
warp_per_modality = True

plot_format = 'png'
joint_auc_axes = False

selected_unit_ids = ['E2R-008_4_19']

# --- PLOTS FOLDER -------------------------------------------------------------

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'psths')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

# --- ALPHA LEVEL -------------------------------------------------------------

if auc_alpha_level == 0.001:
    col_name = 'is_sign_001'
elif auc_alpha_level == 0.01:
    col_name = 'is_sign_01'
elif auc_alpha_level == 0.05:
    col_name = 'is_sign_05'
else:
    raise ValueError


# --- LOAD DATA ----------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

if warp:
    trial_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True)
else:
    median_reaction_times = {}

if larger_bins:
    trial_data, time_bin_centers = rolling_window(trial_data, time_bin_centers)


# --- SELECT TRIALS ----------------------------------------------------------------

def get_vals(combo):
    if combo == 'target_vs_distractor' :
        vals = ['target', 'distractor']
    elif combo == 'correct_vs_incorrect' :
        vals = [0, 1]
    elif combo == 'hit_miss' :
        vals = ['hit', 'miss']
    elif combo == 'response_side':
        vals = ['L', 'R']
    return vals

def get_col(combo):
    if combo == 'target_vs_distractor' :
        col = 'target'
    elif combo == 'correct_vs_incorrect' :
        col = 'response'
    elif combo == 'hit_miss' :
        col = 'hit_miss'
    elif combo == 'response_side':
        col = 'response_side'
    return col


for unit_id in selected_unit_ids:

    animal_id = unit_id.split('_')[0]
    session_id = unit_id.split('_')[1]
    unit_ind = int(unit_id.split('_')[2])

    combos = ['target_vs_distractor', 'hit_miss', 'correct_vs_incorrect', 'response_side']

    unit_data = {c : {m : {} for m in modalities} for c in combos}

    for combo in combos:
        col = get_col(combo)
        vals = get_vals(combo)
        for im, modality in enumerate(modalities):
            unit_data[combo][modality] = {}
            for ir, val in enumerate(vals):
                sdf = trial_df[(trial_df['modality'] == modality) &
                               (trial_df[col] == val) &
                               (trial_df['session_id'] == int(session_id)) &
                               (trial_df['animal_id'] == animal_id)]
                try:
                    unit_data[combo][modality][val] = np.vstack([trial_data[tid][:, unit_ind] for tid in sdf['trial_id']])
                except ValueError:
                    unit_data[combo][modality][val] = np.zeros(shape=[1, len(time_bin_centers)])


    vmaxes = []
    for ic, combo in enumerate(combos) :
        vals = get_vals(combo)
        for im, modality in enumerate(modalities) :
            vmaxes.append(unit_data[combo][modality][vals[0]].max())
            vmaxes.append(unit_data[combo][modality][vals[1]].max())

    vmax = np.max(vmaxes)

    xticks = [0, 500, 1000]

    f = plt.figure(tight_layout=True, figsize=[7, 7])
    gs = gridspec.GridSpec(4, 3)

    # --- PLOT IMSHOW ----------------------------------------------------------

    imshow_axes = {c : {} for c in combos}

    for ic, combo in enumerate(combos):
        for im, modality in enumerate(modalities):
                imshow_axes[combo][modality] = f.add_subplot(gs[ic, im])

    for ic, combo in enumerate(combos):
        for im, modality in enumerate(modalities):

            vals = get_vals(combo)

            n_trials_1 = unit_data[combo][modality][vals[0]].shape[0]
            n_trials_2 = unit_data[combo][modality][vals[1]].shape[0]

            n_trials = n_trials_1 + n_trials_2

            data1 = unit_data[combo][modality][vals[0]]
            data2 = unit_data[combo][modality][vals[1]]

            data = np.vstack([data1, data2])

            imshow_axes[combo][modality].imshow(data,
                                                vmin=0, vmax=vmax,
                                                extent=(time_bin_centers[0]-0.5, time_bin_centers[-1]-0.5, n_trials+0.5, 0.5),
                                                aspect='auto')
            imshow_axes[combo][modality].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
            imshow_axes[combo][modality].set_xticks(xticks)
            imshow_axes[combo][modality].set_yticks([1, n_trials_1, n_trials])
            imshow_axes[combo][modality].set_yticks([1, n_trials])
            imshow_axes[combo][modality].axhline([n_trials_1], c=sns.xkcd_rgb['white'])

            if warp:
                imshow_axes[combo][modality].axvline(median_reaction_times[modality],
                                                        c=modality_palette[modality],
                                                        ls='--')

    imshow_axes[combos[0]][modalities[0]].set_ylabel(combos[0])
    imshow_axes[combos[1]][modalities[0]].set_ylabel(combos[1])
    imshow_axes[combos[2]][modalities[0]].set_ylabel(combos[2])
    imshow_axes[combos[3]][modalities[0]].set_ylabel(combos[3])

    imshow_axes[combos[0]][modalities[0]].set_title(modalities[0])
    imshow_axes[combos[0]][modalities[1]].set_title(modalities[1])
    imshow_axes[combos[0]][modalities[2]].set_title(modalities[2])

    sns.despine()

    plot_name = 'psth_simple_{}_{}.{}'.format(settings_name, unit_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()




    # psth_axes = {m : {} for m in modalities}
    # for im, modality in enumerate(modalities):
    #     for ir, response in enumerate(responses):
    #         psth_axes[modality][response] = f.add_subplot(gs[-2, im])
    #
    #
    #
    #
    # psth_axes = {m : {} for m in modalities}
    # for im, modality in enumerate(modalities):
    #     for ir, response in enumerate(responses):
    #         psth_axes[modality][response] = f.add_subplot(gs[-2, im])
    #
    #
    # auc_axes = {}
    # for exp, experiment in enumerate(experiments):
    #     auc_axes[experiment] = f.add_subplot(gs[-1, exp])
    #
    # # PLOT FIRING HEATMAPS
    # for im, modality in enumerate(modalities):
    #     for ir, response in enumerate(responses):
    #         n_trials = unit_data[modality][response].shape[0]
    #         imshow_axes[modality][response].imshow(unit_data[modality][response],
    #                                                vmin=0, vmax=np.max(vmaxes),
    #                                                extent=(times[0]-0.5, times[-1]-0.5, n_trials+0.5, 0.5),
    #                                                aspect='auto')
    #         imshow_axes[modality][response].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #         imshow_axes[modality][response].set_xticks(xticks)
    #         if warp:
    #             median_reaction_times = out['pars']['median_reaction_times']
    #             imshow_axes[modality][response].axvline(median_reaction_times[modality],
    #                                                     c=modality_palette[modality],
    #                                                     ls='--')
    #
    #             imshow_axes[modality][response].set_yticks([1, n_trials])
    #
    # imshow_axes[modalities[0]][responses[0]].set_title(modalities[0])
    # imshow_axes[modalities[1]][responses[0]].set_title(modalities[1])
    # imshow_axes[modalities[2]][responses[0]].set_title(modalities[2])
    # imshow_axes[modalities[0]][responses[0]].set_ylabel(response_labels[responses[0]], rotation=90, size='large')
    # imshow_axes[modalities[0]][responses[1]].set_ylabel(response_labels[responses[1]], rotation=90, size='large')
    #
    #
    # # PLOT PSTH
    #
    # uppers = []
    # for im, modality in enumerate(modalities):
    #     for ir, response in enumerate(responses):
    #         n_trials = unit_data[modality][response].shape[0]
    #         data = unit_data[modality][response]
    #
    #         upper = np.quantile(data, q=0.8, axis=0)
    #         uppers.append(upper.max())
    # xlim = np.max(uppers)
    #
    # for im, modality in enumerate(modalities):
    #     for ir, response in enumerate(responses):
    #         n_trials = unit_data[modality][response].shape[0]
    #         data = unit_data[modality][response]
    #
    #         #upper = np.quantile(data, q=0.8, axis=0)
    #         #lower = np.quantile(data, q=0.2, axis=0)
    #         mean = np.mean(data, axis=0)
    #         # TODO here we could compute a 95% CI of the mean
    #         # TODO but I am more interested in variation in the data
    #         std = np.std(data, axis=0)
    #
    #         ax = psth_axes[modality][response]
    #         ax.plot(times, mean, c=modality_palette[modality], ls=response_linestyles[response])
    #         # ax.fill_between(times, lower, upper, color=modality_palette[modality],
    #         #                 alpha=0.3, zorder=-2, linewidth=0)
    #         ax.fill_between(times, mean-std/2, mean+std/2, color=modality_palette[modality],
    #                         alpha=0.3, zorder=-2, linewidth=0)
    #         ax.set_ylim([0, xlim])
    #         ax.set_xticks(xticks)
    #         ax.set_xlim([times[0], times[-1]])
    #
    #         if warp:
    #             median_reaction_times = out['pars']['median_reaction_times']
    #             ax.axvline(median_reaction_times[modality],
    #                                                     c=modality_palette[modality],
    #                                                     ls='--')
    #             ax.axvline(0, ls='--', c=sns.xkcd_rgb['grey'])
    #
    # psth_axes[modalities[0]][responses[0]].set_ylabel('Firing rate [??]')
    #
    #
    # # PLOT AUC
    # for experiment in experiments:
    #     dx = df[(df['unit_id'] == unit_id) & (df['experiment'] == experiment)]
    #     auc_axes[experiment].plot(dx['time'], dx[score_col], c=experiment_palette[experiment])
    #     # auc_axes[experiment].scatter(dx[~dx[col_name]]['time'], dx[~dx[col_name]]['score_observed'],
    #     #            marker='x', color=experiment_palette[experiment])
    #     if plot_observed:
    #         auc_axes[experiment].plot(dx['time'], dx['score_observed'], c=experiment_palette[experiment], ls=':')
    #
    #     auc_axes[experiment].scatter(dx[dx[col_name]]['time'], dx[dx[col_name]][score_col],
    #                marker='o', color=experiment_palette[experiment], s=15)
    #
    #     auc_axes[experiment].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #     auc_axes[experiment].set_xticks(xticks)
    #     if warp :
    #         median_reaction_times = out['pars']['median_reaction_times']
    #         for modality in modalities:
    #             auc_axes[experiment].axvline(median_reaction_times[experiment.split('_')[-1]],
    #                                              c=modality_palette[experiment.split('_')[-1]],
    #                                              ls='--')
    #     auc_axes[experiment].set_xlabel('Time [ms]')
    #
    #     auc_axes[experiment].set_xlim([times[0], times[-1]])
    #     auc_axes[experiment].set_ylim([0, 1])
    #     auc_axes[experiment].axhline(0.5, ls=':', c=sns.xkcd_rgb['grey'])
    #
    #
    # auc_axes[experiments[0]].set_ylabel('AUC score')
    #
    # sns.despine()
    #
    # plot_name = 'psth_{}_{}_{}.{}'.format(settings_name, auc_experiment, unit_id, plot_format)
    # f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    #
    #
    # plt.close()




    # else:
    #     f = plt.figure(tight_layout=True, figsize=[7, 6])
    #     gs = gridspec.GridSpec(3, 3)
    #
    #     imshow_axes = {m : {} for m in modalities}
    #     for im, modality in enumerate(modalities):
    #         for ir, response in enumerate(responses):
    #                 imshow_axes[modality][response] = f.add_subplot(gs[ir, im])
    #
    #     auc_axes = {}
    #     for exp, experiment in enumerate(experiments):
    #         auc_axes[experiment] = f.add_subplot(gs[-1, exp])
    #
    #     for im, modality in enumerate(modalities):
    #         for ir, response in enumerate(responses):
    #             n_trials = unit_data[modality][response].shape[0]
    #             imshow_axes[modality][response].imshow(unit_data[modality][response],
    #                                                    vmin=0, vmax=np.max(vmaxes),
    #                                                    extent=(times[0]-0.5, times[-1]-0.5, n_trials+0.5, 0.5),
    #                                                    aspect='auto')
    #             imshow_axes[modality][response].axvline()
    #
    #             imshow_axes[modality][response].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #             if warp:
    #                 median_reaction_times = out['pars']['median_reaction_times']
    #                 imshow_axes[modality][response].axvline(median_reaction_times[modality],
    #                                                         c=modality_palette[modality],
    #                                                         ls='--')
    #
    #                 imshow_axes[modality][response].set_yticks([1, n_trials])
    #
    #
    #     imshow_axes[modalities[0]][responses[0]].set_title(modalities[0])
    #     imshow_axes[modalities[1]][responses[0]].set_title(modalities[1])
    #     imshow_axes[modalities[2]][responses[0]].set_title(modalities[2])
    #     imshow_axes[modalities[0]][responses[0]].set_ylabel(response_labels[responses[0]], rotation=90, size='large')
    #     imshow_axes[modalities[0]][responses[1]].set_ylabel(response_labels[responses[1]], rotation=90, size='large')
    #
    #     for experiment in experiments:
    #         dx = df[(df['unit_id'] == unit_id) & (df['experiment'] == experiment)]
    #         auc_axes[experiment].plot(dx['time'], dx[score_col], c=experiment_palette[experiment])
    #         # auc_axes[experiment].scatter(dx[~dx[col_name]]['time'], dx[~dx[col_name]]['score_observed'],
    #         #            marker='x', color=experiment_palette[experiment])
    #         if plot_observed:
    #             auc_axes[experiment].plot(dx['time'], dx['score_observed'], c=experiment_palette[experiment], ls=':')
    #
    #         auc_axes[experiment].scatter(dx[dx[col_name]]['time'], dx[dx[col_name]][score_col],
    #                    marker='o', color=experiment_palette[experiment], s=15)
    #
    #         auc_axes[experiment].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #         if warp :
    #             median_reaction_times = out['pars']['median_reaction_times']
    #             for modality in modalities:
    #                 auc_axes[experiment].axvline(median_reaction_times[experiment.split('_')[-1]],
    #                                                  c=modality_palette[experiment.split('_')[-1]],
    #                                                  ls='--')
    #         auc_axes[experiment].set_xlabel('Time [ms]')
    #
    #         auc_axes[experiment].set_xlim([times[0], times[-1]])
    #         auc_axes[experiment].set_ylim([0, 1])
    #         auc_axes[experiment].axhline(0.5, ls=':', c=sns.xkcd_rgb['grey'])
    #
    #
    #     auc_axes[experiments[0]].set_ylabel('AUC score')
    #
    #     sns.despine()
    #
    #     plot_name = 'psth_{}_{}_{}.{}'.format(settings_name, auc_experiment, unit_id, plot_format)
    #     f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    #
    #
    #     plt.close()
