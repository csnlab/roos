import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *
from scipy.ndimage import gaussian_filter1d


"""
imshow matrix of AUC values. Possibly outdated. 
"""


plot_format = 'png'

# combos = [['nov9_stim_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_resp_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_stim_fast_50', 'target_vs_distractor_fast'],
#           ['nov9_resp_fast_50', 'target_vs_distractor_fast']]



combos = [['nov9_stim', 'target_vs_distractor']]


combos = [['nov10', 'target_vs_distractor']]


combos = [['nov10_20_trials', 'target_vs_distractor']]




# TIME WITH WHICH WE COMPARE FOR OVERLAP
t0 = 45

#settings_name = 'nov9_stim_slow_30'
#combo = 'target_vs_distractor_slow'

for settings_name, combo in combos:

    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    n_boot = 200

    # EXCLUDE AUDITORY

    df = out['auc_scores']
    df = df[[False if i[-1] == 'A' else True for i in df['experiment']]]

    df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(df['session_id'], df['unit_ind'])]

    dn = pd.DataFrame(columns=['time', 'experiment', 'overlap', 'new_cells'])

    for ind0, experiment in enumerate(df['experiment'].unique()):

        df_exp = df[df['experiment'] == experiment]
        unit_ids = df_exp['unit_id'].unique()
        n_units = unit_ids.shape[0]
        n_time_points = df_exp['time_bin'].unique().shape[0]

        M = np.zeros([n_units, n_time_points])

        for ind1, unit in enumerate(unit_ids):
            for ind2, time in enumerate(df['time'].unique()):


                M[ind1, ind2] = df_exp[(df_exp['time'] == time) &
                                 (df_exp['experiment'] == experiment) &
                                 (df_exp['unit_id'] == unit)]['is_sign_01']


        t0_indx = np.where(df['time'].unique() == t0)[0][0]


        for i in range(M.shape[0]):
            M[i, :] = gaussian_filter1d(M[i, :], sigma=1)

        indx = np.argmax(M, axis=1).argsort()

        unit_extent = 0, n_units
        time_extent = df['time'].min(), df['time'].max()

        f, ax = plt.subplots(1, 1, figsize=[5, 5])

        ax.imshow(M[indx, :], extent=[df['time'].min(), df['time'].max(),  0, n_units],
                  aspect=1.3)
        ax.axvline(x=t0, c=sns.xkcd_rgb['grey'], ls='--')
        ax.set_title('{}\n({})'.format(combo_labels[combo], experiment))
        ax.set_ylabel('Neurons')
        ax.set_xlabel('Time [ms]')
        sns.despine()
        plt.tight_layout()


        plot_name = 'auc_imshow_{}_{}_{}.{}'.format(experiment, combo, settings_name, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



