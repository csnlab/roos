import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.metrics import roc_auc_score
import resampy
import quantities as pq
import scipy.signal

settings_name = 'nov14_stim'
align_to = 'stimulus'
binsize = 50
min_units = 1

if __name__ == '__main__':

    trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                       align_to='stimulus',
                                                       add_reaction_speed_quantiles=False,
                                                       enforce_min_max_reaction_times=True)
    session_ids = trial_df['session_id'].unique()

    warped_trials = {}
    median_reaction_times = {}
    warped_time_bins = {}
    time_bin_centers = np.array(time_bin_centers)
    max_time = (time_bin_centers[-1]-100) * pq.ms

    for modality in modalities:

        sel_trials = trial_df[trial_df['modality'] == modality]
        median_reaction_time = sel_trials['reaction_time'].median()
        median_reaction_time = (median_reaction_time * pq.s).rescale(pq.ms)
        median_reaction_times[modality] = median_reaction_time

        time_bins_pre = time_bin_centers[time_bin_centers <= 0]
        time_bins_reaction = time_bin_centers[ np.logical_and(time_bin_centers > 0, time_bin_centers <= median_reaction_time)]
        time_bins_post = time_bin_centers[time_bin_centers > median_reaction_time]

        n_resample_bins_reaction = time_bins_reaction.shape[0]
        n_resample_bins_pre = time_bins_pre.shape[0]
        n_resample_bins_post = time_bins_post.shape[0]

        for i, row in sel_trials.iterrows():
            tid = row['trial_id']
            # if tid == '0_116':
            #     raise ValueError
            reaction_time = (row['reaction_time'] * pq.s).rescale(pq.ms)

            time_bins_pre = time_bin_centers[time_bin_centers <= 0]
            time_bins_reaction_trial = time_bin_centers[np.logical_and(time_bin_centers > 0,time_bin_centers <= reaction_time)]
            time_bins_post_trial = time_bin_centers[time_bin_centers > reaction_time]

            n_time_bins_reaction_trial = time_bins_reaction_trial.shape[0]
            n_time_bins_post_trial = time_bins_post_trial.shape[0]

            if reaction_time < max_time:

                warped_trial = []
                for neuron_indx in np.arange(trial_data[tid].shape[1]):

                    # if tid =='0_116' and neuron_indx == 13:
                    #     raise ValueError

                    rate = trial_data[tid][:, neuron_indx].astype(float)
                    rate_pre = rate[time_bin_centers < 0]

                    rate_reaction = rate[np.logical_and(time_bin_centers >= 0, time_bin_centers < reaction_time)]
                    # rate_reaction_warped, _ = scipy.signal.resample(rate_reaction, num=n_resample_bins_reaction,
                    #                                                      t=time_bins_reaction_trial)

                    xp = np.arange(n_time_bins_reaction_trial)
                    x = np.linspace(0, n_time_bins_reaction_trial-1, n_resample_bins_reaction)
                    rate_reaction_warped = np.interp(x=x, xp=xp, fp=rate_reaction)

                    rate_post = rate[time_bin_centers >= reaction_time]
                    # rate_post_warped, _ = scipy.signal.resample(rate_post, num=n_resample_bins_post,
                    #                                                          t=time_bins_post_trial)

                    xp = np.arange(n_time_bins_post_trial)
                    x = np.linspace(0, n_time_bins_post_trial-1, n_resample_bins_post)
                    rate_post_warped = np.interp(x=x, xp=xp, fp=rate_post)

                    warped = np.hstack([rate_pre, rate_reaction_warped, rate_post_warped])
                    # the resampling can induce negative values (??), remove

                    warped_trial.append(warped[:, np.newaxis])

                warped_trial = np.hstack(warped_trial)
                warped_trials[tid] = warped_trial

    warped_ids = list(warped_trials.keys())
    trial_df = trial_df[np.isin(trial_df['trial_id'], warped_ids)]

    trial_df.index = trial_df['trial_id']

    trial_id = '1_267'
    neuron_indx = 6
    f, ax = plt.subplots(1, 2, figsize=(10, 5))
    ax[0].plot(time_bin_centers, trial_data[trial_id][:, neuron_indx], c='k')
    ax[0].scatter(time_bin_centers, trial_data[trial_id][:, neuron_indx], c='k')
    ax[0].axvline(0, c='k', ls='--')
    ax[0].axvline(trial_df.loc[trial_id]['reaction_time']*1000, c='r', ls='--')

    ax[1].plot(time_bin_centers, warped_trials[trial_id][:, neuron_indx], c='k')
    ax[1].scatter(time_bin_centers, warped_trials[trial_id][:, neuron_indx], c='k')
    ax[1].axvline(0, c='k', ls='--')
    ax[1].axvline(median_reaction_times[trial_df.loc[trial_id]['modality']], c='r', ls='--')




def warp_trials(trial_data, trial_df, time_bin_centers,
                warp_per_modality=False, keep_misses=True, motion_data=None):

    warped_trials = {}
    warped_movement = {}
    median_reaction_times = {}
    time_bin_centers = np.array(time_bin_centers)
    #max_time = (time_bin_centers[-1]-100) * pq.ms

    if not keep_misses:
        excluded_miss = trial_df.shape[0] - (trial_df['hit_miss'] == 'hit').sum()
        print('Excluding {} miss trials'.format(excluded_miss))
        trial_df = trial_df[trial_df['hit_miss'] == 'hit']


    # use a median reaction per trial type or a global one
    if warp_per_modality:
        for modality in modalities :
            # get the median reaction time per modality
            sel_trials = trial_df[trial_df['modality'] == modality]
            median_reaction_time = sel_trials['reaction_time'].median()
            median_reaction_time = (median_reaction_time * pq.s).rescale(pq.ms)
            median_reaction_times[modality] = median_reaction_time

    else:
        median_reaction_time = trial_df['reaction_time'].median()
        median_reaction_time = (median_reaction_time * pq.s).rescale(pq.ms)
        for modality in modalities :
            median_reaction_times[modality] = median_reaction_time


    # or reaction time per modality
    for modality in modalities:
        # get the median reaction time per modality
        sel_trials = trial_df[trial_df['modality'] == modality]

        time_bins_reaction = time_bin_centers[ np.logical_and(time_bin_centers > 0, time_bin_centers <= median_reaction_time)]
        time_bins_post = time_bin_centers[time_bin_centers > median_reaction_time]

        n_resample_bins_reaction = time_bins_reaction.shape[0]
        n_resample_bins_post = time_bins_post.shape[0]

        for i, row in sel_trials.iterrows():
            tid = row['trial_id']

            reaction_time = (row['reaction_time'] * pq.s).rescale(pq.ms)

            time_bins_reaction_trial = time_bin_centers[np.logical_and(time_bin_centers > 0,time_bin_centers <= reaction_time)]
            time_bins_post_trial = time_bin_centers[time_bin_centers > reaction_time]

            n_time_bins_reaction_trial = time_bins_reaction_trial.shape[0]
            n_time_bins_post_trial = time_bins_post_trial.shape[0]

            if row['hit_miss'] == 'hit':
                warped_trial = []
                for neuron_indx in np.arange(trial_data[tid].shape[1]):

                    rate = trial_data[tid][:, neuron_indx].astype(float)

                    # rate before stimulus onset stays unstretched
                    rate_pre = rate[time_bin_centers < 0]

                    # stretch rate before response
                    rate_reaction = rate[np.logical_and(time_bin_centers >= 0, time_bin_centers < reaction_time)]
                    xp = np.arange(n_time_bins_reaction_trial)
                    x = np.linspace(0, n_time_bins_reaction_trial-1, n_resample_bins_reaction)
                    rate_reaction_warped = np.interp(x=x, xp=xp, fp=rate_reaction)

                    # stretch rate after response
                    rate_post = rate[time_bin_centers >= reaction_time]
                    xp = np.arange(n_time_bins_post_trial)
                    x = np.linspace(0, n_time_bins_post_trial-1, n_resample_bins_post)
                    rate_post_warped = np.interp(x=x, xp=xp, fp=rate_post)

                    # combine the different parts
                    warped = np.hstack([rate_pre, rate_reaction_warped, rate_post_warped])[:, np.newaxis]
                    warped_trial.append(warped)

                warped_trial = np.hstack(warped_trial)
                warped_trials[tid] = warped_trial

                # warp the motion data if we have it
                if motion_data is not None:
                    motion = motion_data[tid]

                    # movement before stimulus onset stays unstretched
                    motion_pre = motion[time_bin_centers < 0]

                    # stretch movement before response
                    motion_reaction = motion[np.logical_and(time_bin_centers >= 0, time_bin_centers < reaction_time)]
                    xp = np.arange(n_time_bins_reaction_trial)
                    x = np.linspace(0, n_time_bins_reaction_trial-1, n_resample_bins_reaction)
                    motion_reaction_warped = np.interp(x=x, xp=xp, fp=motion_reaction)

                    # stretch movement after response
                    motion_post = motion[time_bin_centers >= reaction_time]
                    xp = np.arange(n_time_bins_post_trial)
                    x = np.linspace(0, n_time_bins_post_trial-1, n_resample_bins_post)
                    motion_post_warped = np.interp(x=x, xp=xp, fp=motion_post)

                    # combine the different parts
                    motion_warped = np.hstack([motion_pre, motion_reaction_warped, motion_post_warped])#[:, np.newaxis]
                    warped_movement[tid] = motion_warped


            elif row['hit_miss'] == 'miss' and keep_misses:
                # compute as above and check that its the same thing

                # non_warped_trial = []
                # for neuron_indx in np.arange(trial_data[tid].shape[1]):
                #     rate = trial_data[tid][:, neuron_indx].astype(float)[:, np.newaxis]
                #     non_warped_trial.append(rate)
                #
                # non_warped_trial = np.hstack(non_warped_trial)
                # warped_trials[tid] = non_warped_trial
                # np.testing.assert_array_equal(non_warped_trial, trial_data[tid].astype(float))

                # it is so we can just do
                warped_trials[tid] = trial_data[tid]
                if motion_data is not None:
                    warped_movement[tid] = motion_data[tid]

            else:
                pass

    warped_ids = list(warped_trials.keys())
    trial_df = trial_df[np.isin(trial_df['trial_id'], warped_ids)]

    if motion_data is not None:
        return warped_trials, warped_movement, trial_df, median_reaction_times
    else:
        return warped_trials, trial_df, median_reaction_times


