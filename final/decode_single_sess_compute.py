import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from utils import *
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from utils import log2_likelihood
from sklearn.preprocessing import LabelEncoder, minmax_scale
from sklearn.ensemble import RandomForestClassifier

"""
Simple decoding per session
- uses logistic regression or random forest
- option to balance the data. if it does not balance, it still shuffles the order
of the trials
- computes a balanced accuracy score
- decodes from spikes and option to decode from confounds
"""


# this string is used to identify the particular combination of parameters
# that is used to run the encoding
settings_name = 'apr28'

# specify the contrasts for which to run the decoding
experiments = ['correct_vs_incorrect_V',
               'correct_vs_incorrect_M',
               'correct_vs_incorrect_A',
               'hit_miss_V',
               'hit_miss_M',
               'hit_miss_A',
               'target_vs_distractor_V',
               'target_vs_distractor_M',
               'target_vs_distractor_A']

# DATA PARAMETERS
# set parameters
align_to = 'stimulus'
binsize = 200
# minimum number of neurons required for a session
min_units = 8
# minimum number of trials required for each class of the target variable
min_trials_per_class = 20
min_tot_trials = 60
# time window
start_time_in_ms = -500
end_time_in_ms = 1500

warp = True
# if True, warp using the median reaction time computed separately for each
# stimulus modality
warp_per_modality = True

# DECODING PARAMETERS
# if True, decode the target of interest also using the confound (orofacial
# movement and other trial labels) as a control
decode_from_confound = False
decoder = 'random_forest' # 'random_forest' of 'logistic_regression'
# number of trees for the random forest
n_estimators = 200
# logistic regression parameters
tune_C = False
lr_penalty = 'l1' #'l1'
lr_solver = 'liblinear'#'liblinear'#'lbfgs'

# whether to balance the data by random subsampling of the majority class
subsample_majority_class = True
# cross-validation:
n_splits = 5
n_repeats = 5

seed = 92

# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, motion_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

# consider late hits as hits
trial_df.loc[trial_df['hit_miss'] == 'late', 'hit_miss'] = 'miss'

# remove early trials (marked as early in the hit_miss column)
trial_df = trial_df[np.isin(trial_df['hit_miss'], ['hit', 'miss'])]
trial_data = {id : trial_data[id] for id in trial_df['trial_id']}
motion_data = {id : motion_data[id] for id in trial_df['trial_id']}

# consider misses to be incorrect (in the sense of no reward)
trial_df.loc[trial_df['hit_miss'] == 'miss', 'response'] = 0



if warp:
    trial_data, motion_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True,
                                                              motion_data=motion_data)
else:
    median_reaction_times = {}


for experiment in experiments:

    # --- OUTPUT FILES ---------------------------------------------------------
    output_file_name = 'decode_single_sess_setting_{}_{}.pkl'.format(settings_name,
                                                         experiment)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'decode_single_sess',
                                 settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    pars_file_name = 'parameters_decode_single_sess_setting_{}_{}.pkl'.format(settings_name,
                                                                  experiment)
    pars_full_path = os.path.join(output_folder, pars_file_name)

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)


    # --- SELECT SESSIONS AND TRIALS ------------------------------------------
    session_df, sdf, dp = select_data_experiment(experiment, trial_df)


    n_time_bins_per_trial = trial_data['0_0'].shape[0]

    selsess_df = session_df[(session_df['n1'] >= min_trials_per_class) &
                                (session_df['n2'] >= min_trials_per_class) &
                                (session_df['n_units'] >= min_units)]
    selsess_df['tot_n_trials'] = selsess_df['n1'] + selsess_df['n2']
    selsess_df = selsess_df[selsess_df['tot_n_trials'] >= min_tot_trials]
    selected_session_ids = selsess_df['session_id'].__array__()

    tot_n_units = selsess_df['n_units'].sum()

    print('\n\nEXPERIMENT: {}'.format(experiment))
    print('\n # units: {}'.format(tot_n_units))
    print(selsess_df)

    df = pd.DataFrame(columns=['animal_id', 'session_id',
                                'experiment', 'features',
                                'repeat', 'time', 'score', 'n_neurons'])

    for session_id in selected_session_ids:

        animal_id = trial_df[trial_df['session_id'] == session_id]['animal_id'].unique()
        assert len(animal_id) == 1
        animal_id = animal_id[0]

        print('______ decoding session {}, animal {}'.format(session_id,
                                                             animal_id))

        ds = dp[dp['session_id'] == session_id]

        ds['target'].__array__()

        tdf_sess = sdf[sdf['session_id'] == session_id]

        # miss trials have nan response, set to 0 (no reward)
        tdf_sess.loc[tdf_sess['hit_miss'] == 'miss', 'response'] = 0

        np.testing.assert_array_equal(tdf_sess['trial_id'], ds['trial_numbers'])
        tdf_sess.index = tdf_sess['trial_id']

        d0 = ds[ds['target'] == 0]
        d1 = ds[ds['target'] == 1]

        n_trials_minority_class = min(d0.shape[0], d1.shape[0])

        for repeat in range(n_repeats):

            if subsample_majority_class :
                t0 = np.random.default_rng(seed=repeat).choice(d0['trial_numbers'],
                                          size=n_trials_minority_class,
                                          replace=False)
                t1 = np.random.default_rng(seed=repeat).choice(d1['trial_numbers'],
                                      size=n_trials_minority_class,
                                      replace=False)

                resampled_trials = np.hstack([t0, t1])

                y = np.hstack([np.zeros(n_trials_minority_class),
                               np.ones(n_trials_minority_class)]).astype(int)

            else :
                resampled_trials = np.hstack([d0['trial_numbers'], d1['trial_numbers']])

                y = np.hstack([np.zeros(d0['trial_numbers'].shape[0]),
                               np.ones(d1['trial_numbers'].shape[0])]).astype(int)

            sel_trial_ids = resampled_trials

            random_index = np.random.default_rng(seed=repeat).permutation(np.arange(len(sel_trial_ids)))

            for time_bin in range(n_time_bins_per_trial) :

                time = time_bin_centers[time_bin]

                if time >= start_time_in_ms and time <= end_time_in_ms :

                    X_full = np.vstack([trial_data[t][time_bin, :] for t in sel_trial_ids])
                    y_full = y.copy()

                    X_full = X_full[random_index,:]
                    y_full = y_full[random_index]

                    if decode_from_confound:
                        # BUILD THE CONFOUND MATRIX
                        C_movement = np.array([motion_data[t][time_bin] for t in sel_trial_ids]).reshape(-1, 1)
                        C_correct = np.array([tdf_sess.loc[t]['response'] for t in sel_trial_ids]).reshape(-1, 1)

                        C_target = np.array([tdf_sess.loc[t]['target'] for t in sel_trial_ids])
                        C_target = LabelEncoder().fit_transform(C_target).reshape(-1, 1)

                        if np.isin(experiment, target_vs_distractor_experiments):
                            C_full = np.hstack([C_movement, C_correct])

                        elif np.isin(experiment, correct_vs_incorrect_experiments):
                            C_full = np.hstack([C_movement, C_target])

                        elif np.isin(experiment, hit_miss_experiments):
                            C_full = np.hstack([C_movement, C_target, C_correct])

                        C_full = C_full[random_index,:]

                    T = X_full.shape[0]
                    center_point = T // 2

                    kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                            random_state=repeat)

                    iterable = kfold.split(X_full, y_full)

                    kfold_scores_spikes = []
                    kfold_scores_confound = []

                    for fold, (training_ind, testing_ind) in enumerate(iterable) :

                        X_train = X_full[training_ind, :]
                        X_test = X_full[testing_ind, :]
                        y_train = y_full[training_ind]
                        y_test = y_full[testing_ind]

                        # standardize the neural data
                        ss = StandardScaler()
                        X_train = ss.fit_transform(X_train)
                        X_test = ss.transform(X_test)

                        # --- decode from spikes ---
                        if decoder == 'logistic_regression':
                            model = LogisticRegression(penalty=lr_penalty,
                                                       solver=lr_solver)
                        elif decoder == 'random_forest':
                            model = RandomForestClassifier(n_estimators=n_estimators)

                        model.fit(X_train, y_train)
                        y_pred = model.predict(X_test)

                        fold_score_spikes = balanced_accuracy_score(y_test, y_pred)
                        kfold_scores_spikes.append(fold_score_spikes)

                        if decode_from_confound:
                            # standardize the movement confound
                            C_train = C_full[training_ind, :]
                            C_test = C_full[testing_ind, :]
                            ss = StandardScaler()
                            C_train[0, :] = ss.fit_transform(C_train[0, :].reshape(-1, 1)).flatten()
                            C_test[0, :] = ss.transform(C_test[0, :].reshape(-1, 1)).flatten()

                            # --- decode from confound ---
                            if decoder == 'logistic_regression':
                                model = LogisticRegression(penalty=lr_penalty,
                                                           solver=lr_solver)
                            elif decoder == 'random_forest':
                                model = RandomForestClassifier(n_estimators=150)

                            model.fit(C_train, y_train)
                            #print(y_test)
                            y_pred = model.predict(C_test)
                            #print(y_pred)

                            fold_score_confound = balanced_accuracy_score(y_pred, y_test)
                            #print(fold_score_confound)

                            kfold_scores_confound.append(fold_score_confound)


                    mean_score_spikes = np.mean(kfold_scores_spikes)

                    row = [animal_id, session_id,
                           experiment, 'spikes', repeat,
                           time, mean_score_spikes, X_full.shape[1]]

                    df.loc[df.shape[0], :] = row

                    if decode_from_confound:
                        mean_score_confound = np.mean(kfold_scores_confound)
                        #print('> > > {}\n'.format(mean_score_confound))

                        row = [animal_id, session_id,
                               experiment, 'confound', repeat,
                               time, mean_score_confound, X_full.shape[1]]

                        df.loc[df.shape[0], :] = row


    # df['q1'] = np.nan
    # df['q2'] = np.nan
    # df['significant'] = np.nan

    df = df.groupby(['animal_id', 'session_id', 'experiment', 'features',
                    'time', 'n_neurons']).mean().reset_index()
    df = df.drop(columns=['repeat'])

    pars = {'settings_name' : settings_name,
            'experiment' : experiment,
            'seed' : seed,
            'binsize_in_ms' : binsize,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'sessions' : selected_session_ids,
            'n_splits' : n_splits,
            # not strictly user defined
            'selected_trials' : dp,
            'n_time_bins_per_trial' : n_time_bins_per_trial,
            'warp' : warp,
            'warp_per_modality' : warp_per_modality,
            'median_reaction_times' : median_reaction_times,
            'subsample_majority_class' : subsample_majority_class,
            'decoder' : decoder}
            #'time_bin_edges' : time_bin_edges,
            #'time_bin_centers' : time_bin_centers}

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))

    print('Saving parameters to {}'.format(pars_full_path))
    pickle.dump(pars, open(pars_full_path, 'wb'))



