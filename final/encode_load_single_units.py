import os
from constants import *
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from plotting_style import *
import pandas as pd

# specify the encoding run
settings_name = 'aug13'

# specify a list of units for which to generate the plots
# selected_unit_ids = ['E2R-008_4_19',
#                      'E2R-008_4_9',
#                      'E2R-009_5_4',
#                      'E2R-009_7_7',
#                      'E2R-009_7_15',
#                      'E2R-008_4_8',
#                      'E2R-008_4_22',
#                      'E2R-006_2_0',
#                      'E2R-006_1_11',
#                      'E2R-006_1_14']

# alternatively, if None, generate plots for all the cells for which the encoding
# was run
selected_unit_ids = None

selected_unit_ids = ['E2R-008_4_19']

# specify which plots to generate
plot_overall_encoding_concatenated_barplot = True
plot_overall_encoding_barplot              = False
plot_average_predictions_full_model        = False
plot_average_predictions_partial_models    = True
plot_example_trials_partial_models         = False
plot_unique_encoding_barplot               = False
plot_unique_encoding_barplot_concatenated  = True

# these are the sets of predictors that we plot independently
#predictor_sets_partial = ['full', 'stimulus', 'behavior']

predictor_sets_partial = ['full',
                          'visual',
                          'audio',
                          'behavior']

# these are the sets of predictors for which we compute the unique information
# by comparing with the full model
predictor_sets_unique = ['unique_stimulus',
                         'unique_behavior']

selected_unit_ids = ['E2R-006_1_14', # more subtle hit miss but also visual
                     'E2R-008_4_9', # hit miss
                     'E2R-008_4_9'] # late outcome (outside of range)

selected_unit_ids = ['E2R-008_4_9'] # late outcome (outside of range)


# option to use the standard explained variance, but should be avoided
# because it's theoretically not correct
metric = 'PR2'

# if concatenated=True, use the PR2 computed on the concatenated trials for
# the global encoding quality
concatenated = True

# --- PLOTTING ---

# set plot format to 'svg' for vector images
plot_format = 'png'
# dpi specifies the "quality" of the plots, increase for high definition png
dpi = 150

# define where the plots will be saved, and create that folder if it does not
# exist
plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encoding',
                            settings_name, 'single_units')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)

# ------------------------------------------------------------------------------

if selected_unit_ids is None:
    # run over all sessions
    session_ids = [str(i) for i in np.arange(19)]

else:
    session_ids = []
    for unit_id in selected_unit_ids:
        session_id = unit_id.split('_')[1]
        session_ids.append(session_id)
    session_ids = np.unique(session_ids)

    selected_units_dict = {s : [] for s in session_ids}
    for unit_id in selected_unit_ids:
        selected_units_dict[unit_id.split('_')[1]].append(unit_id)


if metric == 'EV':
    metric_epoch_1 = 'EV_epoch_1'
    metric_epoch_2 = 'EV_epoch_2'
    estimator = 'median'
elif metric == 'PR2':
    metric_epoch_1 = 'PR2_epoch_1'
    metric_epoch_2 = 'PR2_epoch_2'
    estimator = 'median'
else:
    raise ValueError


for sessn, session_id in enumerate(session_ids):

    output_file_name = 'encode_setting_{}_{}.pkl'.format(settings_name, session_id )
    output_folder = os.path.join(DATA_FOLDER, 'results', 'encode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    try:
        output = pickle.load(open(output_full_path, 'rb'))
    except FileNotFoundError:
        print('NO ENCODING RESULTS FOR SESSION {}'.format(session_id))
        continue

    pars = output['pars']
    time_bin_centers = pars['time_bin_centers']
    predictor_sets = pars['predictor_sets']

    if selected_unit_ids is None:
        selected_unit_ids_session = list(output['dfs'].keys())
    else:
        selected_unit_ids_session = selected_units_dict[session_id]

    trial_df_session = output['trial_df']
    epoch_1 = pars['epoch_1']
    epoch_2 = pars['epoch_2']
    predictor_sets_order  = list(predictor_sets.keys())

    columns = ['target', 'target', 'response', 'response',
               'hit_miss', 'hit_miss']  # 'response_side', 'response_side']

    vals = ['target', 'distractor', '0', '1', 'hit', 'miss']

    for unit_id in selected_unit_ids_session:

        df = output['dfs'][unit_id]
        dfc = output['dfc'][unit_id]
        for col in ['PR2_epoch_1', 'PR2_epoch_2']:
            dfc[col] = dfc[col].astype(float)

        spikes_unit = output['spikes'][unit_id]
        predicted_firing_rate = output['predictions'][unit_id]

        # IMPORTANT: average scores across trial repeats
        cols = ['animal_id', 'session_id', 'unit_id', 'trial_id', 'modality',
                'response', 'response_side', 'hit_miss', 'target', 'predictor_set']
        df = df.groupby(cols).mean().reset_index()

        # # possibility: average across folds (not repeats)
        # cols = ['animal_id', 'session_id', 'unit_id', 'modality', 'repeat', 'predictor_set']
        # dfc = dfc.groupby(cols).mean().reset_index()

        # --- OVERALL ENCODING BARPLOT [CONCATENATED] ---
        if plot_overall_encoding_concatenated_barplot :
            f, ax = plt.subplots(2, 2, figsize=[7, 7], sharey='row')

            sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_1,
                        ax=ax[0, 0], alpha=0.7, color='grey',
                        order=predictor_sets_partial, estimator=estimator)

            sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_2,
                        ax=ax[0, 1], alpha=0.7, color='grey',
                        order=predictor_sets_partial, estimator=estimator)
            ax[0, 0].set_title('Encoding evaluated in epoch 1\n'
                               '{} - {} ms'.format(epoch_1[0], epoch_1[1]),
                               fontsize=11)
            ax[0, 1].set_title('Encoding evaluated in epoch 2\n'
                               '{} - {} ms'.format(epoch_2[0], epoch_2[1]),
                               fontsize=11)

            sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_1,
                        hue='modality', order=predictor_sets_partial,
                        ax=ax[1, 0], palette=modality_palette, alpha=0.7,
                        estimator=estimator)

            sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_2,
                        hue='modality', order=predictor_sets_partial,
                        ax=ax[1, 1], palette=modality_palette, alpha=0.7,
                        estimator=estimator)
            ax[1, 1].legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'encode_{}_quality_barplot_concatenated.{}'.format(unit_id,
                                                              plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)

        # --- OVERALL ENCODING BARPLOT ---
        if plot_overall_encoding_barplot:
            f, ax = plt.subplots(2, 2, figsize=[7, 7], sharey='row')

            sns.barplot(data=df, x='predictor_set', y=metric_epoch_1,
                        ax=ax[0,0], alpha=0.7, color='grey',
                        order=predictor_sets_partial, estimator=estimator)

            sns.barplot(data=df, x='predictor_set', y=metric_epoch_2,
                        ax=ax[0,1], alpha=0.7, color='grey',
                        order=predictor_sets_partial, estimator=estimator)
            ax[0, 0].set_title('Encoding evaluated in epoch 1\n'
                               '{} - {} ms'.format(epoch_1[0], epoch_1[1]), fontsize=11)
            ax[0, 1].set_title('Encoding evaluated in epoch 2\n'
                               '{} - {} ms'.format(epoch_2[0], epoch_2[1]), fontsize=11)

            sns.barplot(data=df, x='predictor_set', y=metric_epoch_1,
                        hue='modality', order=predictor_sets_partial,
                        ax=ax[1, 0], palette=modality_palette, alpha=0.7,
                        estimator=estimator)

            sns.barplot(data=df, x='predictor_set', y=metric_epoch_2,
                        hue='modality', order=predictor_sets_partial,
                        ax=ax[1, 1], palette=modality_palette, alpha=0.7,
                        estimator=estimator)
            ax[1, 1].legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'encode_{}_quality_barplot.{}'.format(unit_id, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)


        # --- PLOT PREDICTIONS AVERAGED ACROSS TRIAL TYPES FOR FULL MODEL ---
        if plot_average_predictions_full_model:
            f, ax = plt.subplots(6, 3, figsize=[11, 10],
                                 sharex=True, sharey=True)

            for j, modality in enumerate(modalities):
                for i, (col, val) in enumerate(zip(columns, vals)):

                    # select only the full model
                    dx = df[(df['modality'] == modality) & (df[col] == val)
                            & (df['predictor_set'] == 'full')]

                    tids = dx['trial_id'].unique()
                    print(modality, col, val, len(tids))

                    if len(tids) >= 5:

                        spikes = np.vstack([spikes_unit[t] for t in tids])
                        spikes_mean = np.mean(spikes, axis=0)
                        spikes_std = spikes.std(axis=0)

                        # to get the trial predictions, first average the different
                        # predictions for each trial (on per kfold repeat)
                        predictions = [np.vstack(predicted_firing_rate['full'][t]).mean(axis=0) for t in tids]
                        predictions = np.vstack(predictions)
                        predictions_mean = np.mean(predictions, axis=0)
                        predictions_std = predictions.std(axis=0)

                        average_pr2_e1 = dx['PR2_epoch_1'].mean()
                        average_pr2_e2 = dx['PR2_epoch_2'].mean()

                        ax[i, j].plot(time_bin_centers, spikes_mean,
                                      c=sns.xkcd_rgb['dark grey'],label='Average firing rate')
                        ax[i, j].fill_between(time_bin_centers,
                                              spikes_mean-spikes_std/2,
                                              spikes_mean+spikes_std/2,
                                              color=sns.xkcd_rgb['dark grey'], alpha=0.3,
                                               linewidth=0)

                        ax[i, j].plot(time_bin_centers, predictions_mean,
                                   c=modality_palette[modality], label='Average prediction')
                        ax[i, j].fill_between(time_bin_centers,
                                              predictions_mean-predictions_std/2,
                                              predictions_mean+predictions_std/2,
                                   color=modality_palette[modality], alpha=0.5,
                                    linewidth=0)
                        ax[i, j].axvline(0, c='grey', ls='--')

                        ax[i, j].set_title('{} - {}: {}\nMean PR2 (full model, {} - {}ms): '
                                           '{:.2f}'.format(modality, col, val,
                                                           epoch_1[0], epoch_1[1],
                                                           average_pr2_e1),
                                           fontsize=10)
                        # ax[i, j].set_title('{} - {}: {}\nMean PR2 (500-1500 ms)={:.3f}'
                        #                    ''.format(modality, col, val,
                        #                              average_pr2_e1), fontsize=10)

                        ax[i, 0].set_ylabel('Firing rate (Hz.)')
                        ax[0, j].legend(fontsize=8)
            sns.despine()
            plt.tight_layout()

            plot_name = 'encode_{}_trial_averages_only_full.{}'.format(unit_id, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)

        # --- PLOT PREDICTIONS AVERAGED ACROSS TRIAL TYPES ALL MODELS ---
        if plot_average_predictions_partial_models:
            f, ax = plt.subplots(6, 3, figsize=[11, 10],
                                 sharex=True, sharey=True)

            for j, modality in enumerate(modalities):
                for i, (col, val) in enumerate(zip(columns, vals)):

                    # we get the true values from here
                    dx = df[(df['modality'] == modality) & (df[col] == val)
                            & (df['predictor_set'] == 'full')]
                    tids = dx['trial_id'].unique()

                    if len(tids) >= 5 :
                        spikes = np.vstack([spikes_unit[t] for t in tids])
                        spikes_mean = np.mean(spikes, axis=0)
                        spikes_std = spikes.std(axis=0)

                        ax[i, j].plot(time_bin_centers, spikes_mean,
                                      c=sns.xkcd_rgb['dark grey'],label='Average firing rate')
                        ax[i, j].fill_between(time_bin_centers,
                                              spikes_mean-spikes_std/2,
                                              spikes_mean+spikes_std/2,
                                              color=sns.xkcd_rgb['dark grey'], alpha=0.3,
                                               linewidth=0)

                    for predictor_set in predictor_sets_partial:

                        dxset = df[(df['modality'] == modality) & (df[col] == val)
                                    & (df['predictor_set'] == predictor_set)]

                        tidsset = dxset['trial_id'].unique()
                        # to get the trial predictions, first average the different
                        # predictions for each trial (on per kfold repeat)

                        if len(tidsset) >= 5:
                            predictions = [np.vstack(predicted_firing_rate[predictor_set][t]).mean(
                                                    axis=0) for t in tidsset]
                            predictions = np.vstack(predictions)
                            predictions_mean = np.mean(predictions, axis=0)
                            predictions_std = predictions.std(axis=0)

                            # average_pr2_e1 = dxset['PR2_epoch_1'].mean()
                            # average_pr2_e2 = dxset['PR2_epoch_2'].mean()

                            ax[i, j].plot(time_bin_centers, predictions_mean,
                                       c=modality_palette[modality],
                                       label='Average prediction ({} model)'.format(predictor_set),
                                       ls=predictor_set_ls[predictor_set])
                            # ax[i, j].fill_between(time_bin_centers,
                            #                       predictions_mean-predictions_std/2,
                            #                       predictions_mean+predictions_std/2,
                            #            color=modality_palette[modality], alpha=0.5,
                            #             linewidth=0)
                            ax[i, j].axvline(0, c='grey', ls='--')
                            ax[i, j].set_title('{} - {}: {}'.format(modality, col, val), fontsize=10)

                            # ax[i, j].set_title('{} - {}: {}\nMean PR2 (0-500 ms)={:.3f}'
                            #                    ''.format(modality, col, val, average_pr2_e1), fontsize=10)
                            #

                            ax[i, 0].set_ylabel('Firing rate (Hz.)')
                            ax[0, j].legend(fontsize=8)
                sns.despine()
            plt.tight_layout()

            plot_name = 'encode_{}_trial_averages.{}'.format(unit_id, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)

        # --- PLOT PREDICTIONS OF 6 RANDOMLY CHOSEN TRIALS, ALL MODELS ---
        if plot_example_trials_partial_models:
            ncols = 6
            selected_trial_ids = {}
            for modality in modalities:
                ids_mod = trial_df_session[(trial_df_session['modality'] == modality) &
                                           (trial_df_session['session_id'] == session_id)]['trial_id']
                ids_sel = np.random.choice(ids_mod, size=ncols,replace=False)
                selected_trial_ids[modality] = ids_sel


            f, ax = plt.subplots(ncols, 3, figsize=[11, 11],
                                 sharex=True, sharey=False)

            for j, modality in enumerate(modalities):
                ax[-1, j].set_xlabel('Time [ms]')
                ids_sel = selected_trial_ids[modality]
                for i, tid_plot in enumerate(ids_sel):

                    row = trial_df_session.loc[tid_plot]
                    if row['hit_miss'] == 'hit':
                        string = '{} - difficulty: {}, response: {}'.format(tid_plot, row['difficulty'],
                                                                       row['response'])
                    else:
                        string = '{} - difficulty: {}, miss'.format(tid_plot, row['difficulty'])


                    mean_score_e1_full = df[(df['trial_id'] == tid_plot) &
                                       (df['predictor_set'] == 'full')]['PR2_epoch_1'].iloc[0]
                    mean_score_e1_stim = df[(df['trial_id'] == tid_plot) &
                                       (df['predictor_set'] == 'stimulus')]['PR2_epoch_1'].iloc[0]
                    mean_score_e1_behav = df[(df['trial_id'] == tid_plot) &
                                       (df['predictor_set'] == 'behavior')]['PR2_epoch_1'].iloc[0]
                    title = '{}' \
                            '\nPR2 score full model ({} - {}ms): {:.2f}' \
                            '\nPR2 score stim. model ({} - {}ms): {:.2f}' \
                            '\nPR2 score behav. model ({} - {}ms): {:.2f}' \
                            ''.format(string, epoch_1[0], epoch_1[1], mean_score_e1_full,
                                      epoch_1[0], epoch_1[1], mean_score_e1_stim,
                                      epoch_1[0], epoch_1[1], mean_score_e1_behav)
                    ax[i, j].set_title(title, fontsize=9)

                    ax[i, j].plot(time_bin_centers, spikes_unit[tid_plot],
                                  c='k', label='Firing rate')

                    for predictor_set in predictor_sets_partial:
                        pp = predicted_firing_rate[predictor_set][tid_plot]
                        px = np.vstack([p for p in pp])
                        stdx = px.std(axis=0)
                        ax[i, j].plot(time_bin_centers, px.mean(axis=0),
                                   c=modality_palette[modality],
                                   label='Average prediction {}'.format(predictor_set),
                                      ls=predictor_set_ls[predictor_set])
                        # ax[i, j].fill_between(time_bin_centers, px.mean(axis=0)-stdx/2, px.mean(axis=0)+stdx/2,
                        #            color=modality_palette[modality], alpha=0.5,
                        #             linewidth=0)

                    ax[i, j].axvline(row['reaction_time'],
                                     c=modality_palette[modality],
                                     ls='--')

                    ax[i, j].axvline(0, c='grey', ls='--')
                    ax[0, j].legend(fontsize=7)

            sns.despine()
            plt.tight_layout()


            plot_name = 'encode_{}_example_trials.{}'.format(unit_id, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)





        if plot_unique_encoding_barplot:

            dfull = df[df['predictor_set'] == 'full']
            dfull.index = dfull['trial_id']
            assert dfull['trial_id'].value_counts().max() == 1
            dfull = dfull.drop('trial_id', axis=1)

            du = []
            for predictor_set in predictor_sets_unique :

                dunique = df[df['predictor_set'] == predictor_set]
                dunique.index = dunique['trial_id']
                dunique = dunique.drop('trial_id', axis=1)

                for col in ['PR2_epoch_1', 'PR2_epoch_2'] :
                    pr2_without = dunique[col]
                    pr2_full = dfull.loc[dunique.index, col]
                    dunique[col] = pr2_full - pr2_without
                du.append(dunique)
            du = pd.concat(du)

            f, ax = plt.subplots(2, 2, figsize=[7, 7], sharey='row')

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_1,
                        ax=ax[0,0], alpha=0.7, color='grey',
                        order=predictor_sets_unique, estimator=estimator)

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_2,
                        ax=ax[0,1], alpha=0.7, color='grey',
                        order=predictor_sets_unique, estimator=estimator)
            ax[0, 0].set_title('Encoding evaluated in epoch 1\n'
                               '{} - {} ms'.format(epoch_1[0], epoch_1[1]), fontsize=11)
            ax[0, 1].set_title('Encoding evaluated in epoch 2\n'
                               '{} - {} ms'.format(epoch_2[0], epoch_2[1]), fontsize=11)

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_1,
                        hue='modality', order=predictor_sets_unique,
                        ax=ax[1, 0], palette=modality_palette, alpha=0.7,
                        estimator=estimator)

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_2,
                        hue='modality', order=predictor_sets_unique,
                        ax=ax[1, 1], palette=modality_palette, alpha=0.7,
                        estimator=estimator)
            ax[1, 1].legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'encode_{}_unique_barplot.{}'.format(unit_id, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)


        if plot_unique_encoding_barplot_concatenated:

            dfull = dfc[dfc['predictor_set'] == 'full']

            du = []
            for predictor_set in predictor_sets_unique :

                dunique = dfc[dfc['predictor_set'] == predictor_set]

                for col in ['PR2_epoch_1', 'PR2_epoch_2'] :
                    pr2_without = dunique[col]
                    pr2_full = dfull[col]
                    dunique[col] = pr2_full.values - pr2_without.values
                du.append(dunique)
            du = pd.concat(du).reset_index()


            f, ax = plt.subplots(2, 2, figsize=[7, 7], sharey='row')

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_1,
                        ax=ax[0,0], alpha=0.7, color='grey',
                        order=predictor_sets_unique, estimator=estimator)

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_2,
                        ax=ax[0,1], alpha=0.7, color='grey',
                        order=predictor_sets_unique, estimator=estimator)
            ax[0, 0].set_title('Encoding evaluated in epoch 1\n'
                               '{} - {} ms'.format(epoch_1[0], epoch_1[1]), fontsize=11)
            ax[0, 1].set_title('Encoding evaluated in epoch 2\n'
                               '{} - {} ms'.format(epoch_2[0], epoch_2[1]), fontsize=11)

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_1,
                        hue='modality', order=predictor_sets_unique,
                        ax=ax[1, 0], palette=modality_palette, alpha=0.7,
                        estimator=estimator)

            sns.barplot(data=du, x='predictor_set', y=metric_epoch_2,
                        hue='modality', order=predictor_sets_unique,
                        ax=ax[1, 1], palette=modality_palette, alpha=0.7,
                        estimator=estimator)
            ax[1, 1].legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'encode_{}_unique_barplot_concatenated.{}'.format(unit_id, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)


