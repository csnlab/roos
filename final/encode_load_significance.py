import os
from constants import *
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from plotting_style import *
import pandas as pd
from scipy.stats import wilcoxon, ranksums

"""
Compute whether individual predictors (or sets of predictors) contribute
to the encoding. 

For every neuron, we compute the contribution of a predictor by taking the 
encoding scores (computed per trial) of the full model and subtracting 
the encoding scores of a model in which the predictor of interest has been 
removed. If the difference in encoding scores is larger than zero (one sided
Wilcoxon test) then the predictor contributes significantly to the encoding 
of that neuron. 

The significance can be computed separately on audio, visual, and multimodal 
trials. 
"""


# specify the encoding run
settings_name = 'aug9'

# specify a list of units for which to generate the plots
# selected_unit_ids = ['E2R-006_1_14']

# alternatively, if None, generate plots for all the cells for which the encoding
# was run
selected_unit_ids = None

# these are the sets of predictors for which we compute the unique information
# by comparing with the full model
predictor_sets_unique = ['unique_stimulus', 'unique_outcome', 'unique_response_side']

# if True, the significance of each predictor is computed separately for each
# trial modality
significance_per_trial_type = False

# option to use the standard explained variance, but should be avoided
# because it's theoretically not correct
metric = 'PR2'

# significance level for predictor contributions
alpha = 0.05

# --- PLOTTING ---

# set plot format to 'svg' for vector images
plot_format = 'png'
# dpi specifies the "quality" of the plots, increase for high definition png
dpi = 150

# define where the plots will be saved, and create that folder if it does not
# exist
plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encoding',
                            settings_name, 'aggregate')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)



# ------------------------------------------------------------------------------

if selected_unit_ids is None:
    # run over all sessions
    session_ids = [str(i) for i in np.arange(19)]

else:
    session_ids = []
    for unit_id in selected_unit_ids:
        session_id = unit_id.split('_')[1]
        session_ids.append(session_id)
    session_ids = np.unique(session_ids)

    selected_units_dict = {s : [] for s in session_ids}
    for unit_id in selected_unit_ids:
        selected_units_dict[unit_id.split('_')[1]].append(unit_id)


if metric == 'EV':
    metric_epoch_1 = 'EV_epoch_1'
    metric_epoch_2 = 'EV_epoch_2'
    estimator = 'median'
elif metric == 'PR2':
    metric_epoch_1 = 'PR2_epoch_1'
    metric_epoch_2 = 'PR2_epoch_2'
    estimator = 'median'
else:
    raise ValueError


# prepare the summary dataframe
summary = pd.DataFrame(columns=['unit_id', 'predictor_set', 'modality', 'epoch',
                                'mean_score', 'median_score', 'pval', 'significant'])


for sessn, session_id in enumerate(session_ids):

    output_file_name = 'encode_setting_{}_{}.pkl'.format(settings_name, session_id )
    output_folder = os.path.join(DATA_FOLDER, 'results', 'encode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    try:
        output = pickle.load(open(output_full_path, 'rb'))
    except FileNotFoundError:
        print('NO ENCODING RESULTS FOR SESSION {}'.format(session_id))
        continue

    pars = output['pars']
    time_bin_centers = pars['time_bin_centers']
    predictor_sets = pars['predictor_sets']

    if selected_unit_ids is None:
        selected_unit_ids_session = list(output['dfs'].keys())
    else:
        selected_unit_ids_session = selected_units_dict[session_id]

    trial_df_session = output['trial_df']
    epoch_1 = pars['epoch_1']
    epoch_2 = pars['epoch_2']
    predictor_sets_order  = list(predictor_sets.keys())

    columns = ['target', 'target', 'response', 'response',
               'hit_miss', 'hit_miss']  # 'response_side', 'response_side']

    vals = ['target', 'distractor', '0', '1', 'hit', 'miss']

    for unit_id in selected_unit_ids_session:

        df = output['dfs'][unit_id]
        dfc = output['dfc'][unit_id]
        for col in ['PR2_epoch_1', 'PR2_epoch_2']:
            dfc[col] = dfc[col].astype(float)

        spikes_unit = output['spikes'][unit_id]
        predicted_firing_rate = output['predictions'][unit_id]

        # IMPORTANT: average scores across trial repeats
        cols = ['animal_id', 'session_id', 'unit_id', 'trial_id', 'modality',
                'response', 'response_side', 'hit_miss', 'target', 'predictor_set']
        df = df.groupby(cols).mean().reset_index()

        # # possibility: average across folds (not repeats)
        # cols = ['animal_id', 'session_id', 'unit_id', 'modality', 'repeat', 'predictor_set']
        # dfc = dfc.groupby(cols).mean().reset_index()


        # get the unique PR2 contribution for each trial and each predictor of interest
        dfull = df[df['predictor_set'] == 'full']
        dfull.index = dfull['trial_id']
        assert dfull['trial_id'].value_counts().max() == 1
        dfull = dfull.drop('trial_id', axis=1)

        du = []
        for predictor_set in predictor_sets_unique :

            dunique = df[df['predictor_set'] == predictor_set]
            dunique.index = dunique['trial_id']
            dunique = dunique.drop('trial_id', axis=1)

            for col in ['PR2_epoch_1', 'PR2_epoch_2'] :
                pr2_without = dunique[col]
                pr2_full = dfull.loc[dunique.index, col]
                dunique[col] = pr2_full - pr2_without
            du.append(dunique)
        du = pd.concat(du)


        for predictor_set in predictor_sets_unique :
            for epoch, col in zip(['epoch_1', 'epoch_2'], [metric_epoch_1, metric_epoch_2]):
                # global or per modality?

                if significance_per_trial_type:
                    mods = modalities + ['all']
                else:
                    mods = ['all']

                for modality in mods:
                    if modality == 'all':
                        dusel = du[du['predictor_set'] == predictor_set]
                    else:
                        dusel = du[(du['predictor_set'] == predictor_set) &
                                (du['modality'] == modality)]

                    vals = dusel[col].__array__()
                    mean_score = vals.mean()
                    median_score = np.median(vals)

                    stat, pval = wilcoxon(vals, alternative='greater')


                    sig = False
                    if pval < alpha:
                        sig = True

                    row = [unit_id, predictor_set, modality, epoch,
                           mean_score, median_score, pval, sig]
                    summary.loc[summary.shape[0], :] = row



# --- PLOT PIE CHARTS ----------------------------------------------------------
sorted_summary = summary.sort_values(by='median_score', ascending=False)

fig, ax = plt.subplots(2, 3, figsize=[8, 8])

for j, predictor_set in enumerate(predictor_sets_unique):
    ax[0, j].set_title(predictor_set)

    for i, epoch in enumerate(['epoch_1', 'epoch_2']):
        ax[i, 0].set_ylabel(epoch)

        s = summary[(summary['epoch'] == epoch) &
                    (summary['predictor_set'] == predictor_set)]

        labels = 'Not sig.', 'Sig.'
        vc = s['significant'].value_counts()
        sizes = [vc.loc[False], vc.loc[True]]
        print(sizes)
        explode = [0, 0.2]
        ax[i, j].pie(sizes, labels=labels, autopct='%1.1f%%',
               colors=['gray', 'purple'],
               explode=explode)

plot_name = 'encode_significance_pie_charts.{}'.format(plot_format)
fig.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)


# --- OVERLAP ------------------------------------------------------------------

sm = summary[summary['modality'] == 'all']
N_tot = sm['unit_id'].unique().shape[0]

sig_stim = sm[(sm['epoch'] == 'epoch_1') &
            (sm['predictor_set'] == 'unique_stimulus')]
sig_stim = sig_stim[sig_stim['significant'] == True]


sig_outc = sm[(sm['epoch'] == 'epoch_2') &
            (sm['predictor_set'] == 'unique_outcome')]
sig_outc = sig_outc[sig_outc['significant'] == True]

sig_resp = sm[(sm['epoch'] == 'epoch_2') &
            (sm['predictor_set'] == 'unique_response_side')]
sig_resp = sig_resp[sig_resp['significant'] == True]

n_stim_outc = list(set(sig_stim['unit_id']).intersection(set(sig_outc['unit_id']))).__len__()
n_stim_resp = list(set(sig_stim['unit_id']).intersection(set(sig_resp['unit_id']))).__len__()

overlap_stim_outc = n_stim_outc / N_tot
overlap_stim_resp = n_stim_resp / N_tot

do = pd.DataFrame(columns=['combo', 'overlap'])
do.loc[0, :] = ['stim_outcome', overlap_stim_outc]
do.loc[1, :] = ['stim_resp', overlap_stim_resp]
