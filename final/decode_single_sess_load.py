import os
from constants import *
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *

settings_name = 'apr28'

experiments = ['target_vs_distractor_with_misses_V',
               'target_vs_distractor_with_misses_M',
               'target_vs_distractor_with_misses_A']

experiments = ['correct_vs_incorrect_V',
               'correct_vs_incorrect_M',
               'correct_vs_incorrect_A',
               'hit_miss_V',
               'hit_miss_M',
               'hit_miss_A',
               'target_vs_distractor_V',
               'target_vs_distractor_M',
               'target_vs_distractor_A']

plot_format = 'png'

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'decoding_single_sess', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

for experiment in experiments:

    output_file_name = 'decode_single_sess_setting_{}_{}.pkl'.format(settings_name,
                                                                     experiment)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'decode_single_sess',
                                 settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    out = pickle.load(open(output_full_path, 'rb'))

    df = out['decoding_scores']
    pars = out['pars']


    for features in ['spikes', 'confound']:

        f, ax = plt.subplots(1, 1, figsize=[5, 5])
        sns.lineplot(data=df[df['features'] == features], x='time', y='score', hue='session_id', ax=ax,
                     estimator=None, palette='muted')
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Accuracy\n{}'.format(experiment))
        sns.despine()
        ax.set_ylim([0.3, 1])
        plt.tight_layout()

        ax.axhline(0.5, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')

        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')

        plot_name = 'decss_{}_{}.{}'.format(experiment, features, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


        f, ax = plt.subplots(1, 1, figsize=[5, 5])
        sns.lineplot(data=df[df['features'] == features], x='time', y='score',
                     ax=ax,
                     estimator='mean', color=experiment_palette[experiment])
        ax.axhline(0.5, ls=':', c='grey')
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Accuracy\n{}'.format(experiment))
        sns.despine()
        ax.set_ylim([0.3, 1])
        plt.tight_layout()
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')

        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')

        plot_name = 'decss_mean_{}_{}.{}'.format(experiment, features, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


