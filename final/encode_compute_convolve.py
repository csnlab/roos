import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
#from warp import warp_trials
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.metrics import make_scorer
from sklearn.metrics import r2_score
from utils import rolling_window, rebin
from MLencoding import MLencoding
from sklearn.model_selection import KFold
from plotting_style import *
from sklearn.metrics import explained_variance_score
from utils import poisson_EV

"""
Encode firing rates using different predictors.
"""

# this string is used to identify the particular combination of parameters
# that is used to run the encoding
settings_name = 'aug13'

# - data parameters:
align_to = 'stimulus'
binsize = 100
# units with average firing rate below this threshold are skipped
full_sess_firing_rate_threshold = 0.5
# not implemented now, but we might still want to warp for the averages later
warp = False
warp_per_modality = False

# - encoding parameters
encoding_model = 'random_forest'
if encoding_model == 'random_forest':
    encoder_params = {'n_estimators' : 100}
n_splits = 5
n_repeats = 5
encoder_max_time = 1000
if binsize == 100:
    encoder_n_filters = 5
elif binsize == 50:
    encoder_n_filters = 8
# set the edges (in ms) of the time epochs in which to evaluate the encoding
epoch_1_t1, epoch_1_t2 = -100, 300
epoch_2_t1, epoch_2_t2 = 300, 600

expand_predictors = True
# the

# if true, generate a plot to visualize the predictors used by the model
plot_predictors = True

# list ids of units you want to encode. if selected_unit_ids = None,
# run over all units
# selected_unit_ids = ['E2R-008_4_19',
#                      'E2R-008_4_9',
#                      'E2R-009_5_4',
#                      'E2R-009_7_7',
#                      'E2R-009_7_15',
#                      'E2R-008_4_8',
#                      'E2R-008_4_22',
#                      'E2R-006_2_0',
#                      'E2R-006_1_11',
#                      'E2R-006_1_14']

selected_unit_ids = None

selected_unit_ids = ['E2R-006_1_14', # more subtle hit miss but also visual
                     'E2R-008_4_9', # hit miss
                     'E2R-008_4_9'] # late outcome (outside of range)

selected_unit_ids = ['E2R-008_4_9'] # late outcome (outside of range)

# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, motion_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)


if selected_unit_ids is None:
    # if no selection of units is provided, run over all sessions
    session_ids = trial_df['session_id'].unique()

else:
    # otherwise select only the sessions corresponding to the selected units ids
    session_ids = []
    for unit_id in selected_unit_ids:
        session_id = unit_id.split('_')[1]
        session_ids.append(session_id)
    session_ids = np.unique(session_ids)

    selected_units_dict = {s : [] for s in session_ids}
    for unit_id in selected_unit_ids:
        selected_units_dict[unit_id.split('_')[1]].append(unit_id)

# TODO remove: only to run for certain sessions
session_ids = session_ids[:4]

# --- SET THE PREDICTORS ---------------------------------------------------


# list of full set of predictors
predictors = ['visual', # presence of visual stimulus
              'audio', # presence of auditory stimulus
              #'multisensory', # is it multisensory? (should allow to pick up multisensory enhancemennt)
              'difficulty',
              'target',  # keep only target as a measure of contrast
              'outcome',
              'response_right',
              'response_left',
              #'hit_miss',
              #'time_from_stim',
              'motion']

# list of predictors related to visual stimulus
predictors_visual= ['visual',
                    'difficulty',
                     'target']

# list of predictors related to auditory stimulus
predictors_audio = ['audio',
                    'difficulty',
                    'target']

# list of predictors related to behavioral response
predictors_behavior = ['outcome',
                       'response_right',
                       'response_left',
                       'motion']

predictors_full_minus_stimulus = ['outcome',
                                  'response_right',
                                  'response_left',
                                  'motion']

predictors_full_minus_behavior = ['visual',
                                  'audio',
                                  'difficulty',
                                  'target']

# all predictors except outcome
predictors_full_minus_outcome = ['visual',
                                  'audio',
                                  'difficulty',
                                  'target',
                                  'response_right',
                                  'response_left',
                                  'motion']

# all predictors except response side
predictors_full_minus_respside = ['visual',
                                  'audio',
                                  'difficulty',
                                  'target',
                                  'outcome',
                                  'motion']

# we will predict the firing rate using these sets of predictors
predictor_sets = {'full' : predictors,
                  # 'visual' : predictors_visual,
                  # 'audio' : predictors_audio,
                  'behavior' : predictors_behavior,
                  'unique_stimulus' : predictors_full_minus_stimulus,
                  'unique_behavior' : predictors_full_minus_behavior}
                  #'unique_outcome' : predictors_full_minus_outcome,
                  # 'unique_response_side' : predictors_full_minus_respside}


# --- RUN THE ENCODING ---------------------------------------------------
n_time_bins_per_trial = len(time_bin_centers)

# exclude late and early trials
trial_df = trial_df[np.isin(trial_df['hit_miss'], ['hit', 'miss'])]


# prepare the target, hit_miss and difficulty columns
columns = ['target',
           'hit_miss']

labelencoders = {}
for col in columns :
    labelencoders[col] = LabelEncoder()
    labelencoders[col].fit(trial_df[col])

difficulty_encoder = OneHotEncoder()
difficulty_encoder.fit(trial_df['difficulty'].__array__().astype(int).reshape(-1, 1))



for sessn, session_id in enumerate(session_ids):

    # prepare the path of the output file
    output_file_name = 'encode_setting_{}_{}.pkl'.format(settings_name, session_id )
    output_folder = os.path.join(DATA_FOLDER, 'results', 'encode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    if not os.path.isdir(output_folder) :
        os.makedirs(output_folder)

    trial_df_session = trial_df[trial_df['session_id'] == session_id]
    trial_df_session.index = trial_df_session['trial_id']
    trial_df_session['reaction_time'] = trial_df_session['reaction_time'] * 1000
    n_neurons = trial_df_session['n_neurons'].iloc[0]
    animal_id = trial_df_session['animal_id'].unique()[0]

    trial_ids = trial_df[trial_df['session_id'] == session_id]['trial_id'].unique()
    assert len(trial_ids) == trial_df_session.shape[0]
    trial_predictor_data = {}

    # iterate through all the trials and for each trial generate the
    # matrix of predictors
    for i, row in trial_df_session.iterrows():

        reaction_time = row['reaction_time']
        predictor_matrix_components = []
        # IMPORTANT: a single predictor can consist of a >1D variable, so we
        # keep a list of predictors corresponding to each column of the predictor
        # matrix
        predictor_array_labels = []

        if np.isnan(reaction_time):
            # if the trial is a miss, the stimulus continues until max stimulus time
            stimulus_end_time = 2000
        else:
            stimulus_end_time = reaction_time
        # iterate through each predictor label and generate the vector
        # of predictor values
        for predictor in predictors:
            if np.isin(predictor, columns):
                p = labelencoders[predictor].transform(np.repeat(row[predictor],
                                                                 n_time_bins_per_trial))
            elif predictor == 'visual':
                if expand_predictors:
                    step_size = 5
                    n_predictor_convolutions = n_time_bins_per_trial
                    n_after_zero = (np.array(time_bin_centers) > 0).sum()
                    if np.isin(row['modality'], ['V', 'M']):
                        p = np.eye(n_after_zero, n_predictor_convolutions, n_predictor_convolutions - n_after_zero)

                        p = np.vstack([np.sum(p[i :i + step_size, :], axis=0) for i in
                                       np.arange(0, p.shape[0], step_size)])
                    else:
                        p = np.zeros([np.arange(0, n_after_zero, step_size).shape[0], n_time_bins_per_trial])

                else:
                    if np.isin(row['modality'], ['V', 'M']):
                        p = np.repeat(1, n_time_bins_per_trial)
                        p[np.array(time_bin_centers) <= 0] = 0
                        #p[np.array(time_bin_centers) >= stimulus_end_time] = 0
                    else:
                        p = np.repeat(0, n_time_bins_per_trial)


            elif predictor == 'audio':
                if expand_predictors:
                    step_size = 5
                    n_predictor_convolutions = n_time_bins_per_trial
                    n_after_zero = (np.array(time_bin_centers) > 0).sum()
                    if np.isin(row['modality'], ['A', 'M']):
                        p = np.eye(n_after_zero, n_predictor_convolutions, n_predictor_convolutions - n_after_zero)

                        p = np.vstack([np.sum(p[i :i + step_size, :], axis=0) for i in
                                       np.arange(0, p.shape[0], step_size)])
                    else:
                        p = np.zeros([np.arange(0, n_after_zero, step_size).shape[0], n_time_bins_per_trial])


                else:
                    if np.isin(row['modality'], ['A', 'M']):
                        p = np.repeat(1, n_time_bins_per_trial)
                        p[np.array(time_bin_centers) <= 0] = 0
                        #p[np.array(time_bin_centers) >= stimulus_end_time] = 0
                    else:
                        p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'multisensory':
                raise ValueError
                # if np.isin(row['modality'], ['M']):
                #     p = np.repeat(1, n_time_bins_per_trial)
                #     p[np.array(time_bin_centers) <= 0] = 0
                #     p[np.array(time_bin_centers) >= stimulus_end_time] = 0
                # else:
                #     p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'outcome_visual':
                raise ValueError
                # p = np.repeat(0, n_time_bins_per_trial)
                # if np.isin(row['modality'], ['V', 'M']) and row['response'] == 1:
                #     p[np.array(time_bin_centers) >= reaction_time] = 1

            elif predictor == 'outcome_audio':
                raise ValueError
                # p = np.repeat(0, n_time_bins_per_trial)
                # if np.isin(row['modality'], ['A', 'M']) and row['response'] == 1:
                #     p[np.array(time_bin_centers) >= reaction_time] = 1

            elif predictor == 'outcome':
                if expand_predictors:
                    n_predictor_convolutions = 6
                    step_size = 6
                    spacing = 3
                    p = np.zeros(shape=[n_predictor_convolutions, n_time_bins_per_trial])
                    if row['response'] == 1:
                        first_idx = next((i for i, j in enumerate(np.array(time_bin_centers) >= reaction_time) if j),None)
                        first_idx = first_idx - 1 # start at the bin before
                        for i in range(n_predictor_convolutions):
                            p[i, first_idx+i*spacing : first_idx+i*spacing+step_size] = 1
                else:
                    p = np.repeat(0, n_time_bins_per_trial)
                    if row['response'] == 1:
                        p[np.array(time_bin_centers) >= reaction_time] = 1
                        #p[np.array(time_bin_centers) >= reaction_time+200] = 0


            elif predictor == 'response_right':
                if expand_predictors:
                    n_predictor_convolutions = 6
                    step_size = 6
                    spacing = 3
                    p = np.zeros(shape=[n_predictor_convolutions, n_time_bins_per_trial])
                    if np.isin(row['response_side'], ['R']):
                        first_idx = next((i for i, j in enumerate(np.array(time_bin_centers) >= reaction_time) if j),None)
                        first_idx = first_idx - 1 # start at the bin before
                        for i in range(n_predictor_convolutions):
                            p[i, first_idx+i*spacing : first_idx+i*spacing+step_size] = 1

                else:
                    p = np.repeat(0, n_time_bins_per_trial)
                    if np.isin(row['response_side'], ['R']):
                        p[np.array(time_bin_centers) >= reaction_time] = 1

            elif predictor == 'response_left':
                if expand_predictors:
                    n_predictor_convolutions = 6
                    step_size = 6
                    spacing = 3
                    p = np.zeros(shape=[n_predictor_convolutions, n_time_bins_per_trial])
                    if np.isin(row['response_side'], ['L']):
                        first_idx = next((i for i, j in enumerate(np.array(time_bin_centers) >= reaction_time) if j),None)
                        first_idx = first_idx - 1 # start at the bin before
                        for i in range(n_predictor_convolutions):
                            p[i, first_idx+i*spacing : first_idx+i*spacing+step_size] = 1

                else:
                    p = np.repeat(0, n_time_bins_per_trial)
                    if np.isin(row['response_side'], ['L']):
                        p[np.array(time_bin_centers) >= reaction_time] = 1

            elif predictor == 'difficulty':
                diff = np.repeat(row['difficulty'], n_time_bins_per_trial).astype(int).reshape(-1, 1)
                p = difficulty_encoder.transform(diff).T
                p = np.array(p.todense())
                #p[:, np.array(time_bin_centers) <= 0] = 0
                #p[:, np.array(time_bin_centers) >= stimulus_end_time] = 0

            elif predictor == 'time_from_stim':
                p = np.array(time_bin_centers) / 1000

            elif predictor == 'motion':
                p = motion_data[row['trial_id']]


            if p.shape.__len__() == 1 or p.shape[0] == 1:
                predictor_array_labels.append(predictor)
            else:
                for _ in range(p.shape[0]):
                    predictor_array_labels.append(predictor)
            predictor_matrix_components.append(p)

        predictor_matrix = np.vstack(predictor_matrix_components)
        trial_predictor_data[row['trial_id']] = predictor_matrix.T

    if plot_predictors:
        binary_predictors = ['visual', 'audio', 'difficulty',
                             'target', 'outcome',
                             'response_right', 'response_left']
        n_rows = len(predictor_array_labels)
        f, ax = plt.subplots(n_rows, 3, figsize=[10, 8], sharey=False,
                             sharex=True)
        # pick three random hit trials, one per modality
        id1 = trial_df_session[(trial_df_session['modality'] == 'V') &
                               (trial_df_session['hit_miss'] == 'hit')].index.values
        id1 = np.random.choice(id1, 1, replace=False)[0]

        id2 = trial_df_session[(trial_df_session['modality'] == 'M') &
                               (trial_df_session['hit_miss'] == 'hit')].index.values
        id2 = np.random.choice(id2, 1, replace=False)[0]

        id3 = trial_df_session[(trial_df_session['modality'] == 'A') &
                               (trial_df_session['hit_miss'] == 'hit')].index.values
        id3 = np.random.choice(id3, 1, replace=False)[0]

        for ix, id in enumerate([id1, id2, id3]):
            x = time_bin_centers
            modality = trial_df_session.loc[id, 'modality']
            response = trial_df_session.loc[id, 'response']
            ax[0, ix].set_title('Trial: {} - {} - response: {}'.format(id, modality, response))
            for ix2 in range(trial_predictor_data[id].shape[1]):
                predictor_label = predictor_array_labels[ix2]
                if np.isin(predictor_label, binary_predictors):
                    ax[ix2, ix].set_ylim([-0.05, 1.05])
                    ax[ix2, ix].set_yticks([0, 1])
                if predictor_label == 'response_left':
                    predictor_label = 'resp. L'
                elif predictor_label == 'response_right' :
                    predictor_label = 'resp. R'
                else:
                    predictor_label = predictor_label.replace('_', '\n')

                y = trial_predictor_data[id][:, ix2]
                ax[ix2, ix].plot(x, y, c='k')
                ax[ix2, 0].set_ylabel(predictor_label, rotation=0,
                                      ha='right')
        plt.tight_layout()
        sns.despine()

        plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encoding',
                                    settings_name)

        if not os.path.isdir(plots_folder) :
            os.makedirs(plots_folder)

        plot_name = 'predictors_{}.{}'.format(settings_name, 'png')
        f.savefig(os.path.join(plots_folder, plot_name), dpi=150)


    predictor_array_labels = np.array(predictor_array_labels)

    if selected_unit_ids is None:
        # if selected units are not given, run for all units
        selected_unit_ids_session = ['{}_{}_{}'.format(animal_id, session_id, unit_ind)
                              for unit_ind in np.arange(n_neurons)]
        selected_unit_indx_session = np.arange(n_neurons)
    else:
        # otherwise get selected unit ids for the specific session
        selected_unit_ids_session = selected_units_dict[session_id]
        selected_unit_indx_session = [int(s.split('_')[-1]) for s in selected_unit_ids_session]

    tb = np.array(time_bin_centers)

    epoch_1_indx = np.where([np.logical_and(tb >= epoch_1_t1, tb <= epoch_1_t2)])[1]
    epoch_2_indx = np.where([np.logical_and(tb >= epoch_2_t1, tb <= epoch_2_t2)])[1]

    if predictors[-1] != 'motion':
        raise ValueError('Motion needs to always be the last predictor, because'
                         'thats the predictor that gets standardized')

    # PREDICT ALL TOGETHER WITH RANDOM CV

    dfs_output = {}
    dfc_output = {}
    spikes_output = {}
    prediction_output = {}

    for unit_ind, unit_id in zip(selected_unit_indx_session, selected_unit_ids_session):

        # this is the dataframe where we save the encoding quality computed
        # separately for each trial
        df = pd.DataFrame(columns=['animal_id',
                                   'session_id',
                                   'unit_id',
                                   'trial_id',
                                   'modality',
                                   'response',
                                   'response_side',
                                   'hit_miss',
                                   'target',
                                   'predictor_set',
                                   'PR2_epoch_1',
                                   'PR2_epoch_2',
                                   'EV_epoch_1',
                                   'EV_epoch_2'])

        # this is the dataframe where we save the encoding quality computed
        # for the relevant epochs of all test trials concatenated
        dfc = pd.DataFrame(columns=['animal_id',
                                   'session_id',
                                   'unit_id',
                                   'modality',
                                   'predictor_set',
                                   'repeat',
                                   'fold',
                                   'PR2_epoch_1',
                                   'PR2_epoch_2'])


        predicted_firing_rate = {st : {t : [] for t in trial_ids} for st in predictor_sets.keys()}

        full_sess_firing_rate = np.hstack([trial_data[t][:, unit_ind] for t in trial_ids])
        full_sess_firing_rate_mean = full_sess_firing_rate.mean()

        if full_sess_firing_rate_mean < full_sess_firing_rate_threshold:
            print('Skipping unit {} - average firing rate {:.2f}'.format(unit_id, full_sess_firing_rate_mean))
            continue
        else:
            print('Encoding unit {}'.format(unit_id))

        # we standardize the firing rate which we will try to predict
        # but then it become not poisson
        # all_unit_data = np.hstack([trial_data[t][:, unit_ind] for t in trial_ids]).reshape(-1, 1)
        # su = StandardScaler()
        # su.fit((all_unit_data))
        # spikes_unit = {t : su.transform(trial_data[t][:, unit_ind].reshape(-1, 1)).flatten()
        #                for t in trial_ids}

        spikes_unit = {t : trial_data[t][:, unit_ind] for t in trial_ids}

        for predictor_set, predictor_set_list in predictor_sets.items():
            print('Running encoding for predictor set: {}'.format(predictor_set))
            print('Predictors:\n')
            print(*predictor_set_list, sep='\n')

            predictor_indices_set = [i for i in range(predictor_array_labels.shape[0])
                                     if np.isin(predictor_array_labels[i], np.array(predictor_set_list))]
            predictor_array_labels_set = predictor_array_labels[predictor_indices_set]

            # find the index of the motion predictor if present, so we can
            # standardize it later
            motion_present = np.isin('motion', predictor_array_labels_set)
            if motion_present:
                relative_indx_motion = np.where(np.array(predictor_array_labels_set) == 'motion')[0][0]

            for repeat in range(n_repeats):

                # cross-validation routine, split the trials into train and test
                kfold = KFold(n_splits=n_splits, shuffle=True, random_state=repeat)
                iterable = kfold.split(trial_ids)

                for fold, (training_ind, testing_ind) in enumerate(iterable):
                    trials_ids_train = trial_ids[training_ind]
                    trials_ids_test = trial_ids[testing_ind]

                    # features matrix: the predictors
                    X_predictor_train = np.hstack([trial_predictor_data[t][:, predictor_indices_set].T
                                                   for t in trials_ids_train]).T

                    # X_predictor_test = np.hstack([trial_predictor_data[t].T
                    #                                for t in trials_ids_test]).T

                    # we standardize the motion, which is the only non-categorical
                    # predictor we have
                    if motion_present :
                        ss = StandardScaler()
                        ss.fit(X_predictor_train[:, relative_indx_motion].reshape(-1, 1))
                        X_predictor_train[:, relative_indx_motion] = ss.transform(X_predictor_train[:, relative_indx_motion].reshape(-1, 1)).flatten()

                    # target: binned spikes of the selected unit
                    y_train = np.hstack([spikes_unit[t] for t in trials_ids_train])
                    #y_test = np.hstack([spikes_unit[t] for t in trials_ids_test])

                    # fit the encoder
                    encoder = MLencoding(tunemodel=encoding_model,
                                         cov_history=True,
                                         n_filters=encoder_n_filters,
                                         window=binsize,
                                         max_time=encoder_max_time,
                                         n_every=1)

                    encoder.set_params(encoder_params)
                    encoder.fit(X_predictor_train, y_train)

                    # should this be restricted to the epoch?
                    y_null_epoch_1 = np.hstack([spikes_unit[t][epoch_1_indx] for t in trials_ids_train]).mean()
                    y_null_epoch_2 = np.hstack([spikes_unit[t][epoch_2_indx] for t in trials_ids_train]).mean()

                    y_null_epoch_1 = np.hstack([spikes_unit[t] for t in trials_ids_train]).mean()
                    y_null_epoch_2 = np.hstack([spikes_unit[t] for t in trials_ids_train]).mean()

                    # for every trial in the test set generate and evaluate predictions

                    y_pred_conc_epoch_1 = {m : [] for m in modalities}
                    y_test_conc_epoch_1 = {m : [] for m in modalities}
                    y_pred_conc_epoch_2 = {m : [] for m in modalities}
                    y_test_conc_epoch_2 = {m : [] for m in modalities}

                    for tid in trials_ids_test:
                        X_predictor_test = trial_predictor_data[tid][:, predictor_indices_set]
                        # standardize motion
                        if motion_present :
                            X_predictor_test[:, relative_indx_motion] = ss.transform(X_predictor_test[:, relative_indx_motion].reshape(-1, 1)).flatten()

                        # predict the full trial
                        y_test = spikes_unit[tid]
                        y_pred = encoder.predict(X_predictor_test)

                        # store the predictions to compute PR2 on concatenated trials
                        tmod = trial_df_session.loc[tid, 'modality']
                        y_pred_conc_epoch_1[tmod].append(y_pred[epoch_1_indx])
                        y_test_conc_epoch_1[tmod].append(y_test[epoch_1_indx])

                        y_pred_conc_epoch_2[tmod].append(y_pred[epoch_2_indx])
                        y_test_conc_epoch_2[tmod].append(y_test[epoch_2_indx])


                        # compute PR2 on individual trials
                        PR2_epoch_1 = encoder.poisson_pseudoR2(y_test[epoch_1_indx],
                                                               y_pred[epoch_1_indx],
                                                               y_null_epoch_1)

                        PR2_epoch_2 = encoder.poisson_pseudoR2(y_test[epoch_2_indx],
                                                               y_pred[epoch_2_indx],
                                                               y_null_epoch_2)

                        myPR2_epoch_1 = poisson_EV(y_test[epoch_1_indx],
                                                   y_pred[epoch_1_indx],
                                                   y_null_epoch_1)

                        assert np.isclose(PR2_epoch_1, myPR2_epoch_1, rtol=1e-10)

                        #EV_epoch_1 = 1 - (np.var(y_test[epoch_1_indx] - y_pred[epoch_1_indx])/np.var(y_test[epoch_1_indx]))
                        #EV_epoch_2 = 1 - (np.var(y_test[epoch_2_indx] - y_pred[epoch_2_indx])/np.var(y_test[epoch_2_indx]))
                        # The sklearn implementation takes care of the Nan and -Inf
                        # that occur when y_true is constant

                        # explained variance is really a bad version of a standard
                        # R2 score that does not account for a sistematic offset of the
                        # predictions
                        EV_epoch_1 = explained_variance_score(y_true=y_test[epoch_1_indx],
                                                              y_pred=y_pred[epoch_1_indx])

                        EV_epoch_2 = explained_variance_score(y_true=y_test[epoch_2_indx],
                                                              y_pred=y_pred[epoch_2_indx])

                        predicted_firing_rate[predictor_set][tid].append(y_pred)

                        modality = trial_df_session.loc[tid]['modality']
                        response = trial_df_session.loc[tid]['response']
                        response_side = trial_df_session.loc[tid]['response_side']
                        hit_miss = trial_df_session.loc[tid]['hit_miss']
                        target = trial_df_session.loc[tid]['target']

                        row = [animal_id,
                               session_id,
                               unit_id,
                               tid,
                               modality,
                               response,
                               response_side,
                               hit_miss,
                               target,
                               predictor_set,
                               PR2_epoch_1,
                               PR2_epoch_2,
                               EV_epoch_1,
                               EV_epoch_2]

                        df.loc[df.shape[0], :] = row


                    # concatenate trials in the test set generate evaluate predictions
                    for modality in modalities:

                        y_pred_conc_epoch_1_arr = np.hstack(y_pred_conc_epoch_1[modality])
                        y_test_conc_epoch_1_arr = np.hstack(y_test_conc_epoch_1[modality])

                        y_pred_conc_epoch_2_arr = np.hstack(y_pred_conc_epoch_2[modality])
                        y_test_conc_epoch_2_arr = np.hstack(y_test_conc_epoch_2[modality])


                        PR2_epoch_1_conc = encoder.poisson_pseudoR2(y_test_conc_epoch_1_arr,
                                                               y_pred_conc_epoch_1_arr,
                                                               y_null_epoch_1)

                        PR2_epoch_2_conc = encoder.poisson_pseudoR2(y_test_conc_epoch_2_arr,
                                                               y_pred_conc_epoch_2_arr,
                                                               y_null_epoch_2)

                        row = [animal_id,
                               session_id,
                               unit_id,
                               modality,
                               predictor_set,
                               repeat,
                               fold,
                               PR2_epoch_1_conc,
                               PR2_epoch_2_conc]

                        dfc.loc[dfc.shape[0], :] = row


                    print('___ encoding unit {} of {} - repeat {} of {} - fold {} of {}'.format(
                             unit_id, n_neurons, repeat, n_repeats, fold, n_splits))
                    print('_______ average PR2 ({}-{}ms) = {:.3f}'.format(epoch_1_t1, epoch_1_t2, PR2_epoch_1_conc))
                    print('_______ average PR2 ({}-{}ms) = {:.3f}'.format(epoch_2_t1, epoch_2_t2, PR2_epoch_2_conc))



        # casting response and response_side as object, so the nans don't cause
        # them to be treated as floats
        for col in ['animal_id', 'session_id', 'unit_id', 'trial_id',
                    'modality', 'response', 'response_side', 'hit_miss', 'target']:
            df[col] = df[col].apply(str)

        for col in ['animal_id', 'session_id', 'unit_id', 'modality']:
            dfc[col] = dfc[col].apply(str)

        for col in ['PR2_epoch_1', 'PR2_epoch_2']:
            dfc[col] = dfc[col].astype(float)


        # --- OUTPUT ---

        dfs_output[unit_id] = df
        dfc_output[unit_id] = dfc
        spikes_output[unit_id] = spikes_unit
        prediction_output[unit_id] = predicted_firing_rate

    pars = {'time_bin_centers' : time_bin_centers,
            'predictor_sets' : predictor_sets,
            'epoch_1' : [epoch_1_t1, epoch_1_t2],
            'epoch_2' : [epoch_2_t1, epoch_2_t2],
            'full_sess_firing_rate_threshold' : full_sess_firing_rate_threshold,
            'warp' : warp,
            'warp_per_modality' : warp_per_modality,
            'n_splits' : n_splits,
            'n_repeats' : n_repeats,
            'binsize' : binsize,
            'align_to' : align_to,
            'encoding_model' : encoding_model,
            'encoder_params' : encoder_params,
            'encoder_max_time' : encoder_max_time,
            'encoder_n_filters' : encoder_n_filters,
            'settings_name' : settings_name,
            'selected_unit_ids_session' : selected_unit_ids_session}

    output = {'dfs' : dfs_output,
              'dfc' : dfc_output,
              'spikes' : spikes_output,
              'predictions' : prediction_output,
              'trial_df' : trial_df_session,
              'pars' : pars}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(output, open(output_full_path, 'wb'))








