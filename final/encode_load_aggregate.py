import os
from constants import *
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from plotting_style import *
import pandas as pd
from load_data import *

"""
Plot the encoding scores aggregated across all neurons (barplots).
"""


settings_name = 'may18'


# these are the sets of predictors that we plot independently
#predictor_sets_partial = ['full', 'stimulus', 'behavior']

predictor_sets_partial = ['full', 'visual', 'audio', 'behavior']


metric = 'PR2'

# --- PLOTTING ---
plot_format = 'png'
dpi = 150
plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encoding', settings_name, 'aggregate')

if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

session_ids = [str(i) for i in range(4)]


if metric == 'EV' :
    metric_epoch_1 = 'EV_epoch_1'
    metric_epoch_2 = 'EV_epoch_2'
    estimator = 'median'
elif metric == 'PR2' :
    metric_epoch_1 = 'PR2_epoch_1'
    metric_epoch_2 = 'PR2_epoch_2'
    estimator = 'median'
else :
    raise ValueError


dfs, dfcs = [], []
for sessn, session_id in enumerate(session_ids) :



    output_file_name = 'encode_setting_{}_{}.pkl'.format(settings_name,
                                                         session_id)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'encode',
                                 settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    try:
        output = pickle.load(open(output_full_path, 'rb'))
        loaded = True
    except FileNotFoundError:
        print('No results for session {}'.format(session_id))
        loaded = False

    if loaded:
        pars = output['pars']
        time_bin_centers = pars['time_bin_centers']
        predictor_sets = pars['predictor_sets']

        trial_df_session = output['trial_df']
        epoch_1 = pars['epoch_1']
        epoch_2 = pars['epoch_2']
        predictor_sets_order = list(predictor_sets.keys())

        columns = ['target', 'target', 'response', 'response',
                   'hit_miss', 'hit_miss']  # 'response_side', 'response_side']

        vals = ['target', 'distractor', '0', '1', 'hit', 'miss']

        unit_ids = list(output['dfs'].keys())
        for unit_id in unit_ids:
            df = output['dfs'][unit_id]
            dfc = output['dfc'][unit_id]

            spikes_unit = output['spikes'][unit_id]
            predicted_firing_rate = output['predictions'][unit_id]

            # IMPORTANT: average scores across trial repeats
            cols = ['animal_id', 'session_id', 'unit_id', 'trial_id', 'modality',
                    'response', 'response_side', 'hit_miss', 'target',
                    'predictor_set']
            df = df.groupby(cols).mean().reset_index()
            dfs.append(df)

            # average over FOLDS AND REPEATS -> one value per neuron(/predictor set/trial type)
            cols = ['animal_id', 'session_id', 'unit_id',
                    'modality', 'predictor_set']
            dfc = dfc.groupby(cols).mean().reset_index()
            dfcs.append(dfc)

df = pd.concat(dfs).reset_index()
dfc = pd.concat(dfcs).reset_index()


n_neurons = dfc['unit_id'].unique().__len__()
f, ax = plt.subplots(2, 2, figsize=[7, 7], sharey='row')

sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_1,
            ax=ax[0, 0], alpha=0.7, color='grey',
            order=predictor_sets_partial, estimator=estimator)

sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_2,
            ax=ax[0, 1], alpha=0.7, color='grey',
            order=predictor_sets_partial, estimator=estimator)
ax[0, 0].set_title('Encoding evaluated in epoch 1\n'
                   '{} - {} ms\n(N={} neurons)'.format(epoch_1[0], epoch_1[1], n_neurons),
                   fontsize=11)
ax[0, 1].set_title('Encoding evaluated in epoch 2\n'
                   '{} - {} ms\n(N={} neurons)'.format(epoch_2[0], epoch_2[1], n_neurons),
                   fontsize=11)

sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_1,
            hue='modality', hue_order=modalities, order=predictor_sets_partial,
            ax=ax[1, 0], palette=modality_palette, alpha=0.7,
            estimator=estimator)

sns.barplot(data=dfc, x='predictor_set', y=metric_epoch_2,
            hue='modality', hue_order=modalities, order=predictor_sets_partial,
            ax=ax[1, 1], palette=modality_palette, alpha=0.7,
            estimator=estimator)
ax[1, 1].legend().remove()
sns.despine()
plt.tight_layout()

plot_name = 'encode_quality_aggregate_barplot_concatenated.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=dpi)






