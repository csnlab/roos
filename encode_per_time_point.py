import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import make_scorer
from sklearn.metrics import r2_score
from utils import rolling_window, rebin

"""
Encoding per time point of each cell's firing rate. 

At every time point, we take a set of predictors, and train and SGD
regressor to predict the value of a cell's firing rate at that time
point across all trials. Then we repeatedly shuffle each predictor
and compare with the unshuffled model to assess the importance of
each predictor AT EACH TIME STEP (as opposed to having just one importance
over time).

This is a way to essentially compute an equivalent of AUC (it can be
aggregated in the same way as AUC) that is not sensitive to the 
correlations in trial subtypes which causes artifacts in AUC. In this way
we should really look at the UNIQUE variance explained by a predictor
at a given point in time. 

Other older versions of this script using different sets of 
predictors are in /old.

1. Need to further develop and test - some of the results are not
super intuitive. 
2. Define a bit more clearly the question you want to answer 
- otherwise interpretation becomes tricky. 
3. Takes extremely long to run - use only to validate some examples?

"""



settings_name = 'jan30'

align_to = 'stimulus'
binsize = 50
start_time_in_ms = -300
end_time_in_ms = 1500 #1500
model = 'SGD'

warp = True
warp_per_modality = False
larger_bins = True
n_shuffles_per_predictor = 100
n_cross_val_repeats = 10
n_folds = 2
restrict_stim_to_stim = True

# DECODING PARAMETERS
# n_splits = 2
# n_repeats = 5
# n_bootstraps = 500
# score_name = 'accuracy'
# shuffle_kfold = True

seed = 92


selected_unit_ids = ['E2R-008_4_19',
                     'E2R-008_4_9',
                     'E2R-009_5_4',
                     'E2R-009_7_7',
                     'E2R-008_4_8',
                     'E2R-006_2_0']

selected_unit_ids = ['E2R-009_7_4']


session_ids = []
for unit_id in selected_unit_ids:

    session_id = unit_id.split('_')[1]
    session_ids.append(session_id)


# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

if warp:
    trial_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True)
else:
    median_reaction_times = {}

if larger_bins:
    #trial_data, time_bin_centers = rolling_window(trial_data, time_bin_centers)
    trial_data, time_bin_centers = rebin(trial_data, time_bin_centers)


# TODO can introduce the exact contrast fluctuations over time!
#sessions = trial_df['session_id'].unique()


# --- OUTPUT FILES ---------------------------------------------------------
output_file_name = 'encode_setting_{}.pkl'.format(settings_name)

output_folder = os.path.join(DATA_FOLDER, 'results', 'encode',
                             settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
pars_file_name = 'parameters_encode_setting_{}.pkl'.format(settings_name)
pars_full_path = os.path.join(output_folder, pars_file_name)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)



# --- SELECT SESSIONS AND TRIALS ------------------------------------------

n_time_bins_per_trial = len(time_bin_centers)

trial_df = trial_df[np.isin(trial_df['hit_miss'], ['hit', 'miss'])]

columns =    ['modality',
              'difficulty',
              'target',
              'hit_miss']

labelencoders = {}
for col in columns:
    labelencoders[col] = LabelEncoder()
    labelencoders[col].fit(trial_df[col])

predictors = ['outcome_visual',
              'outcome_audio',
              'response_right',
              'response_left',
              'hit_miss',
              #'difficulty',
              'target', # keep only target as a measure of contrast
              'visual',
              'audio',
              'multisensory'] # multisensory allows to pick up multisensory enhancemennt


df = pd.DataFrame(columns=['animal_id',
                           'session_id',
                           'unit_id',
                           'time',
                           'predictor',
                           'full_model_score',
                           'unique_predictor_score',
                           'std_shuffled_scores',
                           '95%CI_1',
                           '95%CI_2',
                           'predictor_significant'])

for sessn, session_id in enumerate(session_ids):

    trial_df_session = trial_df[trial_df['session_id'] == session_id]
    n_neurons = trial_df_session['n_neurons'].iloc[0]
    animal_id = trial_df_session['animal_id'].unique()[0]

    trial_ids = trial_df[trial_df['session_id'] == session_id]['trial_id'].unique()

    trial_predictor_data = {}
    for i, row in trial_df_session.iterrows():

        predictors_list = []
        for predictor in predictors:
            if np.isin(predictor, columns):
                p = labelencoders[predictor].transform(np.repeat(row[predictor],
                                                                 n_time_bins_per_trial))
            elif predictor == 'visual':
                if np.isin(row['modality'], ['V', 'M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    if restrict_stim_to_stim:
                        p[np.array(time_bin_centers) <= 0] = 0
                        p[np.array(time_bin_centers) >= median_reaction_times['V']] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'audio':
                if np.isin(row['modality'], ['A', 'M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    if restrict_stim_to_stim:
                        p[np.array(time_bin_centers) <= 0] = 0
                        p[np.array(time_bin_centers) >= median_reaction_times['V']] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'multisensory':
                if np.isin(row['modality'], ['M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    if restrict_stim_to_stim:
                        p[np.array(time_bin_centers) <= 0] = 0
                        p[np.array(time_bin_centers) >= median_reaction_times['V']] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'outcome_visual':
                if np.isin(row['modality'], ['V', 'M']) and row['response'] == 1:
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'outcome_audio':
                if np.isin(row['modality'], ['A', 'M']) and row['response'] == 1:
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'response_right':
                if np.isin(row['response_side'], ['R']):
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'response_left':
                if np.isin(row['response_side'], ['L']):
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)



            # elif predictor == 'stimulus_window':
            #     p = np.repeat(0, n_time_bins_per_trial)


            predictors_list.append(p)

        predictors_matrix = np.vstack(predictors_list)
        trial_predictor_data[row['trial_id']] = predictors_matrix.T


    for unit_ind in np.arange(n_neurons):

        unit_id = '{}_{}_{}'.format(animal_id, session_id, unit_ind)

        if np.isin(unit_id, selected_unit_ids):#'E2R-008_4_9':

            for time_bin in range(n_time_bins_per_trial):

                time = time_bin_centers[time_bin]
                # time0, time1 = time_bin_edges[time_bin]
                print(time)

                if time >= start_time_in_ms and time <= end_time_in_ms :

                    # TODO X does not actually change over time!
                    X = np.vstack([trial_predictor_data[t][time_bin, :] for t in trial_ids])
                    y = np.array([trial_data[t][time_bin, unit_ind] for t in trial_ids])

                    nonzero_firing_bins = (y>0).sum()
                    min_nonzero_firing_bins = int(10 * len(trial_ids) / 100)

                    if nonzero_firing_bins > min_nonzero_firing_bins:

                        if model == 'SGD':
                            regressor = SGDRegressor()

                        elif model == 'RandomForest':
                            regressor = RandomForestRegressor()

                        scorer = make_scorer(r2_score)

                        full_model_score = []
                        for k in range(n_cross_val_repeats):
                            cross_scores = cross_val_score(estimator=regressor,
                                                           X=X, y=y, cv=n_folds,
                                                           scoring=scorer)
                            full_model_score.append(np.mean(cross_scores))
                        full_model_score = np.mean(full_model_score)

                        shuffled_scores = {p : np.zeros(n_shuffles_per_predictor)
                                           for p in predictors}
                        for pred_indx, predictor in enumerate(predictors):

                            for n in range(n_shuffles_per_predictor):

                                X_s = X.copy()
                                X_s[:, pred_indx] = np.random.permutation(X_s[:, pred_indx])
                                mean_score_s = []
                                for k in range(n_cross_val_repeats):
                                    cross_scores_s = cross_val_score(estimator=regressor,
                                                                     X=X_s, y=y, cv=n_folds,
                                                                     scoring=scorer)

                                    mean_score_s.append(np.mean(cross_scores_s))
                                shuffled_scores[predictor][n] = np.mean(mean_score_s)

                        for predictor in predictors:
                            #q1 = np.quantile(shuffled_scores[predictor], q=0.05)
                            q = np.quantile(shuffled_scores[predictor], q=0.95)
                            unique_predictor_score = full_model_score - np.mean(shuffled_scores[predictor])
                            std_shuffled_scores = np.std(shuffled_scores[predictor])
                            ci1 = np.quantile(shuffled_scores[predictor], q=0.025)
                            ci2 = np.quantile(shuffled_scores[predictor], q=0.975)

                            significant = full_model_score > q

                            if unique_predictor_score<-3 or unique_predictor_score>3:
                                raise ValueError
                            row = []
                            df.loc[df.shape[0], :] = [animal_id,
                                                      session_id,
                                                      unit_id,
                                                      time,
                                                      predictor,
                                                      full_model_score,
                                                      unique_predictor_score,
                                                      std_shuffled_scores,
                                                      ci1,
                                                      ci2,
                                                      significant]
                    else:
                        for predictor in predictors:
                            df.loc[df.shape[0], :] = [animal_id,
                                                      session_id,
                                                      unit_id,
                                                      time,
                                                      predictor,
                                                      0,
                                                      0,
                                                      0,
                                                      0,
                                                      0,
                                                      False]


numeric_cols = ['time', 'full_model_score',
                           'unique_predictor_score',
                           'std_shuffled_scores',
                           '95%CI_1',
                           '95%CI_2']
for col in numeric_cols:
    df[col] = pd.to_numeric(df[col])


predictors_to_plot = ['outcome_visual',
                      'outcome_audio',
                      #'response_right',
                      #'response_left',
                      'hit_miss',
                      #'difficulty',
                      'target', # keep only target as a measure of contrast
                      'visual',
                      'audio',
                      'multisensory']


predictor_palette = {'outcome_visual' : sns.xkcd_rgb['dark red'],
                     'outcome_audio' : sns.xkcd_rgb['dark green'],
                     'response_right' : sns.xkcd_rgb['teal'],
                     'response_left' : sns.xkcd_rgb['brown'],
                     'difficulty' : sns.xkcd_rgb['orange'],
                     'target' : sns.xkcd_rgb['purple'],
                     'visual' : sns.xkcd_rgb['green'],
                     'audio' : sns.xkcd_rgb['red'],
                     'multisensory' : sns.xkcd_rgb['blue'],
                     'hit_miss' : sns.xkcd_rgb['pink']}


plot_format = 'png'
plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encode_single_units')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


for unit_id in selected_unit_ids:

    from plotting_style import *



    f, ax = plt.subplots(1, 1, figsize=[4, 4])

    # sns.lineplot(data=df[df['unit_id'] == unit_id],
    #              x='time', y='unique_predictor_score', hue='predictor', ax=ax)

    for predictor in predictors:
        dx = df[(df['unit_id'] == unit_id) & (df['predictor'] == predictor)]
        y =  dx['unique_predictor_score']
        ax.plot(dx['time'], y, linewidth=2,
                c=predictor_palette[predictor], label=predictor)

        CI95 = dx['95%CI_1'] - dx['95%CI_2'] / 2
        STD = dx['std_shuffled_scores'] /2

        ERR = STD

        ax.fill_between(dx['time'], y-ERR, y+ERR,
                        color=predictor_palette[predictor],
                        alpha=0.3, linewidth=0)


    ax.set_title('{}'.format(unit_id))
    ax.set_ylabel('Unique explained variance')
    ax.set_xlabel('Time [ms]')
    ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.legend(prop={'size': 6})

    if warp:
        for modality in modalities :
            ax.axvline(median_reaction_times[modality],
                       c=modality_palette[modality],
                       ls='--')
    # ax.set_ylim([0, 30])
    #ax.set_ylim([-0.2,0.2])
    sns.despine()
    plt.tight_layout()

    plot_name = 'encode_v2_{}_{}.{}'.format(settings_name, unit_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



