import os

DATA_FOLDER = '/Users/pietro/data/roos'

modalities = ['V', 'M', 'A']
vals = [0, 1]

MIN_REACTION_TIME_IN_SECONDS = 0.150
MAX_REACTION_TIME_IN_SECONDS = 0.726