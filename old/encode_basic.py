import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
#from warp import warp_trials
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import make_scorer
from sklearn.metrics import r2_score
from utils import rolling_window, rebin
from MLencoding import MLencoding


"""
Encode firing rates using different predictors.

To assess importance of a predictor, recompute after rotating the value
of that predictor across trials and compute difference in model performance.

"""


settings_name = 'apr2'

align_to = 'stimulus'
binsize = 50

n_cv = 10
encoding_model = 'random_forest'
encoder_params = {'n_estimators' : 100}


warp = False
warp_per_modality = False
larger_bins = False
n_shuffles_per_predictor = 100
n_cross_val_repeats = 10
n_folds = 2
# TODO this needs to be adapted
restrict_stim_to_stim = False

# DECODING PARAMETERS
# n_splits = 2
# n_repeats = 5
# n_bootstraps = 500
# score_name = 'accuracy'
# shuffle_kfold = True

seed = 92


selected_unit_ids = ['E2R-008_4_19',
                     'E2R-008_4_9',
                     'E2R-009_5_4',
                     'E2R-009_7_7',
                     'E2R-008_4_8',
                     'E2R-006_2_0']

selected_unit_ids = ['E2R-009_5_4']


session_ids = []
for unit_id in selected_unit_ids:

    session_id = unit_id.split('_')[1]
    session_ids.append(int(session_id))


# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, motion_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

# TODO rebin should also rebin the motion data
if larger_bins:
    #trial_data, time_bin_centers = rolling_window(trial_data, time_bin_centers)
    trial_data, time_bin_centers = rebin(trial_data, time_bin_centers)


# TODO can introduce the exact contrast fluctuations over time!
#sessions = trial_df['session_id'].unique()


# --- OUTPUT FILES ---------------------------------------------------------
output_file_name = 'encode_setting_{}.pkl'.format(settings_name)

output_folder = os.path.join(DATA_FOLDER, 'results', 'encode',
                             settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
pars_file_name = 'parameters_encode_setting_{}.pkl'.format(settings_name)
pars_full_path = os.path.join(output_folder, pars_file_name)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)



# --- SELECT SESSIONS AND TRIALS ------------------------------------------

n_time_bins_per_trial = len(time_bin_centers)

trial_df = trial_df[np.isin(trial_df['hit_miss'], ['hit', 'miss'])]

# encoded columns
columns =    ['modality',
              'difficulty',
              'target',
              'hit_miss']

labelencoders = {}
for col in columns:
    labelencoders[col] = LabelEncoder()
    labelencoders[col].fit(trial_df[col])

predictors = ['outcome_visual',
              'outcome_audio',
              'response_right',
              'response_left',
              'hit_miss',
              #'difficulty',
              'target', # keep only target as a measure of contrast
              'visual',
              'audio',
              'multisensory', # multisensory allows to pick up multisensory enhancemennt
              'motion']

df = pd.DataFrame(columns=['animal_id',
                           'session_id',
                           'unit_id',
                           'time',
                           'predictor',
                           'full_model_score',
                           'unique_predictor_score',
                           'std_shuffled_scores',
                           '95%CI_1',
                           '95%CI_2',
                           'predictor_significant'])

for sessn, session_id in enumerate(session_ids):

    trial_df_session = trial_df[trial_df['session_id'] == session_id]
    n_neurons = trial_df_session['n_neurons'].iloc[0]
    animal_id = trial_df_session['animal_id'].unique()[0]

    trial_ids = trial_df[trial_df['session_id'] == session_id]['trial_id'].unique()
    assert len(trial_ids) == trial_df_session.shape[0]
    trial_predictor_data = {}


    for i, row in trial_df_session.iterrows():

        predictors_list = []
        for predictor in predictors:
            if np.isin(predictor, columns):
                p = labelencoders[predictor].transform(np.repeat(row[predictor],
                                                                 n_time_bins_per_trial))
            elif predictor == 'visual':
                if np.isin(row['modality'], ['V', 'M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    if restrict_stim_to_stim:
                        p[np.array(time_bin_centers) <= 0] = 0
                        p[np.array(time_bin_centers) >= median_reaction_times['V']] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'audio':
                if np.isin(row['modality'], ['A', 'M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    if restrict_stim_to_stim:
                        p[np.array(time_bin_centers) <= 0] = 0
                        p[np.array(time_bin_centers) >= median_reaction_times['V']] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'multisensory':
                if np.isin(row['modality'], ['M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    if restrict_stim_to_stim:
                        p[np.array(time_bin_centers) <= 0] = 0
                        p[np.array(time_bin_centers) >= median_reaction_times['V']] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'outcome_visual':
                if np.isin(row['modality'], ['V', 'M']) and row['response'] == 1:
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'outcome_audio':
                if np.isin(row['modality'], ['A', 'M']) and row['response'] == 1:
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'response_right':
                if np.isin(row['response_side'], ['R']):
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'response_left':
                if np.isin(row['response_side'], ['L']):
                    p = np.repeat(1, n_time_bins_per_trial)
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'motion':
                p = motion_data[row['trial_id']]

            predictors_list.append(p)

        predictors_matrix = np.vstack(predictors_list)
        trial_predictor_data[row['trial_id']] = predictors_matrix.T


    # PREDICT ALL TOGETHER WITH RANDOM CV
    for unit_ind in np.arange(n_neurons):

        unit_id = '{}_{}_{}'.format(animal_id, session_id, unit_ind)

        if np.isin(unit_id, selected_unit_ids):#'E2R-008_4_9':

            spikes_unit = np.hstack([trial_data[t][:, unit_ind] for t in trial_ids])


            # THE ENCODING
            X_predictor = np.hstack([trial_predictor_data[t].T for t in trial_ids]).T
            y = spikes_unit


            encoder = MLencoding(tunemodel=encoding_model,
                                 cov_history=True,
                                 n_filters=5,
                                 window=binsize,
                                 max_time=3000,
                                 n_every=1)
            encoder.set_params(encoder_params)


            Y_hat, PR2s = encoder.fit_cv(X_predictor, y,
                                         n_cv=n_cv,
                                         verbose=2,
                                         continuous_folds=True)
            PR2s = np.mean(PR2s)

            #rs[sess_ind][area][predictor][kk, :] = Y_hat





# predictors_to_plot = ['outcome_visual',
#                       'outcome_audio',
#                       #'response_right',
#                       #'response_left',
#                       'hit_miss',
#                       #'difficulty',
#                       'target', # keep only target as a measure of contrast
#                       'visual',
#                       'audio',
#                       'multisensory']
#
#
# predictor_palette = {'outcome_visual' : sns.xkcd_rgb['dark red'],
#                      'outcome_audio' : sns.xkcd_rgb['dark green'],
#                      'response_right' : sns.xkcd_rgb['teal'],
#                      'response_left' : sns.xkcd_rgb['brown'],
#                      'difficulty' : sns.xkcd_rgb['orange'],
#                      'target' : sns.xkcd_rgb['purple'],
#                      'visual' : sns.xkcd_rgb['green'],
#                      'audio' : sns.xkcd_rgb['red'],
#                      'multisensory' : sns.xkcd_rgb['blue'],
#                      'hit_miss' : sns.xkcd_rgb['pink']}
#
#
# plot_format = 'png'
# plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encode_single_units')
# if not os.path.isdir(plots_folder):
#     os.makedirs(plots_folder)
#
#
# for unit_id in selected_unit_ids:
#
#     from plotting_style import *
#
#
#
#     f, ax = plt.subplots(1, 1, figsize=[4, 4])
#
#     # sns.lineplot(data=df[df['unit_id'] == unit_id],
#     #              x='time', y='unique_predictor_score', hue='predictor', ax=ax)
#
#     for predictor in predictors:
#         dx = df[(df['unit_id'] == unit_id) & (df['predictor'] == predictor)]
#         y =  dx['unique_predictor_score']
#         ax.plot(dx['time'], y, linewidth=2,
#                 c=predictor_palette[predictor], label=predictor)
#
#         CI95 = dx['95%CI_1'] - dx['95%CI_2'] / 2
#         STD = dx['std_shuffled_scores'] /2
#
#         ERR = STD
#
#         ax.fill_between(dx['time'], y-ERR, y+ERR,
#                         color=predictor_palette[predictor],
#                         alpha=0.3, linewidth=0)
#
#
#     ax.set_title('{}'.format(unit_id))
#     ax.set_ylabel('Unique explained variance')
#     ax.set_xlabel('Time [ms]')
#     ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
#     ax.legend(prop={'size': 6})
#
#     if warp:
#         for modality in modalities :
#             ax.axvline(median_reaction_times[modality],
#                        c=modality_palette[modality],
#                        ls='--')
#     # ax.set_ylim([0, 30])
#     #ax.set_ylim([-0.2,0.2])
#     sns.despine()
#     plt.tight_layout()
#
#     plot_name = 'encode_v2_{}_{}.{}'.format(settings_name, unit_id, plot_format)
#     f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



