import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from plotting_style import *
import matplotlib.gridspec as gridspec
from utils import rolling_window
from utils import fix_animal_id

settings_name = 'jan25alltrials'
settings_name = 'jan25_min8'

auc_alpha_level = 0.01
align_to = 'stimulus'
binsize = 50

auc_combo = 'correct_vs_incorrect'
auc_experiment = 'correct_vs_incorrect_V'
min_sign_times_points = 3
auc_ref_times =[805, 845,  885,  925,  965, 1005, 1045, 1085, 1125, 1165, 1205, 1245,
                 1285, 1325, 1365, 1405, 1445, 1485]


warp = True
warp_per_modality = True

plot_format = 'png'
joint_auc_axes = False
plot_observed = False


seed = 92

# --- PLOTS FOLDER -------------------------------------------------------------

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'psths')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

# --- ALPHA LEVEL -------------------------------------------------------------

if auc_alpha_level == 0.001:
    col_name = 'is_sign_001'
elif auc_alpha_level == 0.01:
    col_name = 'is_sign_01'
elif auc_alpha_level == 0.05:
    col_name = 'is_sign_05'
else:
    raise ValueError


# --- GET IDs of SIGNIFICANT UNITS ---------------------------------------------
#session_df, sdf, dp = select_data_experiment(experiment, trial_df)


# TODO here clean up cause we moved some things in auc_index_over_time
output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(auc_combo,
                                                                settings_name)
output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
out = pickle.load(open(output_full_path, 'rb'))

df = out['auc_scores']


df['animal_id'] = [fix_animal_id(aid) for aid in df['animal_id']]
df['unit_id'] = ['{}_{}_{}'.format(a, s, i) for a, s, i in zip(df['animal_id'], df['session_id'], df['unit_ind'])]


experiments =  df['experiment'].unique()
pars = out['pars']

try:
    larger_bins = pars['larger_bins']
except KeyError:
    if settings_name == 'jan24':
        larger_bins = True
    else:
        larger_bins = False

try:
    subsample_majority_class = pars['subsample_majority_class']
except KeyError:
    subsample_majority_class = True

if subsample_majority_class:
    score_col = 'score_observed'
else:
    score_col = 'score_debiased'

if np.isin(settings_name, ['jan25', 'jan25_min8']) :
    df['time'] = df['time'] + 80

times = df['time'].unique()

signdf = df[np.isin(df['time'], auc_ref_times) & (df['experiment'] == auc_experiment)]

selected_units = []
for uid in signdf['unit_id'].unique():
    sign_time_points = signdf[signdf['unit_id'] == uid][col_name].sum()
    if sign_time_points >= min_sign_times_points:
        selected_units.append(uid)


# --- LOAD DATA ----------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

if warp:
    trial_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True)
else:
    median_reaction_times = {}

if larger_bins:
    trial_data, time_bin_centers = rolling_window(trial_data, time_bin_centers)

# --- SELECT TRIALS ----------------------------------------------------------------


for unit_id in selected_units:

    animal_id = unit_id.split('_')[0]
    session_id = unit_id.split('_')[1]
    unit_ind = int(unit_id.split('_')[2])

    unit_data = {m : {} for m in modalities}

    for im, modality in enumerate(modalities):
        for ir, response in enumerate(vals):
            sdf = trial_df[(trial_df['modality'] == modality) &
                           (trial_df['response'] == response) &
                           (trial_df['session_id'] == int(session_id)) &
                           (trial_df['animal_id'] == animal_id)]
            try:
                unit_data[modality][response] = np.vstack([trial_data[tid][:, unit_ind] for tid in sdf['trial_id']])
            except ValueError:
                unit_data[modality][response] = np.zeros(shape=[1, len(time_bin_centers)])
    # s = trial_df[trial_df['session_id'] == int(session_id)]
    # s[s['modality'] == 'M']

    vmaxes = []
    for im, modality in enumerate(modalities):
        for ir, response in enumerate(vals):
            vmaxes.append(unit_data[modality][response].max())

    if joint_auc_axes:

        f, axes = plt.subplots(ncols=3, nrows=2, figsize=[5, 5])

        f = plt.figure(tight_layout=True)
        gs = gridspec.GridSpec(3, 3)

        axes = {m : {} for m in modalities}

        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                    axes[modality][response] = f.add_subplot(gs[ir, im])

        ax = f.add_subplot(gs[-1, :])

        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                n_trials = unit_data[modality][response].shape[0]
                axes[modality][response].imshow(unit_data[modality][response],
                                  vmin=0, vmax=np.max(vmaxes),
                                  extent=(times[0]-0.5, times[-1]-0.5, n_trials+0.5, 0.5),
                                aspect='auto')
                axes[modality][response].axvline()

                axes[modality][response].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
                if warp:
                    median_reaction_times = out['pars']['median_reaction_times']
                    axes[modality][response].axvline(median_reaction_times[modality],
                                   c=modality_palette[modality],
                                   ls='--')

                    axes[modality][response].set_yticks([1, n_trials])


        axes[modalities[0]][vals[0]].set_title(modalities[0])
        axes[modalities[1]][vals[0]].set_title(modalities[1])
        axes[modalities[2]][vals[0]].set_title(modalities[2])
        axes[modalities[0]][vals[0]].set_ylabel(psth_labels[vals[0]], rotation=90, size='large')
        axes[modalities[0]][vals[1]].set_ylabel(psth_labels[vals[1]], rotation=90, size='large')

        for experiment in df['experiment'].unique():
            dx = df[(df['unit_id'] == unit_id) & (df['experiment'] == experiment)]
            ax.plot(dx['time'], dx[score_col], c=experiment_palette[experiment])
            if plot_observed:
                ax.plot(dx['time'], dx['score_observed'], c=experiment_palette[experiment], ls=':')

            ax.scatter(dx[~dx[col_name]]['time'], dx[~dx[col_name]][score_col],
                       marker='x', color=experiment_palette[experiment])
            ax.scatter(dx[dx[col_name]]['time'], dx[dx[col_name]][score_col],
                       marker='o', color=experiment_palette[experiment])

        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if warp :
            median_reaction_times = out['pars']['median_reaction_times']
            for modality in modalities:
                ax.axvline(median_reaction_times[modality],
                                                 c=modality_palette[modality],
                                                 ls='--')
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('AUC score')
        # sns.lineplot(data=df[df['unit_id'] == unit_id],
        #              x='time', y='score_observed', hue='experiment',
        #              palette=experiment_palette,markers=True, style=col_name,
        #              ax=ax)
        #ax.get_legend().remove()
        sns.despine()

        plot_name = 'psth_{}_{}_{}.{}'.format(settings_name, auc_experiment, unit_id, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


        plt.close()


    else:

        xticks = [0, 500, 1000]

        f = plt.figure(tight_layout=True, figsize=[7, 6])
        gs = gridspec.GridSpec(4, 3)

        imshow_axes = {m : {} for m in modalities}
        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                    imshow_axes[modality][response] = f.add_subplot(gs[ir, im])

        psth_axes = {m : {} for m in modalities}
        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                psth_axes[modality][response] = f.add_subplot(gs[-2, im])

        auc_axes = {}
        for exp, experiment in enumerate(experiments):
            auc_axes[experiment] = f.add_subplot(gs[-1, exp])

        # PLOT FIRING HEATMAPS
        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                n_trials = unit_data[modality][response].shape[0]
                imshow_axes[modality][response].imshow(unit_data[modality][response],
                                                       vmin=0, vmax=np.max(vmaxes),
                                                       extent=(times[0]-0.5, times[-1]-0.5, n_trials+0.5, 0.5),
                                                       aspect='auto')
                imshow_axes[modality][response].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
                imshow_axes[modality][response].set_xticks(xticks)
                if warp:
                    median_reaction_times = out['pars']['median_reaction_times']
                    imshow_axes[modality][response].axvline(median_reaction_times[modality],
                                                            c=modality_palette[modality],
                                                            ls='--')

                    imshow_axes[modality][response].set_yticks([1, n_trials])

        imshow_axes[modalities[0]][vals[0]].set_title(modalities[0])
        imshow_axes[modalities[1]][vals[0]].set_title(modalities[1])
        imshow_axes[modalities[2]][vals[0]].set_title(modalities[2])
        imshow_axes[modalities[0]][vals[0]].set_ylabel(psth_labels[vals[0]], rotation=90, size='large')
        imshow_axes[modalities[0]][vals[1]].set_ylabel(psth_labels[vals[1]], rotation=90, size='large')


        # PLOT PSTH

        uppers = []
        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                n_trials = unit_data[modality][response].shape[0]
                data = unit_data[modality][response]

                upper = np.quantile(data, q=0.8, axis=0)
                uppers.append(upper.max())
        xlim = np.max(uppers)

        for im, modality in enumerate(modalities):
            for ir, response in enumerate(vals):
                n_trials = unit_data[modality][response].shape[0]
                data = unit_data[modality][response]

                #upper = np.quantile(data, q=0.8, axis=0)
                #lower = np.quantile(data, q=0.2, axis=0)
                mean = np.mean(data, axis=0)
                # TODO here we could compute a 95% CI of the mean
                # TODO but I am more interested in variation in the data
                std = np.std(data, axis=0)

                ax = psth_axes[modality][response]
                ax.plot(times, mean, c=modality_palette[modality], ls=response_linestyles[response])
                # ax.fill_between(times, lower, upper, color=modality_palette[modality],
                #                 alpha=0.3, zorder=-2, linewidth=0)
                ax.fill_between(times, mean-std/2, mean+std/2, color=modality_palette[modality],
                                alpha=0.3, zorder=-2, linewidth=0)
                ax.set_ylim([0, xlim])
                ax.set_xticks(xticks)
                ax.set_xlim([times[0], times[-1]])

                if warp:
                    median_reaction_times = out['pars']['median_reaction_times']
                    ax.axvline(median_reaction_times[modality],
                                                            c=modality_palette[modality],
                                                            ls='--')
                    ax.axvline(0, ls='--', c=sns.xkcd_rgb['grey'])

        psth_axes[modalities[0]][vals[0]].set_ylabel('Firing rate [??]')


        # PLOT AUC
        for experiment in experiments:
            dx = df[(df['unit_id'] == unit_id) & (df['experiment'] == experiment)]
            auc_axes[experiment].plot(dx['time'], dx[score_col], c=experiment_palette[experiment])
            # auc_axes[experiment].scatter(dx[~dx[col_name]]['time'], dx[~dx[col_name]]['score_observed'],
            #            marker='x', color=experiment_palette[experiment])
            if plot_observed:
                auc_axes[experiment].plot(dx['time'], dx['score_observed'], c=experiment_palette[experiment], ls=':')

            auc_axes[experiment].scatter(dx[dx[col_name]]['time'], dx[dx[col_name]][score_col],
                       marker='o', color=experiment_palette[experiment], s=15)

            auc_axes[experiment].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
            auc_axes[experiment].set_xticks(xticks)
            if warp :
                median_reaction_times = out['pars']['median_reaction_times']
                for modality in modalities:
                    auc_axes[experiment].axvline(median_reaction_times[experiment.split('_')[-1]],
                                                     c=modality_palette[experiment.split('_')[-1]],
                                                     ls='--')
            auc_axes[experiment].set_xlabel('Time [ms]')

            auc_axes[experiment].set_xlim([times[0], times[-1]])
            auc_axes[experiment].set_ylim([0, 1])
            auc_axes[experiment].axhline(0.5, ls=':', c=sns.xkcd_rgb['grey'])


        auc_axes[experiments[0]].set_ylabel('AUC score')

        sns.despine()

        plot_name = 'psth_{}_{}_{}.{}'.format(settings_name, auc_experiment, unit_id, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


        plt.close()


    # else:
    #     f = plt.figure(tight_layout=True, figsize=[7, 6])
    #     gs = gridspec.GridSpec(3, 3)
    #
    #     imshow_axes = {m : {} for m in modalities}
    #     for im, modality in enumerate(modalities):
    #         for ir, response in enumerate(responses):
    #                 imshow_axes[modality][response] = f.add_subplot(gs[ir, im])
    #
    #     auc_axes = {}
    #     for exp, experiment in enumerate(experiments):
    #         auc_axes[experiment] = f.add_subplot(gs[-1, exp])
    #
    #     for im, modality in enumerate(modalities):
    #         for ir, response in enumerate(responses):
    #             n_trials = unit_data[modality][response].shape[0]
    #             imshow_axes[modality][response].imshow(unit_data[modality][response],
    #                                                    vmin=0, vmax=np.max(vmaxes),
    #                                                    extent=(times[0]-0.5, times[-1]-0.5, n_trials+0.5, 0.5),
    #                                                    aspect='auto')
    #             imshow_axes[modality][response].axvline()
    #
    #             imshow_axes[modality][response].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #             if warp:
    #                 median_reaction_times = out['pars']['median_reaction_times']
    #                 imshow_axes[modality][response].axvline(median_reaction_times[modality],
    #                                                         c=modality_palette[modality],
    #                                                         ls='--')
    #
    #                 imshow_axes[modality][response].set_yticks([1, n_trials])
    #
    #
    #     imshow_axes[modalities[0]][responses[0]].set_title(modalities[0])
    #     imshow_axes[modalities[1]][responses[0]].set_title(modalities[1])
    #     imshow_axes[modalities[2]][responses[0]].set_title(modalities[2])
    #     imshow_axes[modalities[0]][responses[0]].set_ylabel(response_labels[responses[0]], rotation=90, size='large')
    #     imshow_axes[modalities[0]][responses[1]].set_ylabel(response_labels[responses[1]], rotation=90, size='large')
    #
    #     for experiment in experiments:
    #         dx = df[(df['unit_id'] == unit_id) & (df['experiment'] == experiment)]
    #         auc_axes[experiment].plot(dx['time'], dx[score_col], c=experiment_palette[experiment])
    #         # auc_axes[experiment].scatter(dx[~dx[col_name]]['time'], dx[~dx[col_name]]['score_observed'],
    #         #            marker='x', color=experiment_palette[experiment])
    #         if plot_observed:
    #             auc_axes[experiment].plot(dx['time'], dx['score_observed'], c=experiment_palette[experiment], ls=':')
    #
    #         auc_axes[experiment].scatter(dx[dx[col_name]]['time'], dx[dx[col_name]][score_col],
    #                    marker='o', color=experiment_palette[experiment], s=15)
    #
    #         auc_axes[experiment].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
    #         if warp :
    #             median_reaction_times = out['pars']['median_reaction_times']
    #             for modality in modalities:
    #                 auc_axes[experiment].axvline(median_reaction_times[experiment.split('_')[-1]],
    #                                                  c=modality_palette[experiment.split('_')[-1]],
    #                                                  ls='--')
    #         auc_axes[experiment].set_xlabel('Time [ms]')
    #
    #         auc_axes[experiment].set_xlim([times[0], times[-1]])
    #         auc_axes[experiment].set_ylim([0, 1])
    #         auc_axes[experiment].axhline(0.5, ls=':', c=sns.xkcd_rgb['grey'])
    #
    #
    #     auc_axes[experiments[0]].set_ylabel('AUC score')
    #
    #     sns.despine()
    #
    #     plot_name = 'psth_{}_{}_{}.{}'.format(settings_name, auc_experiment, unit_id, plot_format)
    #     f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    #
    #
    #     plt.close()
