import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *

plot_format = 'png'

# combos = [['nov9_stim_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_resp_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_stim_fast_50', 'target_vs_distractor_fast'],
#           ['nov9_resp_fast_50', 'target_vs_distractor_fast']]

combos = [['nov9_stim', 'target_vs_distractor'],
          ['nov9_resp', 'target_vs_distractor']]

combos = [['nov9_easy', 'target_vs_distractor_easy'],
          ['nov9_easy_resp', 'target_vs_distractor_easy']]

combos = [['nov9_correct', 'target_vs_distractor_correct']]

combos = [['nov9_easy', 'target_vs_distractor_easy']]


combos = [['nov9_stim_slow_50', 'target_vs_distractor_slow'],
          ['nov9_stim_fast_50', 'target_vs_distractor_fast']]

combos = [['nov10_20_trials', 'target_vs_distractor']]

# TIME WITH WHICH WE COMPARE FOR OVERLAP
t0 = 45

#settings_name = 'nov9_stim_slow_30'
#combo = 'target_vs_distractor_slow'

for settings_name, combo in combos:

    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    n_boot = 200

    # EXCLUDE AUDITORY

    df = out['auc_scores']
    df = df[[False if i[-1] == 'A' else True for i in df['experiment']]]
    df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(df['session_id'], df['unit_ind'])]

    dn = pd.DataFrame(columns=['time', 'experiment', 'overlap', 'new_cells'])


    for experiment in df['experiment'].unique() :


        df_time = df[(df['time'] == t0) & (df['experiment'] == experiment)]
        sign = df_time['is_sign_01']
        sign_cohort_t0 = df_time['unit_id'][sign]

        for time in df['time'].unique():
            # if time == 45:
            #     raise ValueError

            df_time = df[(df['time'] == time) & (df['experiment'] == experiment)]
            sign = df_time['is_sign_01']
            sign_cohort = df_time['unit_id'][sign]

            overlap = 100 * np.isin(sign_cohort, sign_cohort_t0).sum() / sign_cohort_t0.shape[0]

            new_cells = 100 * (~np.isin(sign_cohort, sign_cohort_t0)).sum() / sign_cohort.shape[0]

            dn.loc[dn.shape[0], :] = [time, experiment, overlap, new_cells]

            # for boot in range(n_boot):
            #     sign_boot = np.random.choice(sign, size=sign.shape[0], replace=True)
            #     perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
            #     dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]

    dn['area'] = None
    dn['area'] = 'V1'
    if out['pars']['align_to'] == 'response':
        dn = dn[dn['time'] <= 0]


    f, ax = plt.subplots(1, 1, figsize=[3, 3])
    sns.lineplot(data=dn, y='overlap', x='time', hue='experiment', ax=ax,
                 palette=experiment_palette, markers=True, style='area',
                 ci='sd')
    ax.axvline(x=t0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_title(combo_labels[combo])
    ax.set_ylabel('% significant cells which overlap\nwith initial cohort')
    ax.set_xlabel('Time [ms]')
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()
    xlim = ax.get_xlim()

    plot_name = 'auc_overlap_combo_bootstrapped_{}_{}.{}'.format(combo, settings_name, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


    dn.loc[dn['time'] < t0, 'new_cells'] = np.nan
    f, ax = plt.subplots(1, 1, figsize=[3, 3])
    sns.lineplot(data=dn, y='new_cells', x='time', hue='experiment', ax=ax,
                 palette=experiment_palette, markers=True, style='area',
                 ci='sd')
    ax.axvline(x=t0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_xlim(xlim)
    ax.set_title(combo_labels[combo])
    ax.set_ylabel('% significant cells which were not\nin the initial cohort')
    ax.set_xlabel('Time [ms]')
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()


    plot_name = 'auc_newcells_combo_bootstrapped_{}_{}.{}'.format(combo, settings_name, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)




# # ---- NOT BOOTSTRAPPED ------
# df = out['auc_scores']
#
# dn = pd.DataFrame(columns=['time', 'experiment', 'perc_sig'])
#
# for time in df['time'].unique():
#     for experiment in df['experiment'].unique():
#
#         sign = df[(df['time'] == time) & (df['experiment'] == experiment)]['is_sign']
#         perc_sign = 100 * sign.sum() / sign.shape[0]
#
#         dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]
#
# dn['area'] = None
# dn['area'] = 'V1'
# if out['pars']['align_to'] == 'response':
#     dn = dn[dn['time'] <= 0]
#
#
# f, ax = plt.subplots(1, 1, figsize=[3, 3])
# sns.lineplot(data=dn, y='perc_sig', x='time', hue='experiment', ax=ax,
#              palette=experiment_palette, markers=True, style='area')
# ax.set_title(combo_labels[combo])
# ax.set_ylabel('% of units with significant AUC')
# ax.set_xlabel('Time [ms]')
# ax.get_legend().remove()
# sns.despine()
# plt.tight_layout()
#
#
# plot_name = 'auc_over_time_combo_{}_{}.{}'.format(combo, settings_name, plot_format)
# f.savefig(os.path.join(plots_folder, plot_name), dpi=400)