import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *

plot_format = 'png'

# combos = [['nov9_stim_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_resp_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_stim_fast_50', 'target_vs_distractor_fast'],
#           ['nov9_resp_fast_50', 'target_vs_distractor_fast']]

# combos = [['nov9_stim', 'target_vs_distractor'],
#           ['nov9_resp', 'target_vs_distractor']]
#
# combos = [['nov10_20_trials', 'target_vs_distractor'],
#           ['nov10_20_trials_resp', 'target_vs_distractor']]


target_combos = [['nov10_20_trials', 'target_vs_distractor']]


# REFERENCE EXPERIMENTT
ref_settings_name = 'nov14'
ref_combo = 'correct_vs_incorrect_target'
ref_experiment = 'correct_vs_incorrect_target_V'

# REFERENCE TIME
ref_time = -295

# REFERENCE COHORT:
output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(ref_combo, ref_settings_name)
output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', ref_settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
out = pickle.load(open(output_full_path, 'rb'))

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


# EXCLUDE AUDITORY
df = out['auc_scores']
df = df[[False if i[-1] == 'A' else True for i in df['experiment']]]
df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(df['session_id'], df['unit_ind'])]

dn = pd.DataFrame(columns=['time', 'experiment', 'overlap', 'new_cells'])

df_time = df[(df['time'] == ref_time) & (df['experiment'] == ref_experiment)]
sign = df_time['is_sign_01']
reference_cohort = df_time['unit_id'][sign]




#settings_name = 'nov9_stim_slow_30'
#combo = 'target_vs_distractor_slow'

for target_settings_name, target_combo in target_combos:

    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(target_combo, target_settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', target_settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    # EXCLUDE AUDITORY

    df = out['auc_scores']
    df = df[[False if i[-1] == 'A' else True for i in df['experiment']]]
    df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(df['session_id'], df['unit_ind'])]

    dn = pd.DataFrame(columns=['time', 'experiment', 'overlap', 'new_cells'])


    for experiment in df['experiment'].unique() :

        for time in df['time'].unique():
            # if time == 45:
            #     raise ValueError

            df_time = df[(df['time'] == time) & (df['experiment'] == experiment)]
            sign = df_time['is_sign_01']
            sign_cohort = df_time['unit_id'][sign]

            overlap = 100 * np.isin(sign_cohort, reference_cohort).sum() / reference_cohort.shape[0]

            new_cells = 100 * (~np.isin(sign_cohort, reference_cohort)).sum() / sign_cohort.shape[0]

            dn.loc[dn.shape[0], :] = [time, experiment, overlap, new_cells]

            # for boot in range(n_boot):
            #     sign_boot = np.random.choice(sign, size=sign.shape[0], replace=True)
            #     perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
            #     dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]

    dn['area'] = None
    dn['area'] = 'V1'
    if out['pars']['align_to'] == 'response':
        dn = dn[dn['time'] <= 0]


    f, ax = plt.subplots(1, 1, figsize=[3, 3])
    sns.lineplot(data=dn, y='overlap', x='time', hue='experiment', ax=ax,
                 palette=experiment_palette, markers=True, style='area',
                 ci='sd')
    ax.axvline(x=ref_time, c=sns.xkcd_rgb['grey'], ls='--')
    #ax.set_title(combo_labels[target_combo])
    ax.set_ylabel('% overlap with reference cohort')
    ax.set_xlabel('Time [ms]')
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()


    plot_name = 'auc_overlap_{}_{}_VS_{}_{}.{}'.format(ref_combo, ref_settings_name, target_combo, target_settings_name, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


    # f, ax = plt.subplots(1, 1, figsize=[3, 3])
    # sns.lineplot(data=dn, y='new_cells', x='time', hue='experiment', ax=ax,
    #              palette=experiment_palette, markers=True, style='area',
    #              ci='sd')
    # ax.axvline(x=ref_time, c=sns.xkcd_rgb['grey'], ls='--')
    # ax.set_title(combo_labels[target_combo])
    # ax.set_ylabel('% significant cells which were not\nin the initial cohort')
    # ax.set_xlabel('Time [ms]')
    # ax.get_legend().remove()
    # sns.despine()
    # plt.tight_layout()
    #
    #
    # plot_name = 'auc_overlap_BOOT_{}_{}_VS_{}_{}.{}'.format(ref_combo, ref_settings_name, target_combo, target_settings_name, plot_format)
    # f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



