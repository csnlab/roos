import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
#from warp import warp_trials
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.metrics import make_scorer
from sklearn.metrics import r2_score
from utils import rolling_window, rebin
from MLencoding import MLencoding
from sklearn.model_selection import KFold
from plotting_style import *
from sklearn.metrics import explained_variance_score


"""
Encode firing rates using different predictors.
"""

settings_name = 'may4_indagine'

align_to = 'stimulus'
binsize = 50

encoding_model = 'random_forest'
encoder_params = {'n_estimators' : 100}

n_splits = 3
n_repeats = 1
encoder_max_time = 1000
encoder_n_filters = 8

# not implemented now, but we might still want to warp for the averages later
warp = False
warp_per_modality = False

# units with average firing rate below this threshold are skipped
full_sess_firing_rate_threshold = 0.5

# set the edges (in ms) of the time epochs in which to evaluate the encoding
epoch_1_t1, epoch_1_t2 = -200, 400
epoch_2_t1, epoch_2_t2 = 600, 1500

# list ids of units you want to encode. if selected_unit_ids = None,
# run over all units
selected_unit_ids = ['E2R-008_4_19',
                     'E2R-008_4_9',
                     'E2R-009_5_4',
                     'E2R-009_7_7',
                     'E2R-009_7_15',
                     'E2R-008_4_8',
                     'E2R-008_4_22',
                     'E2R-006_2_0',
                     'E2R-006_1_11',
                     'E2R-006_1_14']

selected_unit_ids = ['E2R-008_4_19']
#selected_unit_ids = ['E2R-009_7_7']




# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, motion_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
# make sure the session id is a string, so it cannot be confused for an index
trial_df['session_id'] = trial_df['session_id'].astype(str)

if selected_unit_ids is None:
    # run over all sessions
    session_ids = trial_df['session_id'].unique()

else:
    session_ids = []
    for unit_id in selected_unit_ids:
        session_id = unit_id.split('_')[1]
        session_ids.append(session_id)
    session_ids = np.unique(session_ids)

    selected_units_dict = {s : [] for s in session_ids}
    for unit_id in selected_unit_ids:
        selected_units_dict[unit_id.split('_')[1]].append(unit_id)

# TODO can introduce the exact contrast fluctuations over time!
#sessions = trial_df['session_id'].unique()


# --- OUTPUT FILES ---------------------------------------------------------
output_file_name = 'encode_setting_{}.pkl'.format(settings_name)

output_folder = os.path.join(DATA_FOLDER, 'results', 'encode',
                             settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
pars_file_name = 'parameters_encode_setting_{}.pkl'.format(settings_name)
pars_full_path = os.path.join(output_folder, pars_file_name)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


# --- SELECT SESSIONS AND TRIALS ------------------------------------------

n_time_bins_per_trial = len(time_bin_centers)

trial_df = trial_df[np.isin(trial_df['hit_miss'], ['hit', 'miss'])]

# encoded columns
columns =    ['target',
              'hit_miss']

labelencoders = {}
for col in columns:
    labelencoders[col] = LabelEncoder()
    labelencoders[col].fit(trial_df[col])

difficulty_encoder = OneHotEncoder()
difficulty_encoder.fit(trial_df['difficulty'].__array__().astype(int).reshape(-1, 1))

# list of full set of predictors
predictors = ['visual', # presence of visual stimulus
              'audio', # presence of auditory stimulus
              'multisensory', # is it multisensory? (should allow to pick up multisensory enhancemennt)
              'difficulty',
              'target',  # keep only target as a measure of contrast
              'outcome_visual',
              'outcome_audio',
              'response_right',
              'response_left',
              #'hit_miss',
              #'time_from_stim',
              'motion'] # motion always needs to be last predictor

# list of predictors related to sensory stimulus
predictors_stimulus = ['visual',
                       'audio',
                       'multisensory',
                       'difficulty',
                       'target']

# list of predictors related to behavioral response
predictors_behavior = ['outcome_visual',
                       'outcome_audio',
                       'response_right',
                       'response_left',
                       'motion']

# we will encode the firing rate using these sets of predictors
predictor_sets = {'full' : predictors,
                  'stimulus' : predictors_stimulus}
                  #'behavior' : predictors_behavior}


# df = pd.DataFrame(columns=['animal_id',
#                            'session_id',
#                            'unit_id',
#                            'time',
#                            'predictor',
#                            'full_model_score',
#                            'unique_predictor_score',
#                            'std_shuffled_scores',
#                            '95%CI_1',
#                            '95%CI_2',
#                            'predictor_significant'])

# --- OUTPUT FILES ---------------------------------------------------------

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)

for sessn, session_id in enumerate(session_ids):

    output_file_name = 'encode_setting_{}_{}.pkl'.format(settings_name, session_id )
    output_folder = os.path.join(DATA_FOLDER, 'results', 'encode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)


    trial_df_session = trial_df[trial_df['session_id'] == session_id]
    trial_df_session.index = trial_df_session['trial_id']
    trial_df_session['reaction_time'] = trial_df_session['reaction_time'] * 1000
    n_neurons = trial_df_session['n_neurons'].iloc[0]
    animal_id = trial_df_session['animal_id'].unique()[0]

    trial_ids = trial_df[trial_df['session_id'] == session_id]['trial_id'].unique()
    assert len(trial_ids) == trial_df_session.shape[0]
    trial_predictor_data = {}

    # iterate through all the trials and for each trial generate the
    # matrix of predictors
    for i, row in trial_df_session.iterrows():

        reaction_time = row['reaction_time']
        predictor_matrix_components = []
        # IMPORTANT: a single predictor can consist of a >1D variable, so we
        # keep a list of predictors corresponding to each column of the predictor
        # matrix
        predictor_array_labels = []

        if np.isnan(reaction_time):
            stimulus_end_time = 500
        else:
            stimulus_end_time = reaction_time
        # iterate through each predictor label and generate the vector
        # of predictor values
        for predictor in predictors:
            if np.isin(predictor, columns):
                p = labelencoders[predictor].transform(np.repeat(row[predictor],
                                                                 n_time_bins_per_trial))
            elif predictor == 'visual':
                if np.isin(row['modality'], ['V', 'M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    p[np.array(time_bin_centers) <= 0] = 0
                    p[np.array(time_bin_centers) >= stimulus_end_time] = 0

                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'audio':
                if np.isin(row['modality'], ['A', 'M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    p[np.array(time_bin_centers) <= 0] = 0
                    p[np.array(time_bin_centers) >= stimulus_end_time] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'multisensory':
                if np.isin(row['modality'], ['M']):
                    p = np.repeat(1, n_time_bins_per_trial)
                    p[np.array(time_bin_centers) <= 0] = 0
                    p[np.array(time_bin_centers) >= stimulus_end_time] = 0
                else:
                    p = np.repeat(0, n_time_bins_per_trial)

            elif predictor == 'outcome_visual':
                p = np.repeat(0, n_time_bins_per_trial)
                if np.isin(row['modality'], ['V', 'M']) and row['response'] == 1:
                    p[np.array(time_bin_centers) >= reaction_time] = 1
                    p[np.array(time_bin_centers) >= reaction_time+200] = 0

            elif predictor == 'outcome_audio':
                p = np.repeat(0, n_time_bins_per_trial)
                if np.isin(row['modality'], ['A', 'M']) and row['response'] == 1:
                    p[np.array(time_bin_centers) >= reaction_time] = 1
                    p[np.array(time_bin_centers) >= reaction_time+200] = 0

            elif predictor == 'response_right':
                p = np.repeat(0, n_time_bins_per_trial)
                if np.isin(row['response_side'], ['R']):
                    p[np.array(time_bin_centers) >= reaction_time] = 1

            elif predictor == 'response_left':
                p = np.repeat(0, n_time_bins_per_trial)
                if np.isin(row['response_side'], ['L']):
                    p[np.array(time_bin_centers) >= reaction_time] = 1

            elif predictor == 'difficulty':
                diff = np.repeat(row['difficulty'], n_time_bins_per_trial).astype(int).reshape(-1, 1)
                p = difficulty_encoder.transform(diff).T
                p = np.array(p.todense())
                p[:, np.array(time_bin_centers) <= 0] = 0
                p[:, np.array(time_bin_centers) >= stimulus_end_time] = 0

            elif predictor == 'time_from_stim':
                p = np.array(time_bin_centers) / 1000

            elif predictor == 'motion':
                p = motion_data[row['trial_id']]

            if p.shape.__len__() == 1 or p.shape[0] == 1:
                predictor_array_labels.append(predictor)
            else:
                for _ in range(p.shape[0]):
                    predictor_array_labels.append(predictor)
            predictor_matrix_components.append(p)

        predictor_matrix = np.vstack(predictor_matrix_components)
        trial_predictor_data[row['trial_id']] = predictor_matrix.T

    predictor_array_labels = np.array(predictor_array_labels)

    if selected_unit_ids is None:
        # if selected units are not given, run for all units
        selected_unit_ids_session = ['{}_{}_{}'.format(animal_id, session_id, unit_ind)
                              for unit_ind in np.arange(n_neurons)]
        selected_unit_indx_session = np.arange(n_neurons)
    else:
        # otherwise get selected unit ids for the specific session
        selected_unit_ids_session = selected_units_dict[session_id]
        selected_unit_indx_session = [int(s.split('_')[-1]) for s in selected_unit_ids_session]

    tb = np.array(time_bin_centers)

    epoch_1_indx = np.where([np.logical_and(tb >= epoch_1_t1, tb <= epoch_1_t2)])[1]
    epoch_2_indx = np.where([np.logical_and(tb >= epoch_2_t1, tb <= epoch_2_t2)])[1]

    if predictors[-1] != 'motion':
        raise ValueError('Motion needs to always be the last predictor, because'
                         'thats the predictor that gets standardized')

    # PREDICT ALL TOGETHER WITH RANDOM CV

    dfs_output = {}
    spikes_output = {}
    prediction_output = {}

    for unit_ind, unit_id in zip(selected_unit_indx_session, selected_unit_ids_session):

        df = pd.DataFrame(columns=['animal_id',
                                   'session_id',
                                   'unit_id',
                                   'trial_id',
                                   'modality',
                                   'response',
                                   'response_side',
                                   'hit_miss',
                                   'target',
                                   'predictor_set',
                                   'PR2_epoch_1',
                                   'PR2_epoch_2',
                                   'EV_epoch_1',
                                   'EV_epoch_2'])

        predicted_firing_rate = {st : {t : [] for t in trial_ids} for st in predictor_sets.keys()}

        full_sess_firing_rate = np.hstack([trial_data[t][:, unit_ind] for t in trial_ids])
        full_sess_firing_rate_mean = full_sess_firing_rate.mean()

        if full_sess_firing_rate_mean < full_sess_firing_rate_threshold:
            print('Skipping unit {} - average firing rate {:.2f}'.format(unit_id, full_sess_firing_rate_mean))
            continue
        else:
            print('Encoding unit {}'.format(unit_id))

        # we standardize the firing rate which we will try to predict
        # but then it become not poisson
        # all_unit_data = np.hstack([trial_data[t][:, unit_ind] for t in trial_ids]).reshape(-1, 1)
        # su = StandardScaler()
        # su.fit((all_unit_data))
        # spikes_unit = {t : su.transform(trial_data[t][:, unit_ind].reshape(-1, 1)).flatten()
        #                for t in trial_ids}

        spikes_unit = {t : trial_data[t][:, unit_ind] for t in trial_ids}

        for predictor_set, predictor_set_list in predictor_sets.items():
            print('Running encoding for predictor set: {}'.format(predictor_set))
            print('Predictors:\n')
            print(*predictor_set_list, sep='\n')

            predictor_indices_set = [i for i in range(predictor_array_labels.shape[0])
                                     if np.isin(predictor_array_labels[i], np.array(predictor_set_list))]
            predictor_array_labels_set = predictor_array_labels[predictor_indices_set]

            # find the index of the motion predictor if present, so we can
            # standardize it later
            motion_present = np.isin('motion', predictor_array_labels_set)
            if motion_present:
                relative_indx_motion = np.where(np.array(predictor_array_labels_set) == 'motion')[0][0]

            for repeat in range(n_repeats):

                # cross-validation routine, split the trials into train and test
                kfold = KFold(n_splits=n_splits, shuffle=True, random_state=repeat)
                iterable = kfold.split(trial_ids)

                for fold, (training_ind, testing_ind) in enumerate(iterable):
                    trials_ids_train = trial_ids[training_ind]
                    trials_ids_test = trial_ids[testing_ind]

                    # features matrix: the predictors
                    X_predictor_train = np.hstack([trial_predictor_data[t][:, predictor_indices_set].T
                                                   for t in trials_ids_train]).T

                    # X_predictor_test = np.hstack([trial_predictor_data[t].T
                    #                                for t in trials_ids_test]).T

                    # we standardize the motion, which is the only non-categorical
                    # predictor we have
                    if motion_present :
                        ss = StandardScaler()
                        ss.fit(X_predictor_train[:, relative_indx_motion].reshape(-1, 1))
                        X_predictor_train[:, relative_indx_motion] = ss.transform(X_predictor_train[:, relative_indx_motion].reshape(-1, 1)).flatten()

                    # target: binned spikes of the selected unit
                    y_train = np.hstack([spikes_unit[t] for t in trials_ids_train])
                    #y_test = np.hstack([spikes_unit[t] for t in trials_ids_test])

                    # fit the encoder
                    encoder = MLencoding(tunemodel=encoding_model,
                                         cov_history=True,
                                         n_filters=encoder_n_filters,
                                         window=binsize,
                                         max_time=encoder_max_time,
                                         n_every=1)

                    encoder.set_params(encoder_params)
                    encoder.fit(X_predictor_train, y_train)

                    # should this be restricted to the epoch?
                    y_null_epoch_1 = np.hstack([spikes_unit[t][epoch_1_indx] for t in trials_ids_train]).mean()
                    y_null_epoch_2 = np.hstack([spikes_unit[t][epoch_2_indx] for t in trials_ids_train]).mean()

                    y_null_epoch_1 = np.hstack([spikes_unit[t] for t in trials_ids_train]).mean()
                    y_null_epoch_2 = np.hstack([spikes_unit[t] for t in trials_ids_train]).mean()

                    # for every trial in the test set generate and evaluate predictions
                    for tid in trials_ids_test:
                        X_predictor_test = trial_predictor_data[tid][:, predictor_indices_set]
                        # standardize motion
                        if motion_present :
                            X_predictor_test[:, relative_indx_motion] = ss.transform(X_predictor_test[:, relative_indx_motion].reshape(-1, 1)).flatten()

                        # predict the full trial
                        y_test = spikes_unit[tid]

                        y_pred = encoder.predict(X_predictor_test)


                        PR2_epoch_1 = encoder.poisson_pseudoR2(y_test[epoch_1_indx],
                                                               y_pred[epoch_1_indx],
                                                               y_null_epoch_1)

                        PR2_epoch_2 = encoder.poisson_pseudoR2(y_test[epoch_2_indx],
                                                               y_pred[epoch_2_indx],
                                                               y_null_epoch_2)

                        if tid == '4_24' and predictor_set == 'full':
                            y_null_full = y_null_epoch_1
                            y_test_full = y_test
                            y_pred_full = y_pred


                        elif tid == '4_24' and predictor_set == 'stimulus':
                            y_null_stim = y_null_epoch_1
                            y_test_stim = y_test
                            y_pred_stim = y_pred

                            PR2_full = encoder.poisson_pseudoR2(
                                y_test_full[epoch_1_indx],
                                y_pred_full[epoch_1_indx],
                                y_null_full)

                            PR2_stim = encoder.poisson_pseudoR2(
                                y_test_stim[epoch_1_indx],
                                y_pred_stim[epoch_1_indx],
                                y_null_stim)

                            def D(y, yhat):
                                eps = np.spacing(1)
                                D = np.sum(y * np.log(eps + y/yhat) - (y - yhat))
                                return D



                            d_stim = D(y_test_stim[epoch_1_indx], y_pred_stim[epoch_1_indx])

                            d_full = D(y_test_full[epoch_1_indx], y_pred_full[epoch_1_indx])

                            d_total = D(y_test_full[epoch_1_indx], y_null_full)

                            ev_stim = 1 - d_stim / d_total

                            ev_full = 1 - d_full / d_total

                            # EV_epoch_f = explained_variance_score(
                            #     y_true=y_test_full[epoch_1_indx],
                            #     y_pred=y_pred_full[epoch_1_indx])
                            #
                            # EV_epoch_s = explained_variance_score(
                            #     y_true=y_test_stim[epoch_1_indx],
                            #     y_pred=y_pred_stim[epoch_1_indx])
                            #
                            # yhat  = y_pred_full[epoch_1_indx]
                            # y     = y_test_full[epoch_1_indx]
                            # ynull = y_null_full
                            #
                            # yhat  = y_pred_stim[epoch_1_indx]
                            # y     = y_test_stim[epoch_1_indx]
                            # ynull = y_null_stim
                            #
                            # yhat = yhat.reshape(y.shape)
                            # eps = np.spacing(1)
                            # L1 = np.sum(y * np.log(eps + yhat) - yhat)
                            # L1_v = y * np.log(eps + yhat) - yhat
                            # L0 = np.sum(y * np.log(eps + ynull) - ynull)
                            # LS = np.sum(y * np.log(eps + y) - y)
                            # R2 = 1 - (LS - L1) / (LS - L0)
                            #
                            #
                            # # when negative L1 < L0
                            # print(L0)
                            # print(R2)
                            #
                            #
                            # f, ax = plt.subplots(3, 1, sharex=True)
                            # ax[0].axvspan(epoch_1_t1, epoch_1_t2, color='grey', alpha=0.2)
                            # ax[0].axvline(0, ls='--')
                            # ax[0].plot(np.array(time_bin_centers)[epoch_1_indx], y_test_stim[epoch_1_indx], c='k')
                            # ax[0].plot(np.array(time_bin_centers)[epoch_1_indx], y_pred_stim[epoch_1_indx], ls='--',c=modality_palette[trial_df_session.loc[tid]['modality']])
                            # ax[0].plot(np.array(time_bin_centers)[epoch_1_indx], y_pred_full[epoch_1_indx], c=modality_palette[trial_df_session.loc[tid]['modality']])
                            # ax[0].axhline(y_null_full)
                            # ax[0].set_title('model {} - repeat {}\nPR2={:.3f}'.format(predictor_set, repeat, PR2_epoch_1))
                            # #raise ValueError
                            #
                            # yhat  = y_pred_full[epoch_1_indx]
                            # y     = y_test_full[epoch_1_indx]
                            # ynull = y_null_full
                            #
                            # ax[1].plot(np.array(time_bin_centers)[epoch_1_indx],
                            #            y * np.log(eps + np.repeat(ynull, len(y))) - np.repeat(ynull, len(y)), label='L0')
                            # ax[1].plot(np.array(time_bin_centers)[epoch_1_indx],
                            #            y * np.log(eps + yhat) - yhat, label='L1')
                            # ax[1].legend()
                            #
                            # yhat  = y_pred_stim[epoch_1_indx]
                            # y     = y_test_stim[epoch_1_indx]
                            # ynull = y_null_stim
                            #
                            # ax[2].plot(np.array(time_bin_centers)[epoch_1_indx],
                            #            y * np.log(eps + ynull) - ynull, label='L0')
                            # ax[2].plot(np.array(time_bin_centers)[epoch_1_indx],
                            #            y * np.log(eps + yhat) - yhat, label='L1')
                            # ax[2].legend()


                        #EV_epoch_1 = 1 - (np.var(y_test[epoch_1_indx] - y_pred[epoch_1_indx])/np.var(y_test[epoch_1_indx]))
                        #EV_epoch_2 = 1 - (np.var(y_test[epoch_2_indx] - y_pred[epoch_2_indx])/np.var(y_test[epoch_2_indx]))
                        # The sklearn implementation takes care of the Nan and -Inf
                        # that occur when y_true is constant
                        EV_epoch_1 = explained_variance_score(y_true=y_test[epoch_1_indx],
                                                              y_pred=y_pred[epoch_1_indx])

                        EV_epoch_2 = explained_variance_score(y_true=y_test[epoch_2_indx],
                                                              y_pred=y_pred[epoch_2_indx])

                        predicted_firing_rate[predictor_set][tid].append(y_pred)

                        modality = trial_df_session.loc[tid]['modality']
                        response = trial_df_session.loc[tid]['response']
                        response_side = trial_df_session.loc[tid]['response_side']
                        hit_miss = trial_df_session.loc[tid]['hit_miss']
                        target = trial_df_session.loc[tid]['target']

                        row = [animal_id,
                               session_id,
                               unit_id,
                               tid,
                               modality,
                               response,
                               response_side,
                               hit_miss,
                               target,
                               predictor_set,
                               PR2_epoch_1,
                               PR2_epoch_2,
                               EV_epoch_1,
                               EV_epoch_2]

                        df.loc[df.shape[0], :] = row


                    print('___ encoding unit {} of {} - repeat {} of {} - fold {} of {}'.format(
                             unit_id, n_neurons, repeat, n_repeats, fold, n_splits))
                    print('_______ average PR2 ({}-{}ms) = {:.3f}'.format(epoch_1_t1, epoch_1_t2, PR2_epoch_1))
                    print('_______ average PR2 ({}-{}ms) = {:.3f}'.format(epoch_2_t1, epoch_2_t2, PR2_epoch_2))


            # --- PLOT DESIGN MATRIX PER TRIAL TYPE (as a sanity check) ---

            # f, ax = plt.subplots(6, 3, figsize=[10, 10],
            #                      sharex=True, sharey=True)
            #
            # columns = ['target', 'target', 'response', 'response',
            #            'hit_miss', 'hit_miss'] #'response_side', 'response_side']
            #
            # vals = ['target', 'distractor', 0, 1, 'hit', 'miss']
            #
            # for j, modality in enumerate(modalities):
            #     for i, (col, val) in enumerate(zip(columns, vals)):
            #
            #         dx = df[(df['modality'] == modality) & (df[col] == val)]
            #
            #         tids = dx['trial_id'].unique()
            #
            #         if len(tids) >= 5:
            #             pass


        # casting response and response_side as object, so the nans don't cause
        # them to be treated as floats
        for col in ['animal_id', 'session_id', 'unit_id', 'trial_id',
                    'modality', 'response', 'response_side', 'hit_miss', 'target']:
            df[col] = df[col].apply(str)

        # --- OUTPUT ---

        dfs_output[unit_id] = df
        spikes_output[unit_id] = spikes_unit
        prediction_output[unit_id] = predicted_firing_rate

    pars = {'time_bin_centers' : time_bin_centers,
            'predictor_sets' : predictor_sets,
            'epoch_1' : [epoch_1_t1, epoch_1_t2],
            'epoch_2' : [epoch_2_t1, epoch_2_t2],
            'full_sess_firing_rate_threshold' : full_sess_firing_rate_threshold,
            'warp' : warp,
            'warp_per_modality' : warp_per_modality,
            'n_splits' : n_splits,
            'n_repeats' : n_repeats,
            'binsize' : binsize,
            'align_to' : align_to,
            'encoding_model' : encoding_model,
            'encoder_params' : encoder_params,
            'encoder_max_time' : encoder_max_time,
            'encoder_n_filters' : encoder_n_filters,
            'settings_name' : settings_name}

    output = {'dfs' : dfs_output,
              'spikes' : spikes_output,
              'predictions' : prediction_output,
              'trial_df' : trial_df_session,
              'pars' : pars}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(output, open(output_full_path, 'wb'))






# predictors_to_plot = ['outcome_visual',
#                       'outcome_audio',
#                       #'response_right',
#                       #'response_left',
#                       'hit_miss',
#                       #'difficulty',
#                       'target', # keep only target as a measure of contrast
#                       'visual',
#                       'audio',
#                       'multisensory']
#
#
# predictor_palette = {'outcome_visual' : sns.xkcd_rgb['dark red'],
#                      'outcome_audio' : sns.xkcd_rgb['dark green'],
#                      'response_right' : sns.xkcd_rgb['teal'],
#                      'response_left' : sns.xkcd_rgb['brown'],
#                      'difficulty' : sns.xkcd_rgb['orange'],
#                      'target' : sns.xkcd_rgb['purple'],
#                      'visual' : sns.xkcd_rgb['green'],
#                      'audio' : sns.xkcd_rgb['red'],
#                      'multisensory' : sns.xkcd_rgb['blue'],
#                      'hit_miss' : sns.xkcd_rgb['pink']}
#
#
# plot_format = 'png'
# plots_folder = os.path.join(DATA_FOLDER, 'plots', 'encode_single_units')
# if not os.path.isdir(plots_folder):
#     os.makedirs(plots_folder)
#
#
# for unit_id in selected_unit_ids:
#
#     from plotting_style import *
#
#
#
#     f, ax = plt.subplots(1, 1, figsize=[4, 4])
#
#     # sns.lineplot(data=df[df['unit_id'] == unit_id],
#     #              x='time', y='unique_predictor_score', hue='predictor', ax=ax)
#
#     for predictor in predictors:
#         dx = df[(df['unit_id'] == unit_id) & (df['predictor'] == predictor)]
#         y =  dx['unique_predictor_score']
#         ax.plot(dx['time'], y, linewidth=2,
#                 c=predictor_palette[predictor], label=predictor)
#
#         CI95 = dx['95%CI_1'] - dx['95%CI_2'] / 2
#         STD = dx['std_shuffled_scores'] /2
#
#         ERR = STD
#
#         ax.fill_between(dx['time'], y-ERR, y+ERR,
#                         color=predictor_palette[predictor],
#                         alpha=0.3, linewidth=0)
#
#
#     ax.set_title('{}'.format(unit_id))
#     ax.set_ylabel('Unique explained variance')
#     ax.set_xlabel('Time [ms]')
#     ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
#     ax.legend(prop={'size': 6})
#
#     if warp:
#         for modality in modalities :
#             ax.axvline(median_reaction_times[modality],
#                        c=modality_palette[modality],
#                        ls='--')
#     # ax.set_ylim([0, 30])
#     #ax.set_ylim([-0.2,0.2])
#     sns.despine()
#     plt.tight_layout()
#
#     plot_name = 'encode_v2_{}_{}.{}'.format(settings_name, unit_id, plot_format)
#     f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



