import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.metrics import roc_auc_score


settings_name = 'test9'
align_to = 'stimulus'
binsize = 50
min_units = 1
min_trials_per_class = 25
start_time_in_ms = 25
end_time_in_ms = 250

n_bootstraps = 100
seed = 92


# --- OUTPUT FILES -------------------------------------------------------------
output_file_name = 'auc_scores_setting_target_vs_distractor.pkl'.format(settings_name)
output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


# --- LOAD DATA ----------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize, align_to=align_to)
session_ids = trial_df['session_id'].unique()


# --- SELECT SESSIONS AND TRIALS -----------------------------------------------


df = pd.DataFrame(
    columns=['session_id', 'experiment', 'binsize', 'unit_ind',
             'n_trials_per_class', 'score_observed',
             'p_val_1', 'p_val_2', 'is_sign', 'is_sign_05'])


for experiment in ['target_vs_distractor_V', 'target_vs_distractor_M']:

    if experiment == 'target_vs_distractor_V':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'V']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'V']
            trial_ids = sdf['trial_id']
            target = [1 if t=='target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)


    elif experiment == 'target_vs_distractor_M':
        session_df = pd.DataFrame(columns=['session_id', 'n_units', 'n1', 'n2'])

        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'M']
            n1 = sdf[sdf['target'] == 'target'].shape[0]
            n2 = sdf[sdf['target'] == 'distractor'].shape[0]
            n_neu = sdf['n_neurons'].iloc[0]
            session_df.loc[session_id, :] = [session_id, n_neu, n1, n2]

        dp = []
        for session_id in session_ids:
            sdf = trial_df[trial_df['session_id'] == session_id]
            sdf = sdf[sdf['modality'] == 'M']
            trial_ids = sdf['trial_id']
            target = [1 if t=='target' else 0 for t in sdf['target']]
            dx = pd.DataFrame(columns=['session_id', 'trial_numbers', 'target'])
            dx['trial_numbers'] = trial_ids
            dx['target'] = target
            dx['session_id'] = session_id
            dp.append(dx)
        dp = pd.concat(dp)

    else:
        raise ValueError



    n_time_bins_per_trial = trial_data['0_0'].shape[0]

    selsess_df = session_df[(session_df['n1'] >= min_trials_per_class) &
                                (session_df['n2'] >= min_trials_per_class) &
                                (session_df['n_units'] >= min_units)]
    selected_session_ids = selsess_df['session_id'].__array__()

    print(selsess_df)


    for session_id in selsess_df['session_id']:
        d0 = dp[(dp['session_id'] == session_id) & (dp['target'] == 0)]
        d1 = dp[(dp['session_id'] == session_id) & (dp['target'] == 1)]
        # print(d0.shape)
        # print(d1.shape)
        min_trials_sess = min(d0.shape[0], d1.shape[0])

        t0 = np.random.choice(d0['trial_numbers'],
                              size=min_trials_sess,
                              replace=False)
        t1 = np.random.choice(d1['trial_numbers'],
                              size=min_trials_sess,
                              replace=False)

        resampled_trials = np.hstack([t0, t1])

        n_neurons = trial_df[trial_df['session_id'] == session_id]['n_neurons'].iloc[0]

        # MAKE TARGET
        y = np.hstack([np.zeros(min_trials_sess),
                       np.ones(min_trials_sess)]).astype(int)

        for unit_ind in np.arange(n_neurons):

            scores_observed = []
            for time_bin in range(n_time_bins_per_trial):
                time = time_bin_centers[time_bin]
                # time0, time1 = time_bin_edges[time_bin]

                if time >= start_time_in_ms and time <= end_time_in_ms:
                    X = np.vstack([trial_data[t][time_bin, unit_ind] for t in resampled_trials])
                    score = roc_auc_score(y, X)
                    scores_observed.append(score)

            score_observed = np.mean(scores_observed)

            scores_null = []
            for n in range(n_bootstraps):
                y_perm = np.random.permutation(y)
                score_null_overtime = []

                for time_bin in range(n_time_bins_per_trial) :
                    time = time_bin_centers[time_bin]
                    # time0, time1 = time_bin_edges[time_bin]
                    if time >= start_time_in_ms and time <= end_time_in_ms :
                        X = np.vstack([trial_data[t][time_bin, unit_ind] for t in resampled_trials])
                        score = roc_auc_score(y_perm, X)
                        score_null_overtime.append(score)
                scores_null.append(np.mean(score_null_overtime))

            p_val_1 = (np.array(scores_null) > score_observed).sum() / len(scores_null)
            p_val_2 = (np.array(scores_null) <= score_observed).sum() / len(scores_null)

            alpha = 0.01
            q1 = np.quantile(scores_null, alpha / 2)
            q2 = np.quantile(scores_null, 1 - alpha / 2)
            is_sign = score_observed < q1 or score_observed > q2

            alpha = 0.05
            q1_05 = np.quantile(scores_null, alpha / 2)
            q2_05 = np.quantile(scores_null, 1 - alpha / 2)
            is_sign_05 = score_observed < q1_05 or score_observed > q2_05

            row = [session_id, experiment, binsize, unit_ind,
                   min_trials_sess, score_observed, p_val_1, p_val_2, is_sign,
                   is_sign_05]

            df.loc[df.shape[0], :] = row

numeric_cols = ['score_observed', 'p_val_1', 'p_val_2']
for col in numeric_cols :
    df[col] = pd.to_numeric(df[col])
df['is_sign'] = df['is_sign'].astype(bool)
df['is_sign_05'] = df['is_sign_05'].astype(bool)

pars = {'settings_name' : settings_name,
        'experiment' : experiment,
        'align_to' : align_to,
        'binsize' : binsize,
        'min_trials_per_class' : min_trials_per_class,
        'start_time_in_ms' : start_time_in_ms,
        'end_time_in_ms' : end_time_in_ms,
        'n_bootstraps' : n_bootstraps,
        'seed' : seed}

out = {'pars' : pars,
       'auc_scores' : df}

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))

