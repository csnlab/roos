import os
from constants import *
import pickle

settings_name = 'test9'


output_file_name = 'auc_scores_setting_target_vs_distractor.pkl'.format(settings_name)
output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
out = pickle.load(open(output_full_path, 'rb'))

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


df = out['auc_scores']

# --- PRINT NUMBER OF SIGNIFICANT CELLS ---
sign_v = df[df['experiment'] == 'target_vs_distractor_V']['is_sign']
perc_sign_v = 100*sign_v.sum() / sign_v.shape[0]

sign_m = df[df['experiment'] == 'target_vs_distractor_M']['is_sign']
perc_sign_m = 100*sign_m.sum() / sign_m.shape[0]

print('alpha = 0.01')
print('% sign. cells V trials: {:.1f}'.format(perc_sign_v))
print('% sign. cells M trials: {:.1f}'.format(perc_sign_m))


sign_v = df[df['experiment'] == 'target_vs_distractor_V']['is_sign_05']
perc_sign_v = 100*sign_v.sum() / sign_v.shape[0]

sign_m = df[df['experiment'] == 'target_vs_distractor_M']['is_sign_05']
perc_sign_m = 100*sign_m.sum() / sign_m.shape[0]

print('alpha = 0.05')
print('% sign. cells V trials: {:.1f}'.format(perc_sign_v))
print('% sign. cells M trials: {:.1f}'.format(perc_sign_m))

