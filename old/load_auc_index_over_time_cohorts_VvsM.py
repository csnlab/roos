import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *

plot_format = 'png'

# combos = [['nov9_stim_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_resp_slow_50', 'target_vs_distractor_slow'],
#           ['nov9_stim_fast_50', 'target_vs_distractor_fast'],
#           ['nov9_resp_fast_50', 'target_vs_distractor_fast']]

combos = [['nov9_stim', 'target_vs_distractor']]

combos = [['nov10', 'target_vs_distractor']]

combos = [['nov10_20_trials', 'target_vs_distractor']]


# TIME WITH WHICH WE COMPARE FOR OVERLAP
# t0 = 45

#settings_name = 'nov9_stim_slow_30'
#combo = 'target_vs_distractor_slow'

for settings_name, combo in combos:

    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))

    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

    n_boot = 200

    # EXCLUDE AUDITORY

    df = out['auc_scores']
    df = df[[False if i[-1] == 'A' else True for i in df['experiment']]]
    df['unit_id'] = ['{}_{}'.format(a, b) for a,b in zip(df['session_id'], df['unit_ind'])]

    dn = pd.DataFrame(columns=['time', 'experiment', 'overlap', 'total_sign'])

    dn2 = pd.DataFrame(columns=['time', 'experiment', 'unique_cells'])


    experiments = df['experiment'].unique()

    for time in df['time'].unique():

        df_time1 = df[(df['time'] == time) & (df['experiment'] == experiments[0])]
        sign1 = df_time1['is_sign_01']
        sign_cohort_1 = df_time1['unit_id'][sign1]

        df_time2 = df[(df['time'] == time) & (df['experiment'] == experiments[1])]
        sign2 = df_time2['is_sign_01']
        sign_cohort_2 = df_time2['unit_id'][sign2]

        total_sign = (sign_cohort_1.shape[0] + sign_cohort_2.shape[0])

        overlap = 100 * np.isin(sign_cohort_1, sign_cohort_2).sum() / (sign_cohort_1.shape[0] + sign_cohort_2.shape[0])

        unique_1_cells = 100 * (~np.isin(sign_cohort_1, sign_cohort_2)).sum() /(sign_cohort_1.shape[0] + sign_cohort_2.shape[0])
        unique_2_cells = 100 * (~np.isin(sign_cohort_2, sign_cohort_1)).sum() / (sign_cohort_1.shape[0] + sign_cohort_2.shape[0])

        dn.loc[dn.shape[0], :] = [time, 'V_vs_M', overlap, total_sign]

        dn2.loc[dn2.shape[0], :] = [time, experiments[0], unique_1_cells]
        dn2.loc[dn2.shape[0], :] = [time, experiments[1], unique_2_cells]

        # for boot in range(n_boot):
        #     sign_boot = np.random.choice(sign, size=sign.shape[0], replace=True)
        #     perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
        #     dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]

    dn['area'] = None
    dn['area'] = 'V1'
    if out['pars']['align_to'] == 'response':
        dn = dn[dn['time'] <= 0]

    dn2['area'] = None
    dn2['area'] = 'V1'
    if out['pars']['align_to'] == 'response':
        dn2 = dn2[dn2['time'] <= 0]

    dn.loc[dn['time'] <= 6, 'overlap'] = np.nan
    dn2.loc[dn2['time'] <= 6, 'unique_cells'] = np.nan

    f, ax = plt.subplots(1, 1, figsize=[3, 3])
    sns.lineplot(data=dn, y='overlap', x='time', color=sns.xkcd_rgb['dark yellow'], ax=ax,
                 palette=experiment_palette, markers=True, style='area',
                 ci='sd')
    sns.lineplot(data=dn, y='total_sign', x='time', color=sns.xkcd_rgb['black'], ax=ax,
                 palette=experiment_palette, markers=True, style='area',
                 ci='sd')
    sns.lineplot(data=dn2, y='unique_cells', x='time', hue='experiment', ax=ax,
                 palette=experiment_palette, markers=True, style='area',
                 ci='sd')
    #ax.axvline(x=t0, c=sns.xkcd_rgb['grey'], ls='--')
    ax.set_title(combo_labels[combo])
    ax.set_ylabel('')
    ax.set_xlabel('Time [ms]')
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()


    plot_name = 'auc_overlap_V_vs_M_combo_bootstrapped_{}_{}.{}'.format(combo, settings_name, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



