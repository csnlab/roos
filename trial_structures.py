import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from sklearn.metrics import roc_auc_score
from warp import warp_trials
from utils import select_data_experiment, get_experiments_combo
from utils import rolling_window
from utils import fix_animal_id
from plotting_style import *

"""
Explore different correlations between trial type/behavior that 
may explain certain strange effects we see in AUC (e.g. late contrast coding).
"""


settings_name = 'jan26_smallbins'
align_to = 'stimulus'
binsize = 50
min_units = 1
min_trials_per_class = 8 #20
start_time_in_ms = -1000
end_time_in_ms = 1000 #1500
warp = False
warp_per_modality = True
larger_bins = False
subsample_majority_class = False # if False use all trials!

n_bootstraps = 250
seed = 92

plot_format = 'png'

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'trials')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

# --- LOAD DATA ----------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   add_reaction_speed_quantiles=False,
                                                   enforce_min_max_reaction_times=True)
trial_df_all = trial_df



if warp:
    trial_data, trial_df, median_reaction_times = warp_trials(trial_data,
                                                              trial_df,
                                                              time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True)
else:
    median_reaction_times = {}

if larger_bins:
    trial_data, time_bin_centers = rolling_window(trial_data, time_bin_centers)




print(trial_df.groupby(['animal_id', 'session_id', 'target', 'response']).size().unstack(fill_value=0))

for modality in ['V', 'M', None]:
    df = pd.DataFrame(
        columns=['animal_id', 'session_id', 'target', 'percentage_correct'])

    animal_ids = trial_df['animal_id'].unique()
    session_ids = trial_df['animal_id'].unique()

    for animal_id in animal_ids:
        session_ids = trial_df[trial_df['animal_id'] == animal_id]['session_id'].unique()
        for session_id in session_ids:
            sdf = trial_df[(trial_df['animal_id'] == animal_id) &
                           (trial_df['session_id'] == session_id)]

            if modality is not None:
                sdf = sdf[sdf['modality'] == modality]

            for col in ['target', 'distractor']:
                v = sdf[sdf['target'] == col]['response'].value_counts()
                try:
                    n1 = v.loc[1]
                except KeyError:
                    n1 = 0

                try:
                    n0 = v.loc[0]
                except KeyError:
                    n0 = 0

                if n0 > 0 or n1 > 0:

                    perc = 100 * n1 / (n0+n1)
                    df.loc[df.shape[0], :] = [animal_id, session_id, col, perc]
                else:
                    print('Session {} modality {} no responses'.format(session_id, modality))

    f, ax = plt.subplots(1, 1, figsize=[6, 6])
    sns.barplot(data=df, x='session_id', y='percentage_correct',
                hue='target', ax=ax)
    ax.set_ylabel('% correct trials')
    ax.set_xlabel('Sessions')
    sns.despine()
    plt.tight_layout()

    plot_name = 'target_vs_response_session_{}.{}'.format(modality, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


    f, ax = plt.subplots(1, 1, figsize=[6, 6])

    sns.barplot(data=df, x='animal_id', y='percentage_correct',
                hue='target', ax=ax)
    ax.set_ylabel('% correct trials')
    sns.despine()
    plt.tight_layout()

    plot_name = 'target_vs_response_animal_{}.{}'.format(modality, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)





df = pd.DataFrame(
    columns=['animal_id', 'session_id', 'modality', 'difference_in_perc_correct'])

for modality in ['V', 'M'] :
    animal_ids = trial_df['animal_id'].unique()
    session_ids = trial_df['animal_id'].unique()

    for animal_id in animal_ids:
        session_ids = trial_df[trial_df['animal_id'] == animal_id]['session_id'].unique()
        for session_id in session_ids:
            sdf = trial_df[(trial_df['animal_id'] == animal_id) &
                           (trial_df['session_id'] == session_id)]

            sdf = sdf[sdf['modality'] == modality]

            percs = []
            for col in ['target', 'distractor']:
                v = sdf[sdf['target'] == col]['response'].value_counts()
                try:
                    n1 = v.loc[1]
                except KeyError:
                    n1 = 0

                try:
                    n0 = v.loc[0]
                except KeyError:
                    n0 = 0

                if n0 > 0 or n1 > 0:

                    perc = 100 * n1 / (n0+n1)
                    percs.append(perc)

            if len(percs) == 2:
                diff_perc = percs[0]-percs[1]

                df.loc[df.shape[0], :] = [animal_id, session_id, modality, diff_perc]

f, ax = plt.subplots(1, 1, figsize=[6, 6])
sns.barplot(data=df, x='animal_id', y='difference_in_perc_correct',
            hue='modality', ax=ax, palette=modality_palette)
ax.set_ylabel('difference in % correct between targ. and dist.')
ax.set_xlabel('Sessions')
sns.despine()
plt.tight_layout()

plot_name = 'target_vs_response_session_DIFF.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)




# ------------------------------------------------------------------------------

df = pd.DataFrame(
    columns=['animal_id', 'session_id', 'target', 'percentage_response_side'])

animal_ids = trial_df['animal_id'].unique()
session_ids = trial_df['animal_id'].unique()

for animal_id in animal_ids:
    session_ids = trial_df[trial_df['animal_id'] == animal_id]['session_id'].unique()
    for session_id in session_ids:
        sdf = trial_df[(trial_df['animal_id'] == animal_id) &
                       (trial_df['session_id'] == session_id) &
                       (trial_df['response'] == 1)]
        for col in ['target', 'distractor']:
            v = sdf[sdf['target'] == col]['response_side'].value_counts()
            perc = 100 * v.loc['L'] / (v.loc['L']+v.loc['R'])
            df.loc[df.shape[0], :] = [animal_id, session_id, col, perc]


f, ax = plt.subplots(1, 1, figsize=[6, 6])

sns.barplot(data=df, x='session_id', y='percentage_correct',
            hue='target', ax=ax)
ax.set_ylabel('% correct trials')
ax.set_xlabel('Sessions')
sns.despine()
plt.tight_layout()

plot_name = 'target_vs_response_session.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


f, ax = plt.subplots(1, 1, figsize=[6, 6])

sns.barplot(data=df, x='animal_id', y='percentage_correct',
            hue='target', ax=ax)
ax.set_ylabel('% correct trials')
sns.despine()
plt.tight_layout()

plot_name = 'target_vs_response_animal.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)





print(trial_df.groupby(['animal_id', 'session_id', 'modality', 'response']).size().unstack(fill_value=0))





df = pd.DataFrame(columns=['animal_id', 'session_id', 'modality', 'response',
                           'perc_target', 'perc_left'])

for modality in ['V', 'M', 'A'] :
    animal_ids = trial_df['animal_id'].unique()
    session_ids = trial_df['animal_id'].unique()

    for animal_id in animal_ids:
        session_ids = trial_df[trial_df['animal_id'] == animal_id]['session_id'].unique()

        for session_id in session_ids:
            sdf = trial_df[(trial_df['animal_id'] == animal_id) &
                           (trial_df['session_id'] == session_id) &
                           (trial_df['modality'] == modality)]


            for response in [0, 1]:
                nresp = sdf.groupby(['response']).size().shape[0]
                if nresp > 1:

                    sz = sdf.groupby(['response', 'target']).size().unstack(fill_value=0)
                    perc_target = 100 * sz.loc[response]['target'] / (sz.loc[response]['target'] + sz.loc[response]['distractor'])

                    sz = sdf.groupby(['response', 'response_side']).size().unstack(fill_value=0)
                    perc_left = 100 * sz.loc[response]['L'] / (sz.loc[response]['R'] + sz.loc[response]['L'])

                    # sz = sdf.groupby(['response', 'hit_miss']).size().unstack(fill_value=0)
                    # perc_hit = 100 * sz.loc[0]['hit'] / (sz.loc[0]['hit'] + sz.loc[0]['miss'])

                    df.loc[df.shape[0], :] = [animal_id, session_id, modality,
                                              response, perc_target, perc_left]

for key in ['perc_target', 'perc_left']:
    f, ax = plt.subplots(1, 2, figsize=[10, 5])
    sns.barplot(data=df[df['response'] == 1], x='animal_id', y=key,
                hue='modality', ax=ax[0], palette=modality_palette)

    sns.barplot(data=df[df['response'] == 0], x='animal_id', y=key,
                hue='modality', ax=ax[1], palette=modality_palette)

    ax[0].set_ylabel(key)
    ax[0].set_xlabel('Sessions')
    ax[1].set_xlabel('Sessions')
    ax[0].set_title('Correct trials')
    ax[1].set_title('Incorrect trials')

    sns.despine()
    plt.tight_layout()

    plot_name = '{}_in_correct_vs_incorrect.{}'.format(key, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)




# for string in ['target', 'distractor']:
#     print('\n{} trials\n'.format(string))
#     for contrast in ['response', 'response_side', 'difficulty']:
#         valc = sdf[sdf['target'] == string][contrast].value_counts()
#         index = valc.index
#         if len(index) == 2:
#             n1 = valc.loc[index[0]]
#             n2 = valc.loc[index[1]]
#             if n1 < n2:
#                 perc = 100*n1/n2
#             elif n2 <= n1:
#                 perc = 100*n2/n1
#             print('   {}: {} vs {} ({:.1f}%)'.format(contrast, n1, n2, perc))
#
#         elif len(index) == 3:
#             n1 = valc.loc[index[0]]
#             n2 = valc.loc[index[1]]
#             n3 = valc.loc[index[2]]
#             tot = n1 + n2 + n3
#             perc1, perc2, perc3 = 100*n1/tot, 100*n2/tot, 100*n3/tot
#             print('   {}: {:.1f}% - {:.1f}% - {:.1f}%'.format(contrast, perc1, perc2, perc3))
#
