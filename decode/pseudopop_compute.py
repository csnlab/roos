import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from utils import rolling_window

"""
Pseudopopulation script. 

I had a check to set nans to zero but there should not be any nans anymore.
"""


settings_name = 'jan30'


experiments = ['target_vs_distractor_with_misses_V',
               'target_vs_distractor_with_misses_M',
               'target_vs_distractor_with_misses_A']

experiments = ['correct_vs_incorrect_V',
               'correct_vs_incorrect_M',
               'correct_vs_incorrect_A',
               'hit_miss_V',
               'hit_miss_M',
               'hit_miss_A',
               'target_vs_distractor_V',
               'target_vs_distractor_M',
               'target_vs_distractor_A']


# experiments = ['difficulty_distractor_VM',
#                'difficulty_target_VM']



align_to = 'stimulus'
binsize = 200
min_units = 1
#min_trials_per_class = 25
min_trials_per_class = 20 #15
subpopulation_sizes = [100]

warp = True
warp_per_modality = True

# DECODING PARAMETERS
decoder_name = 'SGD'
n_splits = 3
n_repeats = 5
n_bootstraps = 500
score_name = 'accuracy'
shuffle_kfold = True

seed = 92

# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

trial_df.loc[trial_df['hit_miss'] == 'late', 'hit_miss'] = 'miss'


if warp:
    trial_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True)
else:
    median_reaction_times = {}


for experiment in experiments:


    # --- OUTPUT FILES ---------------------------------------------------------
    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name,
                                                         experiment)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'pseudodec',
                                 settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    pars_file_name = 'parameters_decode_setting_{}_{}.pkl'.format(settings_name,
                                                                  experiment)
    pars_full_path = os.path.join(output_folder, pars_file_name)

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)


    # --- SELECT SESSIONS AND TRIALS ------------------------------------------
    session_df, sdf, dp = select_data_experiment(experiment, trial_df)


    n_time_bins_per_trial = trial_data['0_0'].shape[0]

    selsess_df = session_df[(session_df['n1'] >= min_trials_per_class) &
                                (session_df['n2'] >= min_trials_per_class) &
                                (session_df['n_units'] >= min_units)]
    selected_session_ids = selsess_df['session_id'].__array__()

    tot_n_units = selsess_df['n_units'].sum()

    print('\n\nEXPERIMENT: {}'.format(experiment))
    print('\n # units: {}'.format(tot_n_units))
    print(selsess_df)

    assert tot_n_units > max(subpopulation_sizes)


    # --- DECODE -------------------------------------------------------------------
    df = pd.DataFrame(columns=['experiment', 'binsize', 'group_size', 'time_bin',
                               'time', #'t0', 't1',
                               'bootstrap', 'score'])

    for pi, subpop_size in enumerate(subpopulation_sizes) :

        for boot in range(n_bootstraps) :

            # RESAMPLE TRIALS
            resampled_trials = {}
            for session_id in selsess_df['session_id']:

                d0 = dp[(dp['session_id'] == session_id) & (dp['target'] == 0)]
                d1 = dp[(dp['session_id'] == session_id) & (dp['target'] == 1)]
                # print(d0.shape)
                # print(d1.shape)
                t0 = np.random.choice(d0['trial_numbers'],
                                      size=min_trials_per_class,
                                      replace=False)
                t1 = np.random.choice(d1['trial_numbers'],
                                      size=min_trials_per_class,
                                      replace=False)
                resampled_trials[session_id] = np.hstack([t0, t1])

            # RESAMPLE UNITS
            u_indx = np.random.choice(np.arange(tot_n_units), size=subpop_size,
                                      replace=False)
            # MAKE TARGET
            y = np.hstack([np.zeros(min_trials_per_class),
                           np.ones(min_trials_per_class)]).astype(int)

            # TODO seed should be boot?
            kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                    random_state=seed)

            print(
                'pop. size {}/{} bootstrap {}/{}'.format(
                    pi + 1, len(subpopulation_sizes),
                    boot, n_bootstraps))

            for time_bin in range(n_time_bins_per_trial):
                time = time_bin_centers[time_bin]
                #time0, time1 = time_bin_edges[time_bin]

                X = []
                for session_id in selsess_df['session_id'] :
                    s = np.vstack([trial_data[t][time_bin, :] for t in resampled_trials[session_id]])

                    # s = np.vstack([ds[session_id][t][:, time_bin] for t in
                    #                resampled_trials[session_id]])
                    X.append(s)
                X = np.hstack(X)
                X = X[:, u_indx]

                if np.isnan(X).sum():
                    raise ValueError('There are nans in the data')

                # TODO remove this!
                #X[np.isnan(X)] = 0

                kfold_scores = []
                y_test_all, y_pred_all = [], []

                for fold, (training_ind, testing_ind) in enumerate(
                        kfold.split(X, y)) :

                    X_train = X[training_ind, :]
                    X_test = X[testing_ind, :]
                    y_train = y[training_ind]
                    y_test = y[testing_ind]

                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                    if decoder_name == 'random_forest':
                        pass
                    elif decoder_name == 'SGD' :
                        decoder = SGDClassifier()
                    else :
                        raise NotImplementedError

                    decoder.fit(X_train, y_train)
                    y_pred = decoder.predict(X_test)
                    scoring_function = accuracy_score
                    score = scoring_function(y_test, y_pred)
                    kfold_scores.append(score)
                    y_test_all.append(y_test)
                    y_pred_all.append(y_pred)

                y_test_all = np.hstack(y_test_all)
                y_pred_all = np.hstack(y_pred_all)

                score = np.mean(kfold_scores)

                # print(
                #     'pop. size {}/{} bootstrap {}/{} - time bin {}/{} (t={}) - score={:.2f}'.format(
                #         pi + 1, len(subpopulation_sizes),
                #         boot, n_bootstraps,
                #         time_bin, n_time_bins_per_trial, time_bin, score))

                row = [experiment, binsize,
                       subpop_size, time_bin, time, #time0, time1,
                       boot, score]

                df.loc[df.shape[0], :] = row



    pars = {'settings_name' : settings_name,
            'experiment' : experiment,
            'seed' : seed,
            'binsize_in_ms' : binsize,
            'subpopulation_sizes' : subpopulation_sizes,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'sessions' : selected_session_ids,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'n_repeats' : n_repeats,
            'score_name' : score_name,
            'shuffle_kfold' : shuffle_kfold,
            # not strictly user defined
            'selected_trials' : dp,
            'n_time_bins_per_trial' : n_time_bins_per_trial,
            'warp' : warp,
            'warp_per_modality' : warp_per_modality,
            'larger_bins' : larger_bins,
            'median_reaction_times' : median_reaction_times}
            #'time_bin_edges' : time_bin_edges,
            #'time_bin_centers' : time_bin_centers}

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))

    print('Saving parameters to {}'.format(pars_full_path))
    pickle.dump(pars, open(pars_full_path, 'wb'))



