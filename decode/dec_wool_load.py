import os
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from constants import *
from plotting_style import *
import pandas as pd

settings_name = 'apr29_final_l1'

# experiments = ['hit_miss_V',
#                'hit_miss_M',
#                'hit_miss_A']

# experiments = ['correct_vs_incorrect_V',
#                'correct_vs_incorrect_A',
#                'correct_vs_incorrect_M']

# experiments = ['target_vs_distractor_V',
#                'target_vs_distractor_M',
#                'target_vs_distractor_A']

experiments = ['hit_miss_V',
               'hit_miss_M',
               'hit_miss_A',
               'correct_vs_incorrect_V',
               'correct_vs_incorrect_A',
               'correct_vs_incorrect_M',
               # 'target_vs_distractor_V',
               # 'target_vs_distractor_M',
               # 'target_vs_distractor_A',
               'target_vs_distractor_with_misses_V',
               'target_vs_distractor_with_misses_M',
               'target_vs_distractor_with_misses_A']

experiments = ['target_vs_distractor_with_misses_V']

methods = ['wool_deconf']#['wool_deconf', 'wool_nodeconf']

plot_format = 'png'
center = True


for method in methods:
    for experiment in experiments:

        output_file_name = 'decode_wool_setting_{}_{}_{}.pkl'.format(settings_name, method,
                                                             experiment)
        output_folder = os.path.join(DATA_FOLDER, 'results', 'decode_wool',
                                     settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)
        pars_file_name = 'parameters_decode_wool_setting_{}_{}_{}.pkl'.format(settings_name, method,
                                                                      experiment)
        pars_full_path = os.path.join(output_folder, pars_file_name)

        out = pickle.load(open(output_full_path, 'rb'))

        df = out['df']
        df_null = out['df_null']
        pars = out['pars']
        n_shifts = pars['n_shifts']
        surrogate_method = pars['surrogate_method']
        selected_session_ids = pars['sessions']

        # TODO centering does center everything
        if center:
            dfs, dfs_null = [], []
            for session_id in selected_session_ids:

                ds = df[df['session_id'] == session_id]
                ds_null = df_null[df_null['session_id'] == session_id]

                ds['predictability_score'] = ds['predictability_score'] - np.mean(ds['predictability_score'])
                ds_null['predictability_score'] = ds_null['predictability_score'] - np.mean(ds_null['predictability_score'])
                dfs.append(ds)
                dfs_null.append(ds_null)
            df = pd.concat(dfs)
            df_null = pd.concat(dfs_null)


            plots_folder = os.path.join(DATA_FOLDER, 'plots', 'decoding_wool',
                                        '{}_centered'.format(settings_name), method, experiment)
            if not os.path.isdir(plots_folder) :
                os.makedirs(plots_folder)
        else:
            plots_folder = os.path.join(DATA_FOLDER, 'plots', 'decoding_wool',
                                        settings_name, method, experiment)
            if not os.path.isdir(plots_folder) :
                os.makedirs(plots_folder)

        # --- plot accuracy single sessions ---
        f, ax = plt.subplots(1, 1, figsize=[5, 5])
        sns.lineplot(data=df, x='time', y='accuracy_score',
                     hue='session_id', ax=ax, palette='muted',
                     estimator=None)
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Decoding accuracy\n{}'.format(experiment))
        sns.despine()
        plt.tight_layout()

        ax.axhline(0.5, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')

        plot_name = 'dec_wool_acc_ss_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



        # --- plot accuracy combined ---
        f, ax = plt.subplots(1, 1, figsize=[3.5, 3.5])
        sns.lineplot(data=df, x='time', y='accuracy_score',
                     ax=ax, palette='muted', color=experiment_palette[experiment],
                     estimator='mean')
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Decoding accuracy\n{}'.format(experiment))
        sns.despine()
        plt.tight_layout()

        ax.axhline(0.5, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')

        plot_name = 'dec_wool_acc_combined_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



        # --- plot like in wool ---
        times = df['time'].unique()

        dx = pd.DataFrame(columns=['time', 'predictability_score', 'score_sem',
                                   'q1_surr', 'q2_surr', 'sig'])

        for time in times:
            score = df[df['time'] == time]['predictability_score'].__array__()
            mean = score.mean()
            sem = np.std(score, ddof=1) / np.sqrt(np.size(score))

            surrogate_scores = df_null[df_null['time'] == time]['predictability_score']
            q1, q2 = surrogate_scores.quantile(q=[0.025, 0.975])

            n_higher = (surrogate_scores > mean).sum()
            #     # TODO this is an approximate test right?
            p_val = n_higher / len(surrogate_scores)
            significant  = p_val < 0.05

            row = [time, mean, sem, q1, q2, significant]
            dx.loc[dx.shape[0], :] = row

        f, ax = plt.subplots(1, 1, figsize=[3.5, 3.5])

        ax.plot(dx['time'], dx['predictability_score'], c=experiment_palette[experiment])
        ax.scatter(dx['time'], dx['predictability_score'], c=experiment_palette[experiment])

        ax.fill_between(dx['time'].__array__().astype(float),
                        dx['predictability_score'].__array__().astype(float)-dx['score_sem'].__array__().astype(float),
                        dx['predictability_score'].__array__().astype(float)+dx['score_sem'].__array__().astype(float),
                        color=experiment_palette[experiment],
                           zorder=-5, alpha=0.2,
                           linewidth=0)

        ax.fill_between(dx['time'].__array__().astype(float),
                        dx['q1_surr'].__array__().astype(float),
                        dx['q2_surr'].__array__().astype(float), color='grey',
                        zorder=-10, alpha=0.3, label='Null distribution',
                        linewidth=0)

        dxx = dx[dx['sig']]
        ax.scatter(dxx['time'], np.repeat(ax.get_ylim()[1]-0.01, dxx.shape[0]), marker='X',
                      c=experiment_palette[experiment], zorder=10, alpha=1, s=40,
                      label='Significant p<0.05')

        ax.axhline(0.0, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                              c=modality_palette[modality],
                              ls='--')
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Predictability\n{}'.format(experiment))
        sns.despine()
        plt.tight_layout()
        plot_name = 'dec_wool_global_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


        # --- plot predictability single sessions ---
        f, ax = plt.subplots(1, 1, figsize=[5, 5])
        sns.lineplot(data=df, x='time', y='predictability_score',
                     hue='session_id', ax=ax, palette='muted',
                     estimator=None)
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Predictability\n{}'.format(experiment))
        sns.despine()
        plt.tight_layout()

        ax.axhline(0.0, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')

        plot_name = 'dec_wool_ss_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



        # --- plot predictability single sessions same color ---
        f, ax = plt.subplots(1, 1, figsize=[3.5, 3.5])
        sns.lineplot(data=df, x='time', y='predictability_score',
                     hue='session_id', ax=ax,
                     palette=[experiment_palette[experiment]],
                     estimator=None)
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Predictability\n{}'.format(experiment))
        sns.despine()
        plt.tight_layout()

        ax.axhline(0.0, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')
        ax.legend().remove()
        ax.set_ylim([-0.2, 0.4])
        plot_name = 'dec_wool_ss_monochrom_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


        # --- plot predictability averaged ---
        f, ax = plt.subplots(1, 1, figsize=[3.5, 3.5])
        sns.lineplot(data=df, x='time', y='predictability_score',
                     ax=ax, color=experiment_palette[experiment],
                     estimator='mean', errorbar='se')
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Predictability\n{}'.format(experiment))
        sns.despine()
        plt.tight_layout()

        ax.axhline(0.0, ls=':', c='grey')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
        if pars['warp'] :
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities :
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')
        #ax.set_ylim([-0.05, 0.3])

        plot_name = 'dec_wool_combined_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



        # --- plot session by session ---

        for session_id in df_null['session_id'].unique( ):

            times = df[df['session_id'] == session_id]['time'].unique()

            for time in times:

                surrogate_scores = df_null[(df_null['session_id'] == session_id) &
                                           (df_null['time'] == time)]['predictability_score']
                q1, q2 = surrogate_scores.quantile(q=[0.025, 0.975])
                df.loc[(df['session_id'] == session_id) & (df['time'] == time), 'q1'] = q1
                df.loc[(df['session_id'] == session_id) & (df['time'] == time), 'q2'] = q2
                obs_score = df.loc[(df['session_id'] == session_id) &
                            (df['time'] == time), 'predictability_score']
                n_higher = (surrogate_scores > obs_score.array[0]).sum()

                if surrogate_method == 'shift' :
                    df.loc[(df['session_id'] == session_id) & (
                                df['time'] == time), 'significant'] = n_higher == 0
                elif surrogate_method == 'shuffle' :
                    # TODO this is an approximate test right?
                    p_val = n_higher / len(surrogate_scores)
                    df.loc[(df['session_id'] == session_id) & (
                                df['time'] == time), 'significant'] = p_val < 0.05

                sig_wool = n_higher == 0  # conservative test
                p_val = n_higher / n_shifts  # equivalent to m<=a(N+1), not to m<=a(2N+1)
                sig_pval = p_val < 0.05
                assert sig_wool == sig_pval

        n_cols = 3
        n_rows = int(np.ceil(len(selected_session_ids) / 3))
        f, axx = plt.subplots(n_rows, n_cols,
                              figsize=[n_cols * 4, 3 * n_rows],
                              sharex=True, sharey=False)
        ax = axx.flatten()
        for i, session_id in enumerate(selected_session_ids) :

            features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                                'confound' : sns.xkcd_rgb['grey']}

            for features in ['spikes'] :  # 'confound']:
                dx = df[(df['features'] == features) & (df['session_id'] == session_id)]
                ax[i].plot(dx['time'], dx['predictability_score'],
                           c=experiment_palette[experiment],
                           label='decoding from {}'.format(features))
                ax[i].scatter(dx['time'], dx['predictability_score'],
                              c=experiment_palette[experiment])

                # sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
                #              palette=features_palette, markers=True, style='experiment')
                ax[i].set_ylabel('predict')


                ax[i].fill_between(dx['time'].__array__().astype(float),
                                   dx['q1'].__array__(),
                                   dx['q2'].__array__(), color='grey',
                                   zorder=-10, alpha=0.5, label='Null distribution',
                                   linewidth=0)

                dxx = df[(df['features'] == 'spikes') & (df['significant']) & (
                            df['session_id'] == session_id)]
                ax[i].scatter(dxx['time'], np.repeat(ax[i].get_ylim()[1]+0.02, dxx.shape[0]), marker='X',
                              c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                              label='Significant p<0.05')
                ax[i].set_title(session_id)

                ax[i].axhline(0.0, ls=':', c='grey')
                ax[i].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
                if pars['warp'] :
                    median_reaction_times = pars['median_reaction_times']
                    for modality in modalities :
                        ax[i].axvline(median_reaction_times[modality],
                                   c=modality_palette[modality],
                                   ls='--')

            # ax[0].set_title('{}, deconf method: {}'.format(experiment, method))
            ax[-1].set_xlabel('Time [ms]')

            sns.despine()
            plt.tight_layout()

        plot_name = 'dec_wool_panels_{}_{}.{}'.format(experiment, method,
                                                      plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

