import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
import pandas as pd

"""
Loads pseudopopulation results (for convenience grouped in combos)
and plots them in two ways
1. Using sns.lineplot and the standard deviation of the bootstraps
2. Computing confidence intervals of the bootstraps. This can be 
expanded to also have a significance measure at each time point
and plot it as an horizontal line (as I did for Julien).
"""


plot_format = 'png'
min_time = -1000
max_time = 1500

# alpha for confidence intervals
CI_alpha = 0.95

settings_name = 'jan23'
settings_name = 'jan24'
settings_name = 'jan29'

combo = 'target_vs_distractor'


if combo == 'target_vs_distractor':
    experiments = ['target_vs_distractor_V',
                   'target_vs_distractor_M',
                   'target_vs_distractor_A']

elif combo == 'target_vs_distractor_with_misses':
    experiments = ['target_vs_distractor_with_misses_V',
                   'target_vs_distractor_with_misses_M',
                   'target_vs_distractor_with_misses_A']

elif combo == 'correct_vs_incorrect':
    experiments = ['correct_vs_incorrect_V',
                   'correct_vs_incorrect_M',
                   'correct_vs_incorrect_A']

elif combo == 'hit_miss':
    experiments = ['hit_miss_V',
                   'hit_miss_M',
                   'hit_miss_A']

elif combo == 'difficulty_VM':
    experiments = ['difficulty_distractor_VM',
                   'difficulty_target_VM']



plots_folder = os.path.join(DATA_FOLDER, 'plots', 'pseudopop')
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


dfs = []
pars_dict = {}
for experiment in experiments:
    results_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    results_folder = os.path.join(DATA_FOLDER, 'results', 'pseudodec', settings_name)
    results_full_path = os.path.join(results_folder, results_file_name)
    pars_file_name = 'parameters_decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    pars_full_path = os.path.join(results_folder, pars_file_name)
    out = pickle.load(open(results_full_path, 'rb'))
    df = out['decoding_scores']
    pars = out['pars']
    pars_dict[experiment] = pars
    dfs.append(df)
df = pd.concat(dfs)

if pars_dict[experiments[0]]['warp'] and pars_dict[experiments[1]]['warp']:
    warped = True
    median_reaction_times = pars_dict[experiments[0]]['median_reaction_times']

df = df[df['time'] >= min_time]
df = df[df['time'] <= max_time]


# --- LINEPLOT WITH STANDARD DEVIATION ERROR BARS ------------------------------

f, ax = plt.subplots(1, 1, figsize=[3, 3])
sns.lineplot(data=df, x='time', y='score', size='group_size', ci='sd',
            size_order =pars['subpopulation_sizes'], ax=ax, hue='experiment',
            palette=experiment_palette)
#ax.set_ylabel('V vs M in target correct trials\n(decoding accuracy)')
#ax.set_ylabel('Target vs distractor\n(decoding accuracy)')
#ax.set_ylabel('Stimulus contrast in M trials\n(decoding accuracy)')

if warped:
    ax.axvline(0,
               c=sns.xkcd_rgb['grey'],
               ls='--')
    for modality in modalities:
        ax.axvline(median_reaction_times[modality],
                   c=modality_palette[modality],
                   ls='--')

#ax.set_ylabel('Correct decoding in V+M trials\n(decoding accuracy)')
ax.set_ylabel('Contrast decoding')

ax.set_xlabel('Time [ms]')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()

plot_name = 'decoding_over_time_combo_SD_err_{}_{}.{}'.format(settings_name, '_'.join(experiments), plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)




# --- LINEPLOT WITH CONFIDENCE INTERVALS ---------------------------------------

dx = pd.DataFrame(columns=['experiment', 'time', 'mean', 'median',
                           'ci1', 'ci2', 'sig'])

for experiment in experiments:
    for time in df['time'].unique():

        stats = df[(df['experiment'] == experiment) & (df['time'] == time)]['score']

        # TODO
        # p = ((1.0 - alpha) / 2.0) * 100
        # lower = max(0.0, np.percentile(stats, p))
        # p = (alpha + ((1.0 - alpha) / 2.0)) * 100
        # upper = min(1.0, np.percentile(stats, p))
        # sig = lower > 0.5 and upper > 0.5

        lower = np.quantile(stats, q=(1 - CI_alpha) / 2)
        upper = np.quantile(stats, q=(1 - (1 - CI_alpha) / 2))
        sig = lower > 0.5 and upper > 0.5

        dx.loc[dx.shape[0], :] = [experiment, time, stats.mean(), stats.median(),
                                  lower, upper, sig]
        # err.append([obs_val - lower, upper - obs_val])

for col in ['time', 'mean', 'median', 'ci1', 'ci2']:
    dx[col] = pd.to_numeric(dx[col])


f, ax = plt.subplots(1, 1, figsize=[3, 3])

for i, experiment in enumerate(experiments):
    da = dx[dx['experiment'] == experiment]
    ax.plot(da['time'], da['mean'], c=experiment_palette[experiment], linewidth=3)

    ax.fill_between(da['time'], da['ci1'], da['ci2'], color=experiment_palette[experiment],
                    alpha=0.3, linewidth=0)

    # s = np.ma.masked_where(~da['sig'], (1+(i/100)) * np.ones(shape=da.shape[0]))
    # ax.plot(da['time'], s, c=areas_palette[area], linewidth=2)

    # t = da['time'][da['sig']]
    # x = (1+(i/100)) * np.ones(shape=t.shape[0])
    # ax.scatter(t, x, c=experiment_palette[experiment], linewidth=2,s=20, marker='_')


if warped:
    ax.axvline(0,
               c=sns.xkcd_rgb['grey'],
               ls='--')
    for modality in modalities:
        ax.axvline(median_reaction_times[modality],
                   c=modality_palette[modality],
                   ls='--')

#ax.set_ylabel('Correct decoding in V+M trials\n(decoding accuracy)')
ax.set_ylabel(combo_labels[combo])


ax.set_xlabel('Time [ms]')
sns.despine()
plt.tight_layout()

plot_name = 'decoding_over_time_combo_CI_err_{}_{}.{}'.format(settings_name, '_'.join(experiments), plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)






