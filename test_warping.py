import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from plotting_style import *
import matplotlib.gridspec as gridspec
from utils import rolling_window
from utils import fix_animal_id

settings_name = 'simple'

auc_alpha_level = 0.01
align_to = 'stimulus'
binsize = 50


larger_bins = True
warp = True
warp_per_modality = True

plot_format = 'png'
joint_auc_axes = False
plot_observed = False

selected_unit_ids = ['E2R-006_2_0']

# --- PLOTS FOLDER -------------------------------------------------------------

plots_folder = os.path.join(DATA_FOLDER, 'plots', 'psths')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

# --- ALPHA LEVEL -------------------------------------------------------------

if auc_alpha_level == 0.001:
    col_name = 'is_sign_001'
elif auc_alpha_level == 0.01:
    col_name = 'is_sign_01'
elif auc_alpha_level == 0.05:
    col_name = 'is_sign_05'
else:
    raise ValueError


# --- LOAD DATA ----------------------------------------------------------------
trial_df, trial_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
session_ids = trial_df['session_id'].unique()

trial_data_original = trial_data.copy()

if warp:
    trial_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True)
else:
    median_reaction_times = {}

if larger_bins:
    trial_data, time_bin_centers_changed = rolling_window(trial_data, time_bin_centers)
    trial_data_original, time_bin_centers_changed_original = rolling_window(trial_data_original, time_bin_centers)

for i, row in trial_df.iterrows():
    id = row['trial_id']
    if row['hit_miss'] == 'hit':
        try:
            np.testing.assert_array_equal(trial_data_original[id], trial_data[id])
            print('{} same'.format(id))
        except AssertionError:
            print('{} different'.format(id))


# --- SELECT TRIALS ----------------------------------------------------------------

def get_vals(combo):
    if combo == 'target_vs_distractor' :
        vals = ['target', 'distractor']
    elif combo == 'correct_vs_incorrect' :
        vals = [0, 1]
    elif combo == 'hit_miss' :
        vals = ['hit', 'miss']
    elif combo == 'response_side':
        vals = ['L', 'R']
    return vals

def get_col(combo):
    if combo == 'target_vs_distractor' :
        col = 'target'
    elif combo == 'correct_vs_incorrect' :
        col = 'response'
    elif combo == 'hit_miss' :
        col = 'hit_miss'
    elif combo == 'response_side':
        col = 'response_side'
    return col


for unit_id in selected_unit_ids:

    animal_id = unit_id.split('_')[0]
    session_id = unit_id.split('_')[1]
    unit_ind = int(unit_id.split('_')[2])

    combos = ['target_vs_distractor', 'hit_miss', 'correct_vs_incorrect', 'response_side']

    unit_data = {c : {m : {} for m in modalities} for c in combos}

    for combo in combos:
        col = get_col(combo)
        vals = get_vals(combo)
        for im, modality in enumerate(modalities):
            unit_data[combo][modality] = {}
            for ir, val in enumerate(vals):
                sdf = trial_df[(trial_df['modality'] == modality) &
                               (trial_df[col] == val) &
                               (trial_df['session_id'] == int(session_id)) &
                               (trial_df['animal_id'] == animal_id)]
                try:
                    unit_data[combo][modality][val] = np.vstack([trial_data[tid][:, unit_ind] for tid in sdf['trial_id']])
                except ValueError:
                    unit_data[combo][modality][val] = np.zeros(shape=[1, len(time_bin_centers)])


    vmaxes = []
    for ic, combo in enumerate(combos) :
        vals = get_vals(combo)
        for im, modality in enumerate(modalities) :
            vmaxes.append(unit_data[combo][modality][vals[0]].max())
            vmaxes.append(unit_data[combo][modality][vals[1]].max())

    vmax = np.max(vmaxes)

    xticks = [0, 500, 1000]

    f = plt.figure(tight_layout=True, figsize=[7, 7])
    gs = gridspec.GridSpec(4, 3)

    # --- PLOT IMSHOW ----------------------------------------------------------

    imshow_axes = {c : {} for c in combos}

    for ic, combo in enumerate(combos):
        for im, modality in enumerate(modalities):
                imshow_axes[combo][modality] = f.add_subplot(gs[ic, im])

    for ic, combo in enumerate(combos):
        for im, modality in enumerate(modalities):

            vals = get_vals(combo)

            n_trials_1 = unit_data[combo][modality][vals[0]].shape[0]
            n_trials_2 = unit_data[combo][modality][vals[1]].shape[0]

            n_trials = n_trials_1 + n_trials_2

            data1 = unit_data[combo][modality][vals[0]]
            data2 = unit_data[combo][modality][vals[1]]

            data = np.vstack([data1, data2])

            imshow_axes[combo][modality].imshow(data,
                                                vmin=0, vmax=vmax,
                                                extent=(time_bin_centers[0]-0.5, time_bin_centers[-1]-0.5, n_trials+0.5, 0.5),
                                                aspect='auto')
            imshow_axes[combo][modality].axvline(0, c=sns.xkcd_rgb['grey'], ls='--')
            imshow_axes[combo][modality].set_xticks(xticks)
            imshow_axes[combo][modality].set_yticks([1, n_trials_1, n_trials])
            imshow_axes[combo][modality].set_yticks([1, n_trials])
            imshow_axes[combo][modality].axhline([n_trials_1], c=sns.xkcd_rgb['white'])

            if warp:
                imshow_axes[combo][modality].axvline(median_reaction_times[modality],
                                                        c=modality_palette[modality],
                                                        ls='--')

    imshow_axes[combos[0]][modalities[0]].set_ylabel(combos[0])
    imshow_axes[combos[1]][modalities[0]].set_ylabel(combos[1])
    imshow_axes[combos[2]][modalities[0]].set_ylabel(combos[2])
    imshow_axes[combos[3]][modalities[0]].set_ylabel(combos[3])

    imshow_axes[combos[0]][modalities[0]].set_title(modalities[0])
    imshow_axes[combos[0]][modalities[1]].set_title(modalities[1])
    imshow_axes[combos[0]][modalities[2]].set_title(modalities[2])

    sns.despine()

    plot_name = 'psth_simple_{}_{}.{}'.format(settings_name, unit_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()

