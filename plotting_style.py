import seaborn as sns

experiment_palette = {'target_vs_distractor_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_A' : sns.xkcd_rgb['red'],
                      'target_vs_distractor_easy_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_easy_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_easy_A' : sns.xkcd_rgb['red'],
                      'target_vs_distractor_correct_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_correct_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_correct_A' : sns.xkcd_rgb['red'],
                      'target_vs_distractor_incorrect_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_incorrect_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_slow_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_slow_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_slow_A' : sns.xkcd_rgb['red'],
                      'target_vs_distractor_fast_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_fast_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_fast_A' : sns.xkcd_rgb['red'],
                      'correct_vs_incorrect_target' : sns.xkcd_rgb['grey'],
                      'correct_vs_incorrect_target_V' : sns.xkcd_rgb['green'],
                      'correct_vs_incorrect_target_M' : sns.xkcd_rgb['blue'],
                      'correct_vs_incorrect_target_A' : sns.xkcd_rgb['red'],
                      'correct_vs_incorrect' : sns.xkcd_rgb['grey'],
                      'correct_vs_incorrect_V' : sns.xkcd_rgb['green'],
                      'correct_vs_incorrect_M' : sns.xkcd_rgb['blue'],
                      'correct_vs_incorrect_A' : sns.xkcd_rgb['red'],
                      'correct_vs_incorrect_distractor_V' : sns.xkcd_rgb['green'],
                      'correct_vs_incorrect_distractor_M' : sns.xkcd_rgb['blue'],
                      'correct_vs_incorrect_distractor_A' : sns.xkcd_rgb['red'],
                      'target_vs_distractor_V_diff_2' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_M_diff_2' : sns.xkcd_rgb['blue'],
                      'target_correct_V_vs_M' : sns.xkcd_rgb['purple'],
                      'target_vs_distractor_A' : sns.xkcd_rgb['red'],
                      'difficulty_distractor_VM' : sns.xkcd_rgb['purple'],
                      'difficulty_target_VM' : sns.xkcd_rgb['orange'],
                      'difficulty_distractor_VM_correct' : sns.xkcd_rgb['purple'],
                      'difficulty_target_VM_correct' : sns.xkcd_rgb['orange'],
                      'difficulty_distractor_V' : sns.xkcd_rgb['purple'],
                      'difficulty_target_V' : sns.xkcd_rgb['orange'],
                      'difficulty_distractor_M' : sns.xkcd_rgb['purple'],
                      'difficulty_target_M' : sns.xkcd_rgb['orange'],
                      'difficulty_distractor_1vs3_VM' : sns.xkcd_rgb['purple'],
                      'difficulty_target_1vs3_VM' :  sns.xkcd_rgb['orange'],
                      'correct_vs_incorrect_distractor_VM' : sns.xkcd_rgb['purple'],
                      'correct_vs_incorrect_target_VM' : sns.xkcd_rgb['orange'],
                      'response_side' : sns.xkcd_rgb['grey'],
                      'response_side_V' : sns.xkcd_rgb['green'],
                      'response_side_M' : sns.xkcd_rgb['blue'],
                      'response_side_A' : sns.xkcd_rgb['red'],
                      'response_side_correct' : sns.xkcd_rgb['grey'],
                      'response_side_correct_V' : sns.xkcd_rgb['green'],
                      'response_side_correct_M' : sns.xkcd_rgb['blue'],
                      'response_side_correct_A' : sns.xkcd_rgb['red'],
                      'hit_miss_V' : sns.xkcd_rgb['green'],
                      'hit_miss_M' : sns.xkcd_rgb['blue'],
                      'hit_miss_A' : sns.xkcd_rgb['red'],
                      'target_vs_distractor_with_misses_V' : sns.xkcd_rgb['green'],
                      'target_vs_distractor_with_misses_M' : sns.xkcd_rgb['blue'],
                      'target_vs_distractor_with_misses_A' : sns.xkcd_rgb['red'],}

modality_palette = {'V' : sns.xkcd_rgb['green'],
                    'M' : sns.xkcd_rgb['blue'],
                    'A' : sns.xkcd_rgb['red']}


combo_labels = {'target_vs_distractor' : 'Target vs. distractor',
                'target_vs_distractor_easy' : 'Target vs. distractor\n(easy trials)',
                'target_vs_distractor_correct': 'Target vs. distractor\n(correct trials)',
                'target_vs_distractor_incorrect' : 'Target vs. distractor\n(incorrect trials)',
                'target_vs_distractor_slow' : 'Target vs. distractor\n(slow trials)',
                'target_vs_distractor_fast' : 'Target vs. distractor\n(fast trials)',
                'correct_vs_incorrect' : 'Correct vs. incorrect\n(all trials)',
                'correct_vs_incorrect_target' : 'Correct vs. incorrect\n(target trials)',
                'correct_vs_incorrect_distractor' : 'Correct vs. incorrect\n(distractor trials)',
                'difficulty_VM' : 'Difficulty (V+M trials)',
                'response_side' : 'Response side',
                'response_side_correct' : 'Response side (correct trials)',
                'correct_vs_incorrect_A' : 'Outcome (audio trials)',
                'correct_vs_incorrect_V' : 'Outcome (visual trials)',
                'correct_vs_incorrect_M' : 'Outcome (multimodal trials)',
                'hit_miss' : 'Hit vs. misses'}

psth_labels = {1 : 'Correct',
               0 : 'Incorrect'}

response_linestyles = {0 : ':', 1 : '-'}

psth_labels = {1 : 'Correct',
               0 : 'Incorrect',
               'hit' : 'Hit',
               'miss' : 'Miss',
               'target' : 'Target',
               'distractor' : 'Distractor'}

psth_linestyles = {0 : ':',
                   1 : '-',
                   'miss' : ':',
                   'hit' : '-',
                   'target' : '-',
                   'distractor' : ':'}

predictor_set_ls = {'full' : '-', 'stimulus' : '--', 'behavior' : ':',
                    'visual' : '--', 'audio' : '-.'}


