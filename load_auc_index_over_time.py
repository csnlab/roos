import os
import numpy as np
from constants import *
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from plotting_style import *

plot_format = 'png'
plot_per_animal = False
alpha_level = 0.01
n_boot = 100
only_shared_sessions = False

combos = [['dec19_warp', 'correct_vs_incorrect_target'],
          ['dec19_warp', 'correct_vs_incorrect_distractor'],
          ['dec19_warp', 'target_vs_distractor_correct'],
          ['dec19_warp', 'target_vs_distractor']]
combos = [['dec19_warp', 'target_vs_distractor']]
combos = [['jan16', 'target_vs_distractor']]
combos = [['jan16', 'correct_vs_incorrect']]
combos = [['jan16', 'response_side']]
combos = [['jan16', 'response_side_correct']]
combos = [['jan16', 'correct_vs_incorrect_A'],
          ['jan16', 'correct_vs_incorrect_V'],
          ['jan16', 'correct_vs_incorrect_M']]
combos = [['jan20warp', 'hit_miss']]
combos = [['jan20warp', 'difficulty_VM']]
combos = [['jan23', 'correct_vs_incorrect_A'],
          ['jan23', 'correct_vs_incorrect_V'],
          ['jan23', 'correct_vs_incorrect_M'],
          ['jan23', 'difficulty_VM'],
          ['jan23', 'target_vs_distractor'],
          ['jan23', 'correct_vs_incorrect'],
          ['jan23', 'hit_miss']]
combos = [['jan24', 'target_vs_distractor'],
          ['jan24', 'correct_vs_incorrect'],
          ['jan24', 'hit_miss']]
combos = [['jan25', 'target_vs_distractor'],
          ['jan25', 'correct_vs_incorrect'],
          ['jan25', 'hit_miss']]
combos = [['jan25_min8', 'target_vs_distractor'],
          ['jan25_min8', 'correct_vs_incorrect'],
          ['jan25_min8', 'hit_miss']]


settings_name = 'jan26_largerbins'
experiments = ['correct_vs_incorrect_distractor']

settings_name = 'jan26_nowarp_smallbins'

experiments = ['target_vs_distractor_correct',
          'target_vs_distractor',
          'hit_miss',
          'difficulty_VM']


remove_animals = []#['E2R-008', 'E2R-009']

remove_sessions = []#[4, 6, 7]

settings_name = 'jan29'

experiments = ['target_vs_distractor_correct',
               'target_vs_distractor',
               'correct_vs_incorrect_target',
               'correct_vs_incorrect_distractor',
               'correct_vs_incorrect',
               'hit_miss']
               #'difficulty_VM']

combos = [[settings_name, exp] for exp in experiments]

if alpha_level == 0.001:
    col_name = 'is_sign_001'
elif alpha_level == 0.01:
    col_name = 'is_sign_01'
elif alpha_level == 0.05:
    col_name = 'is_sign_05'
else:
    raise ValueError


if plot_per_animal:
    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc_animals')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)

else:
    plots_folder = os.path.join(DATA_FOLDER, 'plots', 'auc')
    if not os.path.isdir(plots_folder):
        os.makedirs(plots_folder)


for settings_name, combo in combos:

    if np.isin(combo, ['correct_vs_incorrect_A',
                       'correct_vs_incorrect_V',
                       'correct_vs_incorrect_M']):
        experiment_palette = {'correct_vs_incorrect_target_V' : sns.xkcd_rgb['orange'],
                              'correct_vs_incorrect_target_M' : sns.xkcd_rgb['orange'],
                              'correct_vs_incorrect_target_A' : sns.xkcd_rgb['orange'],
                              'correct_vs_incorrect_distractor_V' : sns.xkcd_rgb['purple'],
                              'correct_vs_incorrect_distractor_M' : sns.xkcd_rgb['purple'],
                              'correct_vs_incorrect_distractor_A' : sns.xkcd_rgb['purple'],}
    else:
        from plotting_style import experiment_palette

    output_file_name = 'auc_scores_over_time_setting_{}.pkl'.format(combo, settings_name)
    output_folder = os.path.join(DATA_FOLDER, 'results', 'auc_index', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))


    # ---- BOOTSTRAPPED -----------

    df = out['auc_scores']

    df['unit_id'] = ['{}_{}_{}'.format(a, s, i) for a, s, i in
                     zip(df['animal_id'], df['session_id'], df['unit_ind'])]

    if np.isin(settings_name, ['jan25', 'jan25_min8']):
        df['time'] = df['time'] + 80

    pars = out['pars']

    if only_shared_sessions:
        expsess = []
        for experiment in df['experiment'].unique():
            sessions_experiment = df[df['experiment'] == experiment]['session_id'].unique()
            expsess.append(set(sessions_experiment))
        shared_sessions = set.intersection(*expsess)
        df = df[np.isin(df['session_id'], list(shared_sessions))]

    if len(remove_animals) > 0:
        df = df[~np.isin(df['animal_id'], remove_animals)]

    if len(remove_sessions) > 0:
        df = df[~np.isin(df['session_id'], remove_sessions)]


    if not plot_per_animal:
        dn = pd.DataFrame(columns=['time', 'experiment', 'perc_sig'])

        for time in df['time'].unique():
            for experiment in df['experiment'].unique():

                sign = df[(df['time'] == time) & (df['experiment'] == experiment)][col_name]

                for boot in range(n_boot):
                    sign_boot = np.random.choice(sign, size=sign.shape[0], replace=True)
                    perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
                    dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]

        dn['area'] = None
        dn['area'] = 'V1'

        f, ax = plt.subplots(1, 1, figsize=[5, 3])
        sns.lineplot(data=dn, y='perc_sig', x='time', hue='experiment', ax=ax,
                     palette=experiment_palette, markers=True, style='area',
                     ci='sd')
        ax.set_title(combo_labels[combo])
        ax.set_ylabel('% of units with significant AUC')
        ax.set_xlabel('Time [ms]')
        ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')

        if pars['warp']:
            median_reaction_times = pars['median_reaction_times']
            for modality in modalities:
                ax.axvline(median_reaction_times[modality],
                           c=modality_palette[modality],
                           ls='--')

        #ax.set_ylim([0, 30])
        ax.get_legend().remove()
        sns.despine()
        plt.tight_layout()

        plot_name = 'auc_over_time_combo_bootstrapped_{}_{}_{}.{}'.format(combo, settings_name, alpha_level, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    if plot_per_animal:

        animal_ids = df['animal_id'].unique()

        for animal_id in animal_ids:

            dn = pd.DataFrame(columns=['time', 'experiment', 'perc_sig'])

            n_sess_animal = df[df['animal_id'] == animal_id]['session_id'].unique().shape[0]
            n_units_animal = df[df['animal_id'] == animal_id]['unit_id'].unique().shape[0]

            for time in df['time'].unique() :
                for experiment in df['experiment'].unique() :

                    sign = df[(df['time'] == time) &
                              (df['experiment'] == experiment) &
                              (df['animal_id'] == animal_id)][col_name]

                    for boot in range(n_boot) :
                        sign_boot = np.random.choice(sign, size=sign.shape[0],
                                                     replace=True)
                        perc_sign = 100 * sign_boot.sum() / sign_boot.shape[0]
                        dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]

            dn['area'] = None
            dn['area'] = 'V1'

            f, ax = plt.subplots(1, 1, figsize=[5, 3])
            sns.lineplot(data=dn, y='perc_sig', x='time', hue='experiment', ax=ax,
                         palette=experiment_palette, markers=True, style='area',
                         ci='sd')
            ax.set_title('{} - {}\n[{} session(s) {} units]'.format(combo_labels[combo], animal_id, n_sess_animal, n_units_animal))
            ax.set_ylabel('% of units with significant AUC')
            ax.set_xlabel('Time [ms]')
            ax.axvline(0, c=sns.xkcd_rgb['grey'], ls='--')

            if pars['warp'] :
                median_reaction_times = pars['median_reaction_times']
                for modality in modalities :
                    ax.axvline(median_reaction_times[modality],
                               c=modality_palette[modality],
                               ls='--')
            # ax.set_ylim([0, 30])
            ax.get_legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'auc_over_time_combo_bootstrapped_{}_{}_{}_{}.{}'.format(combo,
                                                                              settings_name,
                                                                          alpha_level, animal_id,plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



# # ---- NOT BOOTSTRAPPED ------
# df = out['auc_scores']
#
# dn = pd.DataFrame(columns=['time', 'experiment', 'perc_sig'])
#
# for time in df['time'].unique():
#     for experiment in df['experiment'].unique():
#
#         sign = df[(df['time'] == time) & (df['experiment'] == experiment)]['is_sign']
#         perc_sign = 100 * sign.sum() / sign.shape[0]
#
#         dn.loc[dn.shape[0], :] = [time, experiment, perc_sign]
#
# dn['area'] = None
# dn['area'] = 'V1'
# if out['pars']['align_to'] == 'response':
#     dn = dn[dn['time'] <= 0]
#
#
# f, ax = plt.subplots(1, 1, figsize=[3, 3])
# sns.lineplot(data=dn, y='perc_sig', x='time', hue='experiment', ax=ax,
#              palette=experiment_palette, markers=True, style='area')
# ax.set_title(combo_labels[combo])
# ax.set_ylabel('% of units with significant AUC')
# ax.set_xlabel('Time [ms]')
# ax.get_legend().remove()
# sns.despine()
# plt.tight_layout()
#
#
# plot_name = 'auc_over_time_combo_{}_{}.{}'.format(combo, settings_name, plot_format)
# f.savefig(os.path.join(plots_folder, plot_name), dpi=400)